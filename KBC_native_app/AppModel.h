//
//  AppModel.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 28/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//
// Singleton that hold general information throughout the app

#import <Foundation/Foundation.h>


@interface AppModel : NSObject{

}

@property (strong, nonatomic) NSString *IP;
@property (strong, nonatomic) NSString* registeringCardNr;
@property (strong, nonatomic) NSString* registeringPassword;
@property (strong, nonatomic) NSObject* userInfo;

+(AppModel *)sharedAppModel;

@end
