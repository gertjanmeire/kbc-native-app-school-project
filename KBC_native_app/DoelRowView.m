//
//  DoelRowView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "DoelRowView.h"

#import "DoelModel.h"

@implementation DoelRowView

@synthesize doelModel = _doelModel;

@synthesize titleLabel = _titleLabel;
@synthesize progressBar = _progressBar;
@synthesize clockImageView = _clockImageView;
@synthesize deadlineLabel = _deadlineLabel;
@synthesize savedAmountLabel = _savedAmountLabel;

- (id)initWithDoel:(DoelModel*)model
{
    self = [super init];
    if (self) {
        //Background image
         UIImage *backgroundImage = [ImageFactory createImageWithName:@"doelRowViewBG" andType:@"png" andDirectory:@"images"];
         UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
         [self addSubview:backgroundImageView];
        
        //Set model
        self.doelModel = model;
        
        //Title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 250, 25)];
        [self.titleLabel setText:self.doelModel.doelNaam];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
        [self.titleLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.titleLabel];

        //Progress bar images
        UIImage * trackImage = [ImageFactory createImageWithName:@"progressBarGoalTrack" andType:@"png" andDirectory:@"images"];
        UIImage * progressImage = [ImageFactory createImageWithName:@"progressBarGoalProgress" andType:@"png" andDirectory:@"images"];

        
        //Progress bar
        self.progressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(10, 45, 183, 13)];
        [self.progressBar setTrackImage:trackImage];
        [self.progressBar setProgressImage:progressImage];
        
        NSLog(@"doel bedrag: %f", [self.doelModel.doelBedrag floatValue]);
        NSLog(@"gespaard bedrag: %f", [self.doelModel.gespaardBedrag floatValue]);
        
        float procent = ([self.doelModel.gespaardBedrag floatValue]/[self.doelModel.doelBedrag floatValue]);
        if(procent == 0){
            self.progressBar.progress = 0;
        }else{
            self.progressBar.progress = procent;
        }
        
        [self addSubview:self.progressBar];
        
        //Clock imageview
        UIImage* clockImage = [ImageFactory createImageWithName:@"clockIcon" andType:@"png" andDirectory:@"images"];
        self.clockImageView = [[UIImageView alloc] initWithImage:clockImage];
        [self.clockImageView setFrame:CGRectMake(10, 10, 50, 50)];
        [self addSubview:self.clockImageView];
        
        //Deadline label
        self.deadlineLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 103, 150, 25)];
        [self.deadlineLabel setText:self.doelModel.deadline];
        [self.deadlineLabel setFont:[UIFont fontWithName:@"LorimerLight" size:18]];
        [self.deadlineLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.deadlineLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.deadlineLabel];

        
        //Saved amount label
        self.savedAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(215, 30, 100, 25)];
        [self.savedAmountLabel setText:[NSString stringWithFormat:@"€ %@", self.doelModel.gespaardBedrag]];
        [self.savedAmountLabel setFont:[UIFont fontWithName:@"LorimerLight" size:18]];
        [self.savedAmountLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.savedAmountLabel setBackgroundColor:[UIColor clearColor]];

        [self addSubview:self.savedAmountLabel];

        
        
    }
    return self;
}



@end
