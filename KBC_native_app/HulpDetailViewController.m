//
//  HulpDetailViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "HulpDetailViewController.h"

@interface HulpDetailViewController ()

@end

@implementation HulpDetailViewController

@synthesize hulpOptionModel = _hulpOptionModel;
@synthesize hulpOptionImageView = _hulpOptionImageView;

- (id)initWithModel:(HulpOptionModel*)model
{
    self = [super init];
    if (self) {
        //Set the background color
        [self.view setBackgroundColor:[UIColor cyanColor]];
        
        //Set the model
        self.hulpOptionModel = model;
        
        NSLog(@"test %@", self.hulpOptionModel.detailImage);
        
        //Set the detail view
        UIImage* detailImage = [ImageFactory createImageWithName:self.hulpOptionModel.detailImage andType:@"png" andDirectory:@"images"];
        self.hulpOptionImageView = [[UIImageView alloc] initWithImage:detailImage];
        [self.hulpOptionImageView setFrame:CGRectMake(0, 0, detailImage.size.width, detailImage.size.height)];
        [self.view addSubview:self.hulpOptionImageView];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// viewWillAppear: hide the back button, show the custom buttons and navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    self.navigationController.navigationBar.hidden = NO;
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_hulpInNood" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
