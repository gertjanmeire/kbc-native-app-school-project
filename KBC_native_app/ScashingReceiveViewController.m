//
//  ScashingReceiveViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ScashingReceiveViewController.h"

@interface ScashingReceiveViewController ()

@end

@implementation ScashingReceiveViewController

//Step 1
@synthesize step1View = _step1View;

//Step 2
@synthesize step2View = _step2View;
@synthesize totaalLabelImageView = _totaalLabelImageView;
@synthesize totalReceivedMoneyLabel = _totalReceivedMoneyLabel;
@synthesize bevestigButton = _bevestigButton;

@synthesize betaling1ImageView = _betaling1ImageView;
@synthesize betaling2ImageView = _betaling2ImageView;

@synthesize tutorialButton = _tutorialButton;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor yellowColor]];
        
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showIncommingMoneyTransfers:) userInfo:nil repeats:NO];
        [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(showFirstMoneyTransfer:) userInfo:nil repeats:NO];
        [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(showSecondMoneyTransfer:) userInfo:nil repeats:NO];
    }
    return self;
}


-(void)showIncommingMoneyTransfers:(NSTimer*)sender{
    NSLog(@"Show incomming data");
    self.step1View.hidden = YES;
    self.step2View.hidden = NO;
}

-(void)showFirstMoneyTransfer:(NSTimer*)sender{
    //Betaling 1
    UIImage* betaling1Image = [ImageFactory createImageWithName:@"betaling1Image" andType:@"png" andDirectory:@"images"];
    self.betaling1ImageView = [[UIImageView alloc] initWithImage:betaling1Image];
    [self.betaling1ImageView setFrame:CGRectMake(20, 13, betaling1Image.size.width, betaling1Image.size.height)];
    [self.step2View addSubview:self.betaling1ImageView];
    
    //Fake incomming money
    [self.totalReceivedMoneyLabel setText:@"€ 50"];
}

-(void)showSecondMoneyTransfer:(NSTimer*)sender{
    //Betaling 2
    UIImage* betaling2Image = [ImageFactory createImageWithName:@"betaling2Image" andType:@"png" andDirectory:@"images"];
    self.betaling2ImageView = [[UIImageView alloc] initWithImage:betaling2Image];
    [self.betaling2ImageView setFrame:CGRectMake(20, 103, betaling2Image.size.width, betaling2Image.size.height)];
    [self.step2View addSubview:self.betaling2ImageView];
    
    //Fake incomming money
    [self.totalReceivedMoneyLabel setText:@"€ 110"];

}




/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //Step 1
    self.step1View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.step1View setBackgroundColor:[UIColor yellowColor]];
    [self.view addSubview:self.step1View];

    //Background image step 1 view
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"receiveMoneyStep1BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.step1View addSubview:backgroundImageView];

    
    //Step 2
    self.step2View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.step2View.hidden = YES;
    [self.view addSubview:self.step2View];
    
    //Background image step 2 view
    UIImage *backgroundImage2 = [ImageFactory createImageWithName:@"receiveMoneyStep2BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView2 = [[UIImageView alloc] initWithImage:backgroundImage2];
    [self.step2View addSubview:backgroundImageView2];
    
    //Totaal label
    UIImage* totaalLabelImage = [ImageFactory createImageWithName:@"receiveMoneyTotaalLabel" andType:@"png" andDirectory:@"images"];
    self.totaalLabelImageView = [[UIImageView alloc] initWithImage:totaalLabelImage];
    [self.totaalLabelImageView setFrame:CGRectMake(122, 239, totaalLabelImage.size.width, totaalLabelImage.size.height)];
    [self.step2View addSubview:self.totaalLabelImageView];
    
    //Total received money label
    self.totalReceivedMoneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(112, 275, 200, 50)];
    [self.totalReceivedMoneyLabel setBackgroundColor:[UIColor clearColor]];
    [self.totalReceivedMoneyLabel setText:@"€ 0"];
    [self.totalReceivedMoneyLabel setFont:[UIFont fontWithName:@"BreeSerif-Regular" size:39]];
    [self.totalReceivedMoneyLabel setTextColor:[UIColor colorWithRed:(231.0/255.0) green:(130.0/255.0) blue:(22.0/255.0) alpha:1.0]];
    [self.step2View addSubview:self.totalReceivedMoneyLabel];
    

    //Bevestig button
    UIImage *bevestigButtonImage = [ImageFactory createImageWithName:@"bevestigButton" andType:@"png" andDirectory:@"images"];
    self.bevestigButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bevestigButton setImage:bevestigButtonImage forState:UIControlStateNormal];
    self.bevestigButton.frame = CGRectMake(72, 344, bevestigButtonImage.size.width, bevestigButtonImage.size.height);
    [self.bevestigButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.step2View addSubview:self.bevestigButton];

    
    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.step2View addSubview:self.tutorialButton];
    
}

-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayScashingReceiveMoney" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}

/****************************************************************************************************************/
// viewDidAppear: add the navigation bar and hide the back button
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_scashen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;

    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}

/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
