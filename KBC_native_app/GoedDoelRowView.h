//
//  GoedDoelRowView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADVRoundProgressView.h"

@interface GoedDoelRowView : UIView

@property (strong, nonatomic) UILabel* titleLabel;
@property (strong, nonatomic) UILabel* currencyLabel;
@property (strong, nonatomic) UIScrollView* donatiePicker;
@property (strong, nonatomic) UIButton* detailButton;
@property (strong, nonatomic) UISlider* donateSlider;

@property (strong, nonatomic) ADVRoundProgressView *roundProgressLarge;

- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title;

@end
