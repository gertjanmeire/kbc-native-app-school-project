//
//  TransactieRowView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactieModel.h"


@interface TransactieRowView : UIView

@property (strong, nonatomic) TransactieModel* transactieModel;


@property (strong, nonatomic) UILabel* dateLabel;
@property (strong, nonatomic) UILabel* titleLabel;
@property (strong, nonatomic) UILabel* amountLabel;

@property (strong, nonatomic) UIImageView* transactieBG;


- (id)initWithFrame:(CGRect)frame andTransactieModel:(TransactieModel*)model;

@end
