//
//  ZoekKantoorViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ZoekKantoorViewController.h"
#import "KantoorDetailViewController.h"


@interface ZoekKantoorViewController ()

@end

@implementation ZoekKantoorViewController

@synthesize zoekOpGemeenteButton = _zoekOpGemeenteButton;
@synthesize zoekMetGPSButton = _zoekMetGPSButton;
@synthesize zoekOpGemeenteField = _zoekOpGemeenteField;

@synthesize mapView = _mapView;
@synthesize locationManager = _locationManager;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}



/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    //Add the mapview
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [self.view addSubview: self.mapView];
    
    //Hard coded bank lat and long values
    NSArray* bank1 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:50.825808], [NSNumber numberWithDouble:3.266442], nil];
    NSArray* bank2 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:50.993642], [NSNumber numberWithDouble:4.500946], nil];
    NSArray* bank3 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:50.992092], [NSNumber numberWithDouble:4.117763], nil];
    
    NSArray *bankCoords = [[NSArray alloc] initWithObjects: bank1, bank2, bank3 , nil];
    
    //Add the markers
    for (NSArray *bank in bankCoords) {
        CLLocationCoordinate2D annotationCoord;
        annotationCoord.latitude = [[bank objectAtIndex:0] doubleValue];
        annotationCoord.longitude = [[bank objectAtIndex:1] doubleValue];
        
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        annotationPoint.coordinate = annotationCoord;
        annotationPoint.title = @"KBC";
        annotationPoint.subtitle = @"KBC Bank";
        [self.mapView addAnnotation:annotationPoint];
    }
    
    //Get user location
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.locationManager.distanceFilter = 10;
    [self.locationManager startUpdatingLocation];

    
    //Kantoor op kaart toevoegen (tijdelijk)
    UIImage* kantoor = [ImageFactory createImageWithName:@"mapKantoorIndicator" andType:@"png" andDirectory:@"images"];
    UIImageView* kantoorView = [[UIImageView alloc] initWithImage:kantoor];
    [kantoorView setFrame:CGRectMake(100, 250, kantoor.size.width, kantoor.size.height)];
    kantoorView.userInteractionEnabled = YES;
    [self.view addSubview:kantoorView];
    
    UITapGestureRecognizer *kantoorTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoKantoorDetailView:)];
    [kantoorView addGestureRecognizer:kantoorTap];
    
    //Gemeente textfield
    UIImage *zoekOpGemeenteFieldImage = [ImageFactory createImageWithName:@"textfieldZoekKantoorBG" andType:@"png" andDirectory:@"images"];
    self.zoekOpGemeenteField = [[CustomTextField alloc] initWithFrame:CGRectMake(25, 25, zoekOpGemeenteFieldImage.size.width, zoekOpGemeenteFieldImage.size.height)];
    [self.zoekOpGemeenteField setBackground:zoekOpGemeenteFieldImage];
    [self.zoekOpGemeenteField setPlaceholder:@"Zoek op gemeente"];
    [self.zoekOpGemeenteField setDelegate:self];
    [self.view addSubview:self.zoekOpGemeenteField];
    
    //Zoek button
    UIImage *zoekOpGemeenteButtonImage = [ImageFactory createImageWithName:@"zoekOpGemeenteButton" andType:@"png" andDirectory:@"images"];
    self.zoekOpGemeenteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.zoekOpGemeenteButton setImage:zoekOpGemeenteButtonImage forState:UIControlStateNormal];
    self.zoekOpGemeenteButton.frame = CGRectMake(255, 25, zoekOpGemeenteButtonImage.size.width, zoekOpGemeenteButtonImage.size.height);
    [self.zoekOpGemeenteButton addTarget:self action:@selector(zoekOpGemeente:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.zoekOpGemeenteButton];
    
    //GPS zoek button
    UIImage *zoekMetGPSButtonImage = [ImageFactory createImageWithName:@"zoekMetGPSButton" andType:@"png" andDirectory:@"images"];
    self.zoekMetGPSButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.zoekMetGPSButton setImage:zoekMetGPSButtonImage forState:UIControlStateNormal];
    self.zoekMetGPSButton.frame = CGRectMake(10, 410, zoekMetGPSButtonImage.size.width, zoekMetGPSButtonImage.size.height);
    [self.zoekMetGPSButton addTarget:self action:@selector(zoekOpGPS:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.zoekMetGPSButton];
    
    
}


-(void)gotoKantoorDetailView:(UITapGestureRecognizer*)sender{
    KantoorDetailViewController *kantoorDetailVC = [[KantoorDetailViewController alloc] init];
    [self.navigationController pushViewController:kantoorDetailVC animated:YES];
}


/****************************************************************************************************************/
// zoekOpGemeente: search banks based on the city field
/****************************************************************************************************************/
-(void)zoekOpGemeente:(UIButton *)sender{
    NSLog(@"Zoek op Gemeente");
    [self gotoKantoorDetailView:nil];
}


/****************************************************************************************************************/
// zoekOpGPS: search banks based on your GPS location
/****************************************************************************************************************/
-(void)zoekOpGPS:(UIButton *)sender{
    NSLog(@"Zoek op GPS");
    
}


/****************************************************************************************************************/
// viewWillAppear: hide back button, show navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide navbar button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    self.navigationController.navigationBar.hidden = NO;
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_zoekKantoor" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    [super viewWillAppear:animated];
}




/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
