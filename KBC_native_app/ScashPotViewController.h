//
//  ScashPotViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "DDPageControl.h"

@interface ScashPotViewController : UIViewController

@property (strong, nonatomic) UIScrollView* donatiePicker;
@property (strong, nonatomic) DDPageControl* pageControl;
@property (strong, nonatomic) UIButton* donateButton;
@property (strong, nonatomic) UIButton* tutorialButton;



@end
