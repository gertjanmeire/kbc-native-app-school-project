//
//  LoginViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"

@interface LoginViewController : UIViewController <AMFRemotingCallDelegate, UITextFieldDelegate>{
    AppModel* appModel;
}

@property (strong, nonatomic) AppModel* appModel;

@property (strong, nonatomic) CustomTextField* kaartNrTextField;
@property (strong, nonatomic) CustomTextField* wachtwoordTextField;
@property (strong, nonatomic) UIImageView* onthoudenImageView;
@property (strong, nonatomic) UIButton* loginButton;
@property (strong, nonatomic) UIButton* registrationButton;
@property (strong, nonatomic) UIButton *kantoorButton;
@property (strong, nonatomic) UIButton *hulpButton;

@end
