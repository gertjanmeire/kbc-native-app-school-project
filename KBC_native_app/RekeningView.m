//
//  RekeningView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "RekeningView.h"
#import "RekeningModel.h"

@implementation RekeningView

@synthesize rekeningModel = _rekeningModel;

@synthesize bundleView = _bundleView;
@synthesize  rekeningImageView = _rekeningImageView;
@synthesize titleLabel = _titleLabel;
@synthesize amountLabel = _amountLabel;

- (id)initWithFrame:(CGRect)frame andRekeningModel:(RekeningModel*)model
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //Rekeningmodel instellen
        self.rekeningModel = model;
        
        //Bundeling view
        self.bundleView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, self.bounds.size.width, self.bounds.size.height)];
        [self addSubview:self.bundleView];
        
        //Rekening bg imageview
        UIImage *rekeningBGImage = [ImageFactory createImageWithName:@"rekeningViewBG" andType:@"png" andDirectory:@"images"];
        self.rekeningImageView = [[UIImageView alloc] initWithImage:rekeningBGImage];
        [self.rekeningImageView setFrame:CGRectMake(0, 0, rekeningBGImage.size.width, rekeningBGImage.size.height)];
        [self.bundleView addSubview:self.rekeningImageView];
        
        //Title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 264, 25)];
        [self.titleLabel setText:self.rekeningModel.rekeningNaam];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:25]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.bundleView addSubview:self.titleLabel];
        
        //Rekening nr label (NOG IN DB STEKEN)
        /*self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 100, 25)];
        [self.titleLabel setText:self.rekeningModel.rekeningNaam];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:19]];
        [self.bundleView addSubview:self.titleLabel];*/
        
        //Amount label
        self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 170, 264, 50)];
        [self.amountLabel setText:[NSString stringWithFormat:@"%.2f EUR", [self.rekeningModel.saldo doubleValue]]];
        [self.amountLabel setFont:[UIFont fontWithName:@"LorimerLight" size:40]];
        [self.amountLabel setBackgroundColor:[UIColor clearColor]];
        self.amountLabel.textAlignment = NSTextAlignmentCenter;
        [self.amountLabel setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.bundleView addSubview:self.amountLabel];
        
    }
    return self;
}


@end
