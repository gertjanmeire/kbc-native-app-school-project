//
//  AppDelegate.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "AppDelegate.h"
#import "IIWrapController.h"
#import "MainViewController.h"
#import "StartViewController.h"
#import "LoginViewController.h"
#import "NotificationView.h"
#import "NotificationDetail.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize navController = _navController;
@synthesize languageSelectController = _languageSelectController;
@synthesize loginViewController = _loginViewController;

@synthesize notification = _notification;
@synthesize notificationDetailView = _notificationDetailView;

/****************************************************************************************************************/
// application: setup the navigationcontroller for the app (language select and login screens etc)
/****************************************************************************************************************/
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    
    //Set the languageSelectController as the navigation controller of the app
    self.languageSelectController = [[MainViewController alloc] init];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.languageSelectController];
    self.window.rootViewController = self.navController;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeRootViewHandler:) name:@"LOGIN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeToLoginScreenAfterLogout:) name:@"LOGOUT" object:nil];

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

/****************************************************************************************************************/
// changeRootViewHandler: change the rootViewcontroller to the StartViewController to implement the ViewDeck
/****************************************************************************************************************/
-(void)changeRootViewHandler:(NSNotification *)notification{
    [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(showNotificationExample:) userInfo:nil repeats:NO];
    
    StartViewController* startController = [[StartViewController alloc] init];
    self.window.rootViewController = [[IIWrapController alloc] initWithViewController:startController];
}

-(void)showNotificationExample:(NSTimer*)sender{
    NSLog(@"NOTIFICATION");
    self.notification = [[NotificationView alloc] initWithFrame:CGRectMake(5, 20, 320, 116)];
    
    UITapGestureRecognizer* tapNotification = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(seeNotification:)];
    [self.notification addGestureRecognizer:tapNotification];
    
    int count = [[[UIApplication sharedApplication]windows]count];
    [[[[UIApplication sharedApplication] windows] objectAtIndex:count-1]addSubview:self.notification];
}

-(void)seeNotification:(UITapGestureRecognizer*)sender{
    NSLog(@"SHOW NOTIFICATION DETAIL");
    self.notification.hidden = YES;
    
    self.notificationDetailView = [[NotificationDetail alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    
    int count = [[[UIApplication sharedApplication]windows]count];
    [[[[UIApplication sharedApplication] windows] objectAtIndex:count-1]addSubview:self.notificationDetailView];
}


/****************************************************************************************************************/
// changeRootViewHandler: change the rootViewcontroller to the StartViewController to implement the ViewDeck
/****************************************************************************************************************/
-(void)changeToLoginScreenAfterLogout:(NSNotification *)notification{
    self.loginViewController = [[LoginViewController alloc] init];
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.loginViewController];
    self.window.rootViewController = self.navController;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
