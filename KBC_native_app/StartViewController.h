//
//  StartViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface StartViewController : UIViewController


@property (strong, nonatomic) UIButton* mainMenuButton;
@property (strong, nonatomic) UIButton* logoutButton;
@property (strong, nonatomic) UIButton* tutorialButton;

@property (strong, nonatomic) IIViewDeckController* deckController;

@end
