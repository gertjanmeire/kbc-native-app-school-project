//
//  CustomTextField.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 01/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

@synthesize leftMargin = _leftMargin;
@synthesize topMargin = _topMargin;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //Set margins
        self.leftMargin = 10;
        self.topMargin = 12;
        
        //Set font and color
        [self setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
        [self setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
    }
    return self;
}


- (CGRect)textRectForBounds:(CGRect)bounds {
    int marginLeft = self.leftMargin;
    int marginTop = self.topMargin;
    CGRect inset = CGRectMake(bounds.origin.x + marginLeft, bounds.origin.y + marginTop, bounds.size.width - marginLeft, bounds.size.height - marginTop);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    int marginLeft = self.leftMargin;
    int marginTop = self.topMargin;
    CGRect inset = CGRectMake(bounds.origin.x + marginLeft, bounds.origin.y + marginTop, bounds.size.width - marginLeft, bounds.size.height - marginTop);
    return inset;
}

-(void)changeTopMargin:(int)topMargin{
    self.topMargin = topMargin;
}

-(void)changeLeftMargin:(int)leftMargin{
    self.leftMargin = leftMargin;
}

@end
