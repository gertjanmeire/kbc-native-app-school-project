//
//  TransactiesViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "TransactiesViewController.h"
#import "TransactieRowView.h"
#import "TransactieDetailViewController.h"

#import "TransactieModel.h"

@interface TransactiesViewController ()

@end

@implementation TransactiesViewController


@synthesize transactiesPicker = _transactiesPicker;
@synthesize rekeningID = _rekeningID;
@synthesize transactiesArr = _transactiesArr;
@synthesize pageControl = _pageControl;

- (id)initWithRekeningID:(NSNumber*)rekeningID
{
    self = [super init];
    if (self) {
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];
        
        self.rekeningID = rekeningID;
    }
    return self;
}


-(void)getTransacties{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getTransacties" arguments:[NSArray arrayWithObjects: [[AppModel sharedAppModel].userInfo valueForKey:@"id"], self.rekeningID ,nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}


- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    self.transactiesArr = [[NSArray alloc] initWithArray:arr];
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= self.transactiesArr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in self.transactiesArr) {
        NSLog(@"Item name: %@", item);
        CGFloat y = i * 100;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber *transactieID = (NSNumber *)[item valueForKey:@"id"];
        NSString *rekeningNaam = (NSString *)[item valueForKey:@"rekeningNaam"];
        NSNumber *saldo = (NSNumber *)[item valueForKey:@"saldo"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        NSString* betaalmethode = (NSString*)[item valueForKey:@"betaalmethode"];
        
        NSString* datum = (NSString*)[item valueForKey:@"datum"];
        NSString* titel = (NSString*)[item valueForKey:@"titel"];
        NSNumber* amount = (NSNumber*)[item valueForKey:@"amount"];
        NSString* plaats = (NSString*)[item valueForKey:@"plaats"];
        NSString* gemeente = (NSString*)[item valueForKey:@"gemeente"];
        NSNumber* rekeningID = (NSNumber*)[item valueForKey:@"rekeningID"];
        TransactieModel *transactieModel = [[TransactieModel alloc] initWithTransactieID:transactieID andRekeningNaam:rekeningNaam andSaldo:saldo andUserID:userID andBetaalmethode:betaalmethode andDate:datum andTitle:titel andAmount:amount andPlaats:plaats andGemeente:gemeente andRekeningID:rekeningID];
        
        //Rekening view aanmaken in de scrollview
        TransactieRowView *transactieView = [[TransactieRowView alloc] initWithFrame:CGRectMake(0, 0, 320, 110) andTransactieModel:transactieModel];
        [transactieView setFrame:CGRectMake(0, y,self.transactiesPicker.frame.size.width, 100)];
        [self.transactiesPicker addSubview:transactieView];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(transactionSelected:)];
        [transactieView addGestureRecognizer:singleTap];
        i++;
    }
    
    self.transactiesPicker.contentSize = CGSizeMake(self.transactiesPicker.frame.size.width, 100 *viewcount);
    
    //Page control toevegen
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(25, 450, 250, 50)];
    self.pageControl.numberOfPages = viewcount;
    self.pageControl.currentPage = 0;
    [self.view addSubview:self.pageControl];
    
}

- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    //Set the UIScrollView with all the user's pet's
    self.transactiesPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, 320, 500)];
    [self.transactiesPicker setBounces:YES];
    [self.transactiesPicker setPagingEnabled:YES];
    [self.transactiesPicker setShowsHorizontalScrollIndicator:YES];
    self.transactiesPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.transactiesPicker setCanCancelContentTouches:NO];
    
    self.transactiesPicker.delegate = self;
    [self.view addSubview:self.transactiesPicker];
}


/****************************************************************************************************************/
// transactionSelected: go to the detailview of the selected transaction
/****************************************************************************************************************/
-(void)transactionSelected:(UITapGestureRecognizer*)sender{
    NSLog(@"Selected a transaction row");
    TransactieRowView *selectedTransaction = (TransactieRowView*)sender.view ;
    //NSLog(@"Selected transaction with title: %@", selectedTransaction.title );
    
    NSLog(@"SELECTED TRANSACTION: %@", selectedTransaction.transactieModel.titel);
    
    TransactieDetailViewController *transactieDetailVC = [[TransactieDetailViewController alloc] initWithTransactionModel:selectedTransaction.transactieModel];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:transactieDetailVC];
    
}

/****************************************************************************************************************/
// viewDidAppear: hide the back button, show the navigation bar and show custom bar buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide the back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_transacties" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [self getTransacties];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
