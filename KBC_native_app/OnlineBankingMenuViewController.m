//
//  OnlineBankingMenuViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "OnlineBankingMenuViewController.h"

#import "RekeningOverzichtViewController.h"
#import "OverschrijvingenViewController.h"
#import "UitgavenViewController.h"
#import "NieuweRekeningViewController.h"

@interface OnlineBankingMenuViewController ()

@end

@implementation OnlineBankingMenuViewController

@synthesize onlineBankingMenuButton = _onlineBankingMenuButton;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];

    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the menu buttons for the Online Banking part of the app
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float xPos = 15;
    float yPos = 45;
    
    for(int i=1; i<=4; i++){
        NSString *buttonName = [NSString stringWithFormat:@"onlineBankingMenuButton%d", i];
        if(i%2 == 0  || i == 1){
            UIImage* onlineBankingMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.onlineBankingMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.onlineBankingMenuButton setImage:onlineBankingMenuButtonImage forState:UIControlStateNormal];
            self.onlineBankingMenuButton.frame = CGRectMake(xPos, yPos, onlineBankingMenuButtonImage.size.width, onlineBankingMenuButtonImage.size.height);
            [self.onlineBankingMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.onlineBankingMenuButton addTarget:self action:@selector(gotoPage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.onlineBankingMenuButton];
            
            xPos += 150;
        }else if(i%2 == 1 && i != 1){
            xPos = 15;
            yPos += 225;
            
            UIImage* onlineBankingMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.onlineBankingMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.onlineBankingMenuButton setImage:onlineBankingMenuButtonImage forState:UIControlStateNormal];
            self.onlineBankingMenuButton.frame = CGRectMake(xPos, yPos, onlineBankingMenuButtonImage.size.width, onlineBankingMenuButtonImage.size.height);
            [self.onlineBankingMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.onlineBankingMenuButton addTarget:self action:@selector(gotoPage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.onlineBankingMenuButton];
            
            xPos += 150;
        }
    }
}


/****************************************************************************************************************/
// gotoPage: go to one of the pages when clicking one of the menu buttons
/****************************************************************************************************************/
-(void)gotoPage:(UIButton*)sender{
    UIButton* senderButton = (UIButton*)sender;
    NSString* compareToString = senderButton.currentTitle;
    
    if([compareToString isEqualToString:@"onlineBankingMenuButton1"]){
        RekeningOverzichtViewController *rekeningOverzichtVC = [[RekeningOverzichtViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:rekeningOverzichtVC];
    }else if([compareToString isEqualToString:@"onlineBankingMenuButton2"]){
        OverschrijvingenViewController *overschrijvingenVC = [[OverschrijvingenViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:overschrijvingenVC];
    }else if([compareToString isEqualToString:@"onlineBankingMenuButton3"]){
        UitgavenViewController *uitgavenVC = [[UitgavenViewController alloc] init];
        [self.navigationController pushViewController:uitgavenVC animated:YES];
    }else if([compareToString isEqualToString:@"onlineBankingMenuButton4"]){
        NSLog(@"Nieuwe rekening maken");
    }
}


/****************************************************************************************************************/
// viewDidAppear: hide the back button and show the navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_onlineBankingMenu" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
