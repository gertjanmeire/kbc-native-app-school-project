//
//  AppDelegate.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NotificationView.h"
#import "NotificationDetail.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) UIViewController* languageSelectController;
@property (strong, nonatomic) UIViewController* loginViewController;

@property (strong, nonatomic) NotificationView* notification;
@property (strong, nonatomic) NotificationDetail* notificationDetailView;

@end
