//
//  OverschrijvingenViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface OverschrijvingenViewController : UIViewController <AMFRemotingCallDelegate, UITextFieldDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) UIImageView* overschrijvingHeaderImageView;
@property (strong, nonatomic) UIImage* headerImage1;
@property (strong, nonatomic) UIImage* headerImage2;
@property (strong, nonatomic) UIImage* headerImage3;
@property (strong, nonatomic) UIImage* headerImage4;
@property (strong, nonatomic) UIImage* headerImage5;

//Tabbar images
@property (strong, nonatomic) UIImage* tabbarButton1Image;
@property (strong, nonatomic) UIImage* tabbarButton2Image;
@property (strong, nonatomic) UIImage* tabbarButton3Image;
@property (strong, nonatomic) UIImage* tabbarButton4Image;
@property (strong, nonatomic) UIImage* tabbarButton5Image;

//Tabbar selected images
@property (strong, nonatomic) UIImage* selectedTabbarButton1Image;
@property (strong, nonatomic) UIImage* selectedTabbarButton2Image;
@property (strong, nonatomic) UIImage* selectedTabbarButton3Image;
@property (strong, nonatomic) UIImage* selectedTabbarButton4Image;
@property (strong, nonatomic) UIImage* selectedTabbarButton5Image;


//Tabbar buttons
@property (strong, nonatomic) UIButton* tabbarButton1;
@property (strong, nonatomic) UIButton* tabbarButton2;
@property (strong, nonatomic) UIButton* tabbarButton3;
@property (strong, nonatomic) UIButton* tabbarButton4;
@property (strong, nonatomic) UIButton* tabbarButton5;

//Step 1
@property (strong, nonatomic) UIView* step1View;
@property (strong, nonatomic) UIButton* step1Button;
@property (strong, nonatomic) UIScrollView* rekeningenPicker;
@property (strong, nonatomic) NSArray* rekeningArr;

//Step 2
@property (strong, nonatomic) UIView* step2View;
@property (strong, nonatomic) UIButton* step2Button;
@property (strong, nonatomic) UIScrollView* contactenRekeningenPicker;
@property (strong, nonatomic) NSArray* contactenRekeningArr;


//Step 3
@property (strong, nonatomic) UIView* step3View;
@property (strong, nonatomic) CustomTextField* amountField;

//Step 4
@property (strong, nonatomic) UIView* step4View;
//Date picker??
@property (strong, nonatomic) UIDatePicker* datePicker;

//Step 5
@property (strong, nonatomic) UIView* step5View;
@property (strong, nonatomic) UIButton* normaalBerichtButton;
@property (strong, nonatomic) UIButton* gestructureerdBerichtButton;
@property (strong, nonatomic) CustomTextField* normaalBerichtField;
@property (strong, nonatomic) CustomTextField* gestructureerdBerichtField;
@property (strong, nonatomic) UIButton* overschrijvingsButton;

@property (strong, nonatomic) UIButton* tutorialButton;



@end
