//
//  DonatieConfirmatieViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 27/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "DonatieConfirmatieViewController.h"

@interface DonatieConfirmatieViewController ()

@end

@implementation DonatieConfirmatieViewController

@synthesize terugButton = _terugButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"donateBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    //Terug naar doelen button
    UIImage *terugButtonImage = [ImageFactory createImageWithName:@"terugButton" andType:@"png" andDirectory:@"images"];
    self.terugButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.terugButton setImage:terugButtonImage forState:UIControlStateNormal];
    self.terugButton.frame = CGRectMake(72, 370, terugButtonImage.size.width, terugButtonImage.size.height);
    [self.terugButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.terugButton];
}

/****************************************************************************************************************/
// viewDidAppear: add the navigation bar and hide the back button
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_donatie" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}

/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
