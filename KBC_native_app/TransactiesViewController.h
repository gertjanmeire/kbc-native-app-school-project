//
//  TransactiesViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "TransactieModel.h"
 
@interface TransactiesViewController : UIViewController <AMFRemotingCallDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView* transactiesPicker;
@property (strong, nonatomic) NSNumber* rekeningID;
@property (strong, nonatomic) NSArray* transactiesArr;
@property (strong, nonatomic) UIPageControl* pageControl;


- (id)initWithRekeningID:(NSNumber*)rekeningID;

@end
