//
//  MainViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (strong, nonatomic) UIButton *nlButton;
@property (strong, nonatomic) UIButton *frButton;
@property (strong, nonatomic) UIButton *enButton;
@property (strong, nonatomic) UIButton *deButton;


@end
