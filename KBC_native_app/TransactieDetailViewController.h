//
//  TransactieDetailViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

#import "TransactieModel.h"


@interface TransactieDetailViewController : UIViewController

//Model
@property (strong, nonatomic) TransactieModel* transactieModel;
@property (strong, nonatomic) UILabel* betaalmethode;
@property (strong, nonatomic) UILabel* amount;
@property (strong, nonatomic) UILabel* datum;
@property (strong, nonatomic) UILabel* plaats;
@property (strong, nonatomic) UILabel* gemeente;


- (id)initWithTransactionModel:(TransactieModel*)model;

@end
