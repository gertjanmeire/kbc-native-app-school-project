//
//  DoelSparenMenuViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "DoelSparenMenuViewController.h"
#import "MijnDoelenViewController.h"
#import "NieuwDoelViewController.h"

@interface DoelSparenMenuViewController ()

@end

@implementation DoelSparenMenuViewController

@synthesize mijnDOelenButton = _mijnDOelenButton;
@synthesize nieuwDoelButton = _nieuwDoelButton;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the menu items for DoelSparen
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];

    //Mijn doelen button
    UIImage* mijnDoelenButtonImage = [ImageFactory createImageWithName:@"beheerDoelButton" andType:@"png" andDirectory:@"images"];
    self.mijnDOelenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mijnDOelenButton setImage:mijnDoelenButtonImage forState:UIControlStateNormal];
    self.mijnDOelenButton.frame = CGRectMake(85, 23, mijnDoelenButtonImage.size.width, mijnDoelenButtonImage.size.height);
    [self.mijnDOelenButton addTarget:self action:@selector(gotoMijnDoelenView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.mijnDOelenButton];
    
    //Nieuw doel button
    UIImage* nieuwDoelButtonImage = [ImageFactory createImageWithName:@"nieuwDoelButton" andType:@"png" andDirectory:@"images"];
    self.nieuwDoelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.nieuwDoelButton setImage:nieuwDoelButtonImage forState:UIControlStateNormal];
    self.nieuwDoelButton.frame = CGRectMake(85, 263, nieuwDoelButtonImage.size.width, nieuwDoelButtonImage.size.height);
    [self.nieuwDoelButton addTarget:self action:@selector(gotoNieuwDoelView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nieuwDoelButton];
}

/****************************************************************************************************************/
// gotoMijnDoelenView: go to the MijnDoelenViewController
/****************************************************************************************************************/
-(void)gotoMijnDoelenView:(UIButton*)sender{
    NSLog(@"Go to mijn doelen");
    MijnDoelenViewController *mijnDoelenVC = [[MijnDoelenViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:mijnDoelenVC];
    
}


/****************************************************************************************************************/
// gotoNieuwDoelView: go to the NieuwDoelViewController
/****************************************************************************************************************/
-(void)gotoNieuwDoelView:(UIButton*)sender{
    NSLog(@"Go to nieuw doel");
    NieuwDoelViewController *nieuwDoelVC = [[NieuwDoelViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:nieuwDoelVC];
}


/****************************************************************************************************************/
// viewDidAppear: hide the back button and show the navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];

    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_doelsparenMenu" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
