//
//  KBCCentenView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "KBCCentenView.h"

@implementation KBCCentenView

@synthesize kbcCenterImageView = _kbcCenterImageView;
@synthesize kbcCentenLabel = _kbcCentenLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //KBC centen
        UIImage *kbcCentenImage = [ImageFactory createImageWithName:@"kbcCenten" andType:@"png" andDirectory:@"images"];
        self.kbcCenterImageView = [[UIImageView alloc] initWithImage:kbcCentenImage];
        [self.kbcCenterImageView setFrame:CGRectMake(0, 0, kbcCentenImage.size.width, kbcCentenImage.size.height)];
        [self addSubview:self.kbcCenterImageView];
        
        //KBC centen label
        self.kbcCentenLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 25, 40, 30)];
        [self.kbcCentenLabel setFont:[UIFont fontWithName:@"LorimerLight" size:24]];
        self.kbcCentenLabel.textAlignment = NSTextAlignmentCenter;
        [self.kbcCentenLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.kbcCentenLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.kbcCentenLabel];

        
    }
    return self;
}

-(void)changeLabel:(NSString*)labelText{
    [self.kbcCentenLabel setText:[NSString stringWithFormat:@"%@", labelText]];
}


@end
