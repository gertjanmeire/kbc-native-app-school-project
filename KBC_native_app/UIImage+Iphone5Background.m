//
//  UIImage+Iphone5Background.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "UIImage+Iphone5Background.h"

@implementation UIImage (Iphone5Background)

+ (UIImage*)imageNamedForDevice:(NSString*)name {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
            //NSLog(@"Is Iphone");
        if (([UIScreen mainScreen].bounds.size.height * [UIScreen mainScreen].scale) >= 1136.0f)
        {
            
            //NSLog(@"Is Iphone 5");
            //Check if is there a path extension or not
            if (name.pathExtension.length) {
                name = [name stringByReplacingOccurrencesOfString: [NSString stringWithFormat:@".%@", name.pathExtension]
                                                       withString: [NSString stringWithFormat:@"-568h@2x.%@", name.pathExtension ] ];
                
            } else {
                name = [name stringByAppendingString:@"-568h@2x"];
            }

            //NSLog(@"New path = %@", name);
            
            //load the image e.g from disk or cache
            //UIImage *image = [UIImage imageNamed: name ];
            
            UIImage *logoImage = [[UIImage alloc] initWithContentsOfFile:name];
            
            /*if (image) {
                //strange Bug in iOS, the image name have a "@2x" but the scale isn't 2.0f
                return [UIImage imageWithCGImage: image.CGImage scale:2.0f orientation:image.imageOrientation];
            }*/
            
            return logoImage;
            
        }
    }
    
    return [[UIImage alloc] initWithContentsOfFile:name];
    
}

@end
