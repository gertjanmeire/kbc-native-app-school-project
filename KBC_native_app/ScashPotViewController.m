//
//  ScashPotViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ScashPotViewController.h"
#import "GoedDoelRowView.h"
#import "GoalDetailViewController.h"
#import "DonatieConfirmatieViewController.h"

@interface ScashPotViewController ()

@end

@implementation ScashPotViewController

@synthesize donatiePicker = _donatiePicker;
@synthesize pageControl = _pageControl;
@synthesize donateButton = _donateButton;
@synthesize tutorialButton = _tutorialButton;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDetailViewForGoal:) name:@"SHOW_DETAIL_VIEW_OF_GOAL" object:nil];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"scashPotBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    NSArray* donatieDoelen = [NSArray arrayWithObjects:@"De Vlaamse Alzheimer Liga", @"Een Hart voor Autisme", nil];
    
    
    //Set the UIScrollView 
    self.donatiePicker = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 147, 290, 250)];
    [self.donatiePicker setBounces:YES];
    [self.donatiePicker setPagingEnabled:YES];
    [self.donatiePicker setShowsHorizontalScrollIndicator:YES];
    self.donatiePicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.donatiePicker setCanCancelContentTouches:NO];
    
    self.donatiePicker.delegate = self;
    [self.view addSubview:self.donatiePicker];
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= donatieDoelen.count;
    

    int i = 0;
    
    for (NSString* item in donatieDoelen) {
        CGFloat x = i * 290;
        NSLog(@"x: %f", x);
        
        //CUSTOM UIVIEWS AANMAKEN!! met properties in voor de option!!
        GoedDoelRowView *goedDoelOption = [[GoedDoelRowView alloc] initWithFrame:CGRectMake(x, 0, self.donatiePicker.bounds.size.width, self.donatiePicker.bounds.size.height) andTitle:item];
        [self.donatiePicker addSubview:goedDoelOption];
        
        i++;
    }    
    self.donatiePicker.contentSize = CGSizeMake(self.donatiePicker.frame.size.width *viewcount, self.donatiePicker.frame.size.height);


    //ScashPotButton
    UIImage* donateButtonImage = [ImageFactory createImageWithName:@"donateButton" andType:@"png" andDirectory:@"images"];
    self.donateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.donateButton setImage:donateButtonImage forState:UIControlStateNormal];
    self.donateButton.frame = CGRectMake(72, 435, donateButtonImage.size.width, donateButtonImage.size.height);
    [self.donateButton addTarget:self action:@selector(donateCurrency:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.donateButton];
    
    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tutorialButton];
    
    //Add the page control
	self.pageControl = [[DDPageControl alloc] init];
	[self.pageControl setCenter: CGPointMake(self.view.center.x + 3,self.view.center.y + 120)];
	[self.pageControl setNumberOfPages: 2];
	[self.pageControl setCurrentPage: 0];
	[self.pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged];
	[self.pageControl setDefersCurrentPageDisplay: YES];
    [self.pageControl setType:DDPageControlTypeOnFullOffFull];
	[self.pageControl setOnColor: [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
	[self.pageControl setOffColor: [UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 10.0f];
	[self.view addSubview: self.pageControl];
    
}


-(void)pageControlClicked:(DDPageControl*)sender{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
    [self.donatiePicker setContentOffset:CGPointMake(self.donatiePicker.bounds.size.width * thePageControl.currentPage, self.donatiePicker.contentOffset.y) animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (self.pageControl.currentPage != nearestNumber)
	{
		self.pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[self.pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[self.pageControl updateCurrentPageDisplay] ;
}





-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayScashPot" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}


/****************************************************************************************************************/
// donateCurrency: donate selected currency to the respective goals
/****************************************************************************************************************/
-(void)donateCurrency:(UIButton*)sender{
    NSLog(@"Donate currency");
    DonatieConfirmatieViewController* donatieConfirmatieVC = [[DonatieConfirmatieViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:donatieConfirmatieVC];
}

-(void)gotoDetailView:(UIButton*)sender{
    NSLog(@"Go to detail view");
    GoalDetailViewController *goalDetailVC = [[GoalDetailViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:goalDetailVC];
}


/****************************************************************************************************************/
// viewWillAppear: add back button and custom buttons and remove navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_scashPot" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// showDetailViewForGoal: show the detailview of the selected goal
/****************************************************************************************************************/
-(void)showDetailViewForGoal:(NSNotification *)notification{
    GoalDetailViewController *goalDetailVC = [[GoalDetailViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:goalDetailVC];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
