//
//  DoelModel.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoelModel : NSObject

@property (strong, nonatomic) NSNumber* doelID;
@property (strong, nonatomic) NSString* doelNaam;
@property (strong, nonatomic) NSNumber* doelBedrag;
@property (strong, nonatomic) NSNumber* gespaardBedrag;
@property (strong, nonatomic) NSString* deadline;
@property (strong, nonatomic) NSNumber* userID;


- (id)initWithDoelID:(NSNumber*)doelID andDoelNaam:(NSString*)doelNaam andDoelBedrag:(NSNumber*)doelBedrag andGespaardBedrag:(NSNumber*)gespaardBedrag andDeadline:(NSString*)deadline andUserID:(NSNumber*)userID;

@end
