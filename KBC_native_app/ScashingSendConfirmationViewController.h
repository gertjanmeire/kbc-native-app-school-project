//
//  ScashingSendConfirmationViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface ScashingSendConfirmationViewController : UIViewController

@property (strong, nonatomic) UILabel* amountLabel;
@property (strong, nonatomic) UILabel* receiverLabel;
@property (strong, nonatomic) UIButton* confirmButton;
@property (strong, nonatomic) UIButton* tutorialButton;


- (id)initWithAmount:(NSString*)amount andReceiver:(NSString*)receiver;

@end
