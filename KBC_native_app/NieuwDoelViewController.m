//
//  NieuwDoelViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "NieuwDoelViewController.h"
#import "BevestigDoelViewController.h"
#import "RekeningPickerRowView.h"


@interface NieuwDoelViewController ()

@end

@implementation NieuwDoelViewController

@synthesize nieuwDoelScrollview = _nieuwDoelScrollview;
@synthesize nieuwDoelImageView = _nieuwDoelImageView;

@synthesize rekeningenArr = _rekeningenArr;

@synthesize naamField = _naamField;
@synthesize deadlineField = _deadlineField;
@synthesize doelbedragField = _doelbedragField;
@synthesize rekeningPicker = _rekeningPicker;
@synthesize pageControl = _pageControl;
@synthesize confirmButton = _confirmButton;
@synthesize tutorialButton = _tutorialButton;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}


-(void)getRekeningen{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getRekeningenForUser" arguments:[NSArray arrayWithObjects: user_id ,nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}



- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    self.rekeningenArr = [[NSArray alloc] initWithArray:arr];
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= self.rekeningenArr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in self.rekeningenArr) {
        NSLog(@"Item name: %@", item);
        CGFloat x = i * self.rekeningPicker.frame.size.width;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber *rekeningID = (NSNumber *)[item valueForKey:@"id"];
        NSString *rekeningNaam = (NSString *)[item valueForKey:@"rekeningNaam"];
        NSNumber *saldo = (NSNumber *)[item valueForKey:@"saldo"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        RekeningModel *rekeningModel = [[RekeningModel alloc] initWithRekeningNaam:rekeningNaam andID:rekeningID andSaldo:saldo andUserID:userID];
        
        //Rekening view aanmaken in de scrollview
        RekeningPickerRowView *rekeningPickerRowView = [[RekeningPickerRowView alloc] initWithFrame:CGRectMake(0, 0, self.rekeningPicker.bounds.size.width, self.rekeningPicker.bounds.size.height) andRekeningModel:rekeningModel];
        [rekeningPickerRowView setFrame:CGRectMake(x, 0,self.view.frame.size.width, self.view.frame.size.height)];
        [self.rekeningPicker addSubview:rekeningPickerRowView];
        
        i++;
    }
    
    self.rekeningPicker.contentSize = CGSizeMake(self.rekeningPicker.frame.size.width * viewcount, self.rekeningPicker.bounds.size.height);
    
    
	//Add the page control
	self.pageControl = [[DDPageControl alloc] init];
	[self.pageControl setCenter: CGPointMake(self.view.center.x,self.view.center.y + 325)];
	[self.pageControl setNumberOfPages: viewcount];
	[self.pageControl setCurrentPage: 0];
	[self.pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged];
	[self.pageControl setDefersCurrentPageDisplay: YES];
    [self.pageControl setType:DDPageControlTypeOnFullOffFull];
	[self.pageControl setOnColor: [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
	[self.pageControl setOffColor: [UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 10.0f];
	[self.nieuwDoelScrollview addSubview: self.pageControl];
    
}


-(void)pageControlClicked:(DDPageControl*)sender{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
	[self.rekeningPicker setContentOffset: CGPointMake(self.rekeningPicker.bounds.size.width * thePageControl.currentPage , self.rekeningPicker.contentOffset.y) animated: YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (self.pageControl.currentPage != nearestNumber)
	{
		self.pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[self.pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[self.pageControl updateCurrentPageDisplay] ;
}

/****************************************************************************************************************/
// remotingCall: fail of the remote call
/****************************************************************************************************************/
- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"[MainVC] remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical components for NieuwDoel
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nieuwDoelScrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 510)];
    [self.nieuwDoelScrollview setBounces:YES];
    [self.nieuwDoelScrollview setShowsHorizontalScrollIndicator:YES];
    self.nieuwDoelScrollview.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.nieuwDoelScrollview setCanCancelContentTouches:NO];
    
    self.nieuwDoelScrollview.delegate = self;
    [self.view addSubview:self.nieuwDoelScrollview];
    
    self.nieuwDoelScrollview.contentSize = CGSizeMake(self.nieuwDoelScrollview.bounds.size.width, 675);
    
    //Background image
    UIImage* backgroundImage = [ImageFactory createImageWithName:@"nieuwDoelBG" andType:@"png" andDirectory:@"images"];
    self.nieuwDoelImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.nieuwDoelScrollview addSubview:self.nieuwDoelImageView];
    
    //Naam textfield
    UIImage *naamFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.naamField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 55, naamFieldImage.size.width, naamFieldImage.size.height)];
    [self.naamField setBackground:naamFieldImage];
    [self.naamField setText:@""];
    [self.naamField setPlaceholder:@"Vul een naam in"];
    [self.naamField setDelegate:self];
    [self.nieuwDoelScrollview addSubview:self.naamField];
    
    //Deadline textfield
    UIImage *deadlineFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.deadlineField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 163, deadlineFieldImage.size.width, deadlineFieldImage.size.height)];
    [self.deadlineField setBackground:deadlineFieldImage];
    [self.deadlineField setText:@""];
    [self.deadlineField setPlaceholder:@"Vul een deadline in"];
    [self.deadlineField setDelegate:self];
    [self.nieuwDoelScrollview addSubview:self.deadlineField];
    
    //Doelbedrag textfield
    UIImage *doelbedragFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.doelbedragField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 277, doelbedragFieldImage.size.width, doelbedragFieldImage.size.height)];
    [self.doelbedragField setBackground:doelbedragFieldImage];
    [self.doelbedragField setText:@""];
    [self.doelbedragField setPlaceholder:@"Vul een bedrag in"];
    [self.doelbedragField setDelegate:self];
    [self.nieuwDoelScrollview addSubview:self.doelbedragField];
    
    
    //Rekening picker
    self.rekeningPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(15, 406, 320, 160)];
    [self.rekeningPicker setBounces:YES];
    [self.rekeningPicker setPagingEnabled:YES];
    [self.rekeningPicker setShowsHorizontalScrollIndicator:YES];
    self.rekeningPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.rekeningPicker setCanCancelContentTouches:NO];
    
    self.rekeningPicker.delegate = self;
    [self.nieuwDoelScrollview addSubview:self.rekeningPicker];
    
    
    //Confirm button
    UIImage *confirmButtonImage = [ImageFactory createImageWithName:@"berekenButton" andType:@"png" andDirectory:@"images"];
    self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.confirmButton setImage:confirmButtonImage forState:UIControlStateNormal];
    self.confirmButton.frame = CGRectMake(82, 600, confirmButtonImage.size.width, confirmButtonImage.size.height);
    [self.confirmButton addTarget:self action:@selector(confirmNewGoal:) forControlEvents:UIControlEventTouchUpInside];
    [self.nieuwDoelScrollview addSubview:self.confirmButton];

    
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 625, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.nieuwDoelScrollview addSubview:self.tutorialButton];

}


-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayNieuwDoel" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}


/****************************************************************************************************************/
// confirmNewGoal: save a new goal in the database
/****************************************************************************************************************/
-(void)confirmNewGoal:(UIButton*)sender{
    NSLog(@"Confirm new goal");
    
    //Go to the Bevestig doel view
    BevestigDoelViewController *bevestigDoelVC = [[BevestigDoelViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:bevestigDoelVC];
}

/****************************************************************************************************************/
// viewWillAppear: hide back button, show navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide navbar button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_nieuwDoel" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Background image
    /*UIImage *backgroundImage = [ImageFactory createImageWithName:@"zoekKantoorBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];*/
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [self getRekeningen];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
