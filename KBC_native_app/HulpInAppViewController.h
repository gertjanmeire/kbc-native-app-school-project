//
//  HulpInAppViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 25/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HulpOptionModel.h"
#import "IIViewDeckController.h"

#import "DDPageControl.h"

@interface HulpInAppViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView* hulpPicker;
@property (strong, nonatomic) DDPageControl* pageControl;

@end

