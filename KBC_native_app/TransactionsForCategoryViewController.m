//
//  TransactionsForCategoryViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 25/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "TransactionsForCategoryViewController.h"

@interface TransactionsForCategoryViewController ()




@end

@implementation TransactionsForCategoryViewController

@synthesize periodeSlider = _periodeSlider;
@synthesize totaleUitgavenLabel = _totaleUitgavenLabel;
@synthesize periodeLabel = _periodeLabel;
@synthesize detailUitgavenLabel = _detailUitgavenLabel;

@synthesize baseTotaleUitgaven = _baseTotaleUitgaven;
@synthesize baseDetailUitgaven = _baseDetailUitgaven;

@synthesize totaleUitgaven = _totaleUitgaven;
@synthesize periode = _periode;
@synthesize detailUitgaven = _detailUitgaven;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.baseTotaleUitgaven = 320;
        self.baseDetailUitgaven = 170;
        
        self.totaleUitgaven = self.baseTotaleUitgaven;
        self.periode = 2;
        self.detailUitgaven = self.baseDetailUitgaven;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"uitgavenDetailBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    //Totale uitgaven label
    self.totaleUitgavenLabel = [[UILabel alloc] initWithFrame:CGRectMake(185, 15, 125, 30)];
    [self.totaleUitgavenLabel setText:[NSString stringWithFormat:@"€ %.0f", self.totaleUitgaven]];
    [self.totaleUitgavenLabel setFont:[UIFont fontWithName:@"LorimerLight" size:34]];
    [self.totaleUitgavenLabel setTextColor:[UIColor colorWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0]];
    [self.totaleUitgavenLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.totaleUitgavenLabel];

    //Periode label
    self.periodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(113, 52, 197, 30)];
    [self.periodeLabel setText:[NSString stringWithFormat:@"%d maanden", self.periode]];
    [self.periodeLabel setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
    [self.periodeLabel setTextColor:[UIColor colorWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0]];
    [self.periodeLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.periodeLabel];
 
    
    //Slider images
    UIImage * sliderLeftTrackImage = [ImageFactory createImageWithName:@"slider2EmptyTrack" andType:@"png" andDirectory:@"images"];
    UIImage * sliderRightTrackImage = [ImageFactory createImageWithName:@"slider2Track" andType:@"png" andDirectory:@"images"];
    UIImage * thumbImage = [ImageFactory createImageWithName:@"thumb2" andType:@"png" andDirectory:@"images"];
    
    //Periode slider
    self.periodeSlider = [[UISlider alloc] initWithFrame:CGRectMake(10, 77, 283, 50)];
    [self.periodeSlider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.periodeSlider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
    [self.periodeSlider setThumbImage:thumbImage forState:UIControlStateNormal];
    self.periodeSlider.minimumValue = 1;
    self.periodeSlider.maximumValue = 8;
    [self.periodeSlider setValue:1 animated:YES];
    [self.periodeSlider addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.periodeSlider];

    //Detail uitgaven label
    self.detailUitgavenLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 180, 78, 30)];
    [self.detailUitgavenLabel setText:[NSString stringWithFormat:@"€ %.0f", self.detailUitgaven]];
    [self.detailUitgavenLabel setFont:[UIFont fontWithName:@"LorimerLight" size:33]];
    [self.detailUitgavenLabel setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
    [self.detailUitgavenLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.detailUitgavenLabel];
}

-(void)changeValue:(UISlider*)sender{
    self.totaleUitgaven = self.baseTotaleUitgaven * sender.value;
    self.periode = sender.value;
    self.detailUitgaven = self.baseDetailUitgaven * sender.value;
    
    [self.totaleUitgavenLabel setText:[NSString stringWithFormat:@"€ %.0f", self.totaleUitgaven]];
    [self.periodeLabel setText:[NSString stringWithFormat:@"%d maanden", self.periode]];
    [self.detailUitgavenLabel setText:[NSString stringWithFormat:@"€ %.0f", self.detailUitgaven]];

}

/****************************************************************************************************************/
// viewwillAppear: change to tab 1
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_uitgaven" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
