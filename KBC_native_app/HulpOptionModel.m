//
//  HulpOptionModel.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "HulpOptionModel.h"

@implementation HulpOptionModel

@synthesize nameImage = _nameImage;
@synthesize detailImage = _detailImage;

- (id)initWithName:(NSString*)nameImage andWithDetail:(NSString*)detailImage
{
    self = [super init];
    if (self) {
        //Set the properties
        self.nameImage = nameImage;
        self.detailImage = detailImage;
    }
    return self;
}

@end
