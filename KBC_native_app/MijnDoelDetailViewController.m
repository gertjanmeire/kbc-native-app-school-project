//
//  MijnDoelDetailViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "MijnDoelDetailViewController.h"
#import "DoelAanpassenViewController.h"
#import "DoelGeldToevoegenViewController.h"
#import "DoelGeldVerwijderenViewController.h"

#import "DoelModel.h"

@interface MijnDoelDetailViewController ()

@end

@implementation MijnDoelDetailViewController

@synthesize doelModel = _doelModel;

@synthesize roundProgressLarge = _roundProgressLarge;;
@synthesize titelLabel = _titelLabel;
@synthesize aanpassenButton = _aanpassenButton;
@synthesize addMoneyButton = _addMoneyButton;
@synthesize removeMoneyButton = _removeMoneyButton;

- (id)initWithDoelModel:(DoelModel*)model
{
    self = [super init];
    if (self) {
        //Set model
        self.doelModel = model;
        
        [self.titelLabel setText:[NSString stringWithFormat:@"%@", self.doelModel.doelNaam]];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"doelDetailBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];

    
    //Titel label
    self.titelLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
    [self.titelLabel setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
    [self.titelLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.titelLabel];

    //Round progress view
    UIColor *tintColor = [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0];
    self.roundProgressLarge = [[ADVRoundProgressViewType2 alloc] initWithFrame:CGRectMake(30, 44, 262, 262) andImage:[ImageFactory createImageWithName:@"progress-circle-large2" andType:@"png" andDirectory:@"images"]];
    [self.roundProgressLarge setTintColor:tintColor];
    
    float procent = ([self.doelModel.gespaardBedrag floatValue]/[self.doelModel.doelBedrag floatValue]);
    if(procent == 0){
        self.roundProgressLarge.progress = 0;
    }else{
        self.roundProgressLarge.progress = procent;
    }

    
    [self.view addSubview:self.roundProgressLarge];

        
    //Aanpassen button
    UIImage* aanpassenButtonImage = [ImageFactory createImageWithName:@"aanpassenButton" andType:@"png" andDirectory:@"images"];
    self.aanpassenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.aanpassenButton setImage:aanpassenButtonImage forState:UIControlStateNormal];
    self.aanpassenButton.frame = CGRectMake(7, 377, aanpassenButtonImage.size.width, aanpassenButtonImage.size.height);
    [self.aanpassenButton addTarget:self action:@selector(gotoAanpassenView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.aanpassenButton];

    
    //Geld toevoegen button
    UIImage* addMoneyButtonImage = [ImageFactory createImageWithName:@"addMoneyButton" andType:@"png" andDirectory:@"images"];
    self.addMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addMoneyButton setImage:addMoneyButtonImage forState:UIControlStateNormal];
    self.addMoneyButton.frame = CGRectMake(110, 377, addMoneyButtonImage.size.width, addMoneyButtonImage.size.height);
    [self.addMoneyButton addTarget:self action:@selector(gotoAddMoneyView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.addMoneyButton];

    
    //Geld verwijderen button
    UIImage* removeMoneyButtonImage = [ImageFactory createImageWithName:@"removeMoneyButton" andType:@"png" andDirectory:@"images"];
    self.removeMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.removeMoneyButton setImage:removeMoneyButtonImage forState:UIControlStateNormal];
    self.removeMoneyButton.frame = CGRectMake(215, 377, removeMoneyButtonImage.size.width, removeMoneyButtonImage.size.height);
    [self.removeMoneyButton addTarget:self action:@selector(gotoRemoveMoney:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.removeMoneyButton];
}


/****************************************************************************************************************/
// gotoAanpassenView: go to the aanpassingen view
/****************************************************************************************************************/
-(void)gotoAanpassenView:(UIButton*)sender{
    NSLog(@"Go to aanpassen view");
    DoelAanpassenViewController *doelAanpassenVC = [[DoelAanpassenViewController alloc] initWithDoelModel:self.doelModel];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:doelAanpassenVC];
}


/****************************************************************************************************************/
// gotoAddMoneyView: go to the geld toevoegen view
/****************************************************************************************************************/
-(void)gotoAddMoneyView:(UIButton*)sender{
    NSLog(@"Go to add money view");
    DoelGeldToevoegenViewController *doelGeldToevoegenVC = [[DoelGeldToevoegenViewController alloc] initWithDoelModel:self.doelModel];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:doelGeldToevoegenVC];
}


/****************************************************************************************************************/
// gotoRemoveMoney: go to the geld verwijderen view
/****************************************************************************************************************/
-(void)gotoRemoveMoney:(UIButton*)sender{
    NSLog(@"Go to remove money view");
    DoelGeldVerwijderenViewController *doelGeldVerwijderenVC = [[DoelGeldVerwijderenViewController alloc] initWithDoelModel:self.doelModel];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:doelGeldVerwijderenVC];
}

/****************************************************************************************************************/
// viewwillAppear: change to tab 1
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_doelsparenDetail" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
