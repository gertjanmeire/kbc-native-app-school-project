//
//  TransactionsForCategoryViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 25/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface TransactionsForCategoryViewController : UIViewController

@property (strong, nonatomic) UISlider* periodeSlider;
@property (strong, nonatomic) UILabel* totaleUitgavenLabel;
@property (strong, nonatomic) UILabel* periodeLabel;
@property (strong, nonatomic) UILabel* detailUitgavenLabel;

@property double  baseTotaleUitgaven;
@property double  baseDetailUitgaven;


@property (nonatomic) double totaleUitgaven;
@property (nonatomic) int periode;
@property (nonatomic) double detailUitgaven;

@end
