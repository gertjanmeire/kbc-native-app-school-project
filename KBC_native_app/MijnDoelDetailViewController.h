//
//  MijnDoelDetailViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "ADVRoundProgressViewType2.h"

#import "DoelModel.h"

@interface MijnDoelDetailViewController : UIViewController

@property (strong, nonatomic) DoelModel* doelModel;

@property (strong, nonatomic) ADVRoundProgressViewType2* roundProgressLarge;
@property (strong, nonatomic) UILabel* titelLabel;
@property (strong, nonatomic) UIButton* aanpassenButton;
@property (strong, nonatomic) UIButton* addMoneyButton;
@property (strong, nonatomic) UIButton *removeMoneyButton;


- (id)initWithDoelModel:(DoelModel*)model;

@end
