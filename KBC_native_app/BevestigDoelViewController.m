//
//  BevestigDoelViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "BevestigDoelViewController.h"

@interface BevestigDoelViewController ()

@end

@implementation BevestigDoelViewController

@synthesize startButton = _startButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"nieuwDoelBevestigenBG" andType:@"png" andDirectory:@"images"];
     UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
     [self.view addSubview:backgroundImageView];

    //Start button
    UIImage *startButtonImage = [ImageFactory createImageWithName:@"startDoelButton" andType:@"png" andDirectory:@"images"];
    self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.startButton setImage:startButtonImage forState:UIControlStateNormal];
    self.startButton.frame = CGRectMake(72, 340, startButtonImage.size.width, startButtonImage.size.height);
    [self.startButton addTarget:self action:@selector(saveNewGoal:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.startButton];

}



-(void)saveNewGoal:(UIButton*)sender{
    NSLog(@"Save the new goal in the DB");
    //Get the view controller that is 2 step behind
    UIViewController *doelsparenVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 3];
    
    [self.navigationController popToViewController:doelsparenVC animated:YES];

}

/****************************************************************************************************************/
// viewWillAppear: hide back button, show navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide navbar button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_nieuwDoelBevestigen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
