//
//  IntroWizardViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "IntroWizardViewController.h"

@interface IntroWizardViewController ()

@end

@implementation IntroWizardViewController

@synthesize wizardScrollview = _wizardScrollview;
@synthesize wizardImageView = _wizardImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor cyanColor]];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set the UIScrollView with all the user's pet's
    self.wizardScrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 510)];
    [self.wizardScrollview setBounces:YES];
    [self.wizardScrollview setPagingEnabled:NO];
    [self.wizardScrollview setShowsHorizontalScrollIndicator:YES];
    self.wizardScrollview.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.wizardScrollview setCanCancelContentTouches:NO];
    
    self.wizardScrollview.delegate = self;
    [self.view addSubview:self.wizardScrollview];
    
    //Add wizard
    UIImage* wizardImage = [ImageFactory createImageWithName:@"wizard" andType:@"png" andDirectory:@"images"];
    self.wizardImageView = [[UIImageView alloc] initWithImage:wizardImage];
    [self.wizardScrollview addSubview:self.wizardImageView];
    
    //Bevestig button
    UIImage *bevestigButtonImage = [ImageFactory createImageWithName:@"bevestigButton" andType:@"png" andDirectory:@"images"];
    self.bevestigButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bevestigButton setImage:bevestigButtonImage forState:UIControlStateNormal];
    self.bevestigButton.frame = CGRectMake(72, 1000, bevestigButtonImage.size.width, bevestigButtonImage.size.height);
    [self.bevestigButton addTarget:self action:@selector(logTheUserIn:) forControlEvents:UIControlEventTouchUpInside];
    [self.wizardScrollview addSubview:self.bevestigButton];

    
    self.wizardScrollview.contentSize = CGSizeMake(self.wizardScrollview.frame.size.width, 1100);
}


-(void)logTheUserIn:(UIButton*)sender{
    //Log the user in
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGIN" object:nil];    
}


/****************************************************************************************************************/
// viewWillAppear: Hide the back button and show the navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
