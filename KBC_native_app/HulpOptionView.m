//
//  HulpOptionView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "HulpOptionView.h"

@implementation HulpOptionView

@synthesize hulpImage = _hulpImage;
@synthesize hulpOptionModel = _hulpOptionModel;
@synthesize hulpOptionImageView = _hulpOptionImageView;

- (id)initWithFrame:(CGRect)frame andHulpOptionModel:(HulpOptionModel*)hulpOptionModel
{
    self = [super initWithFrame:frame];
    if (self) {
        //Set the background color
        //[self setBackgroundColor:[UIColor cyanColor]];
        
        //Set the model
        self.hulpOptionModel = hulpOptionModel;
        
        //Set the image
        self.hulpImage = [ImageFactory createImageWithName:self.hulpOptionModel.nameImage andType:@"png" andDirectory:@"images"];
        self.hulpOptionImageView = [[UIImageView alloc] initWithImage:self.hulpImage];
        [self.hulpOptionImageView setFrame:CGRectMake(0, 10, self.hulpImage.size.width, self.hulpImage.size.height)];
        [self addSubview:self.hulpOptionImageView];
    }
    return self;
}



@end
