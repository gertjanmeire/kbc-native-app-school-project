//
//  DoelRowView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoelModel.h"

@interface DoelRowView : UIView

@property (strong, nonatomic) DoelModel* doelModel;

@property (strong, nonatomic) UILabel* titleLabel;
@property (strong, nonatomic) UIProgressView* progressBar;
@property (strong, nonatomic) UIImageView* clockImageView;
@property (strong, nonatomic) UILabel* deadlineLabel;
@property (strong, nonatomic) UILabel* savedAmountLabel;

- (id)initWithDoel:(DoelModel*)model;

@end
