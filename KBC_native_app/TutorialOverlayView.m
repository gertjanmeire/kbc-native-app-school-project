//
//  TutorialOverlayView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "TutorialOverlayView.h"

@implementation TutorialOverlayView

-(id)initWithFrame:(CGRect)frame andBackgroundImage:(UIImage*)backgroundImage{
    self = [super initWithFrame:frame];
    if (self) {
        //Background image
        self.backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        self.backgroundImageView.hidden = YES;
        [self addSubview:self.backgroundImageView];
        
        //self.view.userInteractionEnabled = NO;
        int count = [[[UIApplication sharedApplication]windows]count];
        [[[[UIApplication sharedApplication] windows] objectAtIndex:count-1]addSubview:self];
        [self showOverlay];

    }
    return self;
}



-(void)showOverlay{
    self.backgroundImageView.hidden = NO;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideOverlay:)];
    //[self.tutorialView addGestureRecognizer:singleTap];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:singleTap];
}


-(void)hideOverlay:(UITapGestureRecognizer*)sender{
    //self.backgroundImageView.hidden = YES;
    [self removeFromSuperview];
}



@end
