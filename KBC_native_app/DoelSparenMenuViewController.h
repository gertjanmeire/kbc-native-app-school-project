//
//  DoelSparenMenuViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface DoelSparenMenuViewController : UIViewController

@property (nonatomic, assign) IIViewDeckNavigationControllerBehavior behavior;
@property (strong, nonatomic) UIButton* mijnDOelenButton;
@property (strong, nonatomic) UIButton* nieuwDoelButton;



@end
