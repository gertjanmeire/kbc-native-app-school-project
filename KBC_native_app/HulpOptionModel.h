//
//  HulpOptionModel.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HulpOptionModel : NSObject

//Properties
@property (strong, nonatomic) NSString* nameImage;
@property (strong, nonatomic) NSString* detailImage;

//Constructors
- (id)initWithName:(NSString*)nameImage andWithDetail:(NSString*)detailImage;

@end
