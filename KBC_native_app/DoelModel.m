//
//  DoelModel.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "DoelModel.h"

@implementation DoelModel

@synthesize doelID = _doelID;
@synthesize doelNaam = _doelNaam;
@synthesize doelBedrag = _doelBedrag;
@synthesize gespaardBedrag = _gespaardBedrag;
@synthesize deadline = _deadline;
@synthesize userID = _userID;

- (id)initWithDoelID:(NSNumber*)doelID andDoelNaam:(NSString*)doelNaam andDoelBedrag:(NSNumber*)doelBedrag andGespaardBedrag:(NSNumber*)gespaardBedrag andDeadline:(NSString*)deadline andUserID:(NSNumber*)userID
{
    self = [super init];
    if (self) {
        self.doelID = doelID;
        self.doelNaam = doelNaam;
        self.doelBedrag = doelBedrag;
        self.gespaardBedrag = gespaardBedrag;
        self.deadline = deadline;
        self.userID = userID;
    }
    return self;
}

@end
