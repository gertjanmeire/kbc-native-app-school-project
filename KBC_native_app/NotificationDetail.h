//
//  NotificationDetail.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 27/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationDetail : UIView

@property (strong, nonatomic) UIButton* postButton;

@end
