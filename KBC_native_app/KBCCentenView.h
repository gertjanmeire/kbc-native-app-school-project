//
//  KBCCentenView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBCCentenView : UIView

@property (strong, nonatomic) UILabel* kbcCentenLabel;
@property (strong, nonatomic) UIImageView* kbcCenterImageView;

-(void)changeLabel:(NSString*)labelText;

@end
