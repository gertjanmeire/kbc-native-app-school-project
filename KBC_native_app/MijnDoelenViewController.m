//
//  MijnDoelenViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "MijnDoelenViewController.h"
#import "DoelRowView.h"
#import "MijnDoelDetailViewController.h"

#import "DoelModel.h"

@interface MijnDoelenViewController ()

@end

@implementation MijnDoelenViewController

@synthesize mijnDoelenPicker = _mijnDoelenPicker;

@synthesize doelenArr = _doelenArr;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];
    }
    return self;
}



-(void)getDoelen{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getDoelen" arguments:[NSArray arrayWithObjects: [[AppModel sharedAppModel].userInfo valueForKey:@"id"] ,nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}


- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    self.doelenArr = [[NSArray alloc] initWithArray:arr];
    
    NSLog(@"doelen count %d", self.doelenArr.count);
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= self.doelenArr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in self.doelenArr) {
        NSLog(@"Item name: %@", item);
        CGFloat y = i * 145;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber* doelID = (NSNumber*)[item valueForKey:@"id"];
        NSString* doelNaam = (NSString*)[item valueForKey:@"doelNaam"];
        NSNumber* doelBedrag = (NSNumber*)[item valueForKey:@"doelBedrag"];
        NSNumber* gespaardBedrag = (NSNumber*)[item valueForKey:@"gespaardBedrag"];
        NSString* deadline = (NSString*)[item valueForKey:@"deadline"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        //TransactieModel *transactieModel = [[TransactieModel alloc] initWithTransactieID:transactieID andRekeningNaam:rekeningNaam andSaldo:saldo andUserID:userID andDate:datum andTitle:titel andAmount:amount andRekeningID:rekeningID];
        DoelModel *doelModel = [[DoelModel alloc] initWithDoelID:doelID andDoelNaam:doelNaam andDoelBedrag:doelBedrag andGespaardBedrag:gespaardBedrag andDeadline:deadline andUserID:userID];
        
        //Rekening view aanmaken in de scrollview
        DoelRowView *doelRowView = [[DoelRowView alloc]  initWithDoel:doelModel];
        [doelRowView setFrame:CGRectMake(0, y,self.mijnDoelenPicker.frame.size.width, 145)];
        [self.mijnDoelenPicker addSubview:doelRowView];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mijnDoelSelected:)];
        [doelRowView addGestureRecognizer:singleTap];
     
        i++;
    }
    
    self.mijnDoelenPicker.contentSize = CGSizeMake(self.mijnDoelenPicker.frame.size.width, 145 *viewcount);
        
}

- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}



/****************************************************************************************************************/
// viewDidLoad: add the graphical components for MijnDoelen 
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];

    //Set the UIScrollView with all the user's pet's
    self.mijnDoelenPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 10, 320, 510)];
    [self.mijnDoelenPicker setBounces:YES];
    [self.mijnDoelenPicker setPagingEnabled:YES];
    [self.mijnDoelenPicker setShowsHorizontalScrollIndicator:YES];
    self.mijnDoelenPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.mijnDoelenPicker setCanCancelContentTouches:NO];
    
    self.mijnDoelenPicker.delegate = self;
    [self.view addSubview:self.mijnDoelenPicker];
    
}


-(void)mijnDoelSelected:(UITapGestureRecognizer*)sender{
    DoelRowView* selectedView = (DoelRowView*)sender.view;
    
    MijnDoelDetailViewController* mijnDoelDetailVC = [[MijnDoelDetailViewController alloc] initWithDoelModel:selectedView.doelModel];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:mijnDoelDetailVC];
}


/****************************************************************************************************************/
// viewWillAppear: hide back button, show navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide navbar button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbarMijnDoelen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Background image
    /*UIImage *backgroundImage = [ImageFactory createImageWithName:@"zoekKantoorBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];*/
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [self getDoelen];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
