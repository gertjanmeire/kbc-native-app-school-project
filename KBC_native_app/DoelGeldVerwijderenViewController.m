//
//  DoelGeldVerwijderenViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "DoelGeldVerwijderenViewController.h"
#import "RekeningPickerRowView.h"

@interface DoelGeldVerwijderenViewController ()

@end

@implementation DoelGeldVerwijderenViewController

@synthesize doelModel = _doelModel;

@synthesize rekeningenArr = _rekeningenArr;

@synthesize headerImageView = _headerImageView;
@synthesize rekeningPicker = _rekeningPicker;
@synthesize pageControl = _pageControl;
@synthesize bedragField = _bedragField;
@synthesize schrijfOverButton = _schrijfOverButton;

- (id)initWithDoelModel:(DoelModel*)model
{
    self = [super init];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor greenColor]];
        
        //Set model
        self.doelModel = model;
        
        NSLog(@"Doel naam: %@", self.doelModel.doelNaam);
        
        //Background image
        UIImage *backgroundImage = [ImageFactory createImageWithName:@"geldToevoegenBG" andType:@"png" andDirectory:@"images"];
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        [self.view addSubview:backgroundImageView];
        
        //Header image
        UIImage* headerImage = [ImageFactory createImageWithName:@"geldVerwijderenHeader" andType:@"png" andDirectory:@"images"];
        self.headerImageView = [[UIImageView alloc] initWithImage:headerImage];
        [self.headerImageView setFrame:CGRectMake(0, 7, 320, 108)];
        [self.view addSubview:self.headerImageView];
        
        //Set the UIScrollView with all the user's pet's
        self.rekeningPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 157, 320, 160)];
        [self.rekeningPicker setBounces:YES];
        [self.rekeningPicker setPagingEnabled:YES];
        [self.rekeningPicker setShowsHorizontalScrollIndicator:YES];
        self.rekeningPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
        [self.rekeningPicker setCanCancelContentTouches:NO];
        
        self.rekeningPicker.delegate = self;
        [self.view addSubview:self.rekeningPicker];
        
        
        //Bedrag field
        UIImage *nameFieldImage = [ImageFactory createImageWithName:@"aanpassingenBedragTextfieldBG" andType:@"png" andDirectory:@"images"];
        self.bedragField = [[CustomTextField alloc] initWithFrame:CGRectMake(50, 356, nameFieldImage.size.width, nameFieldImage.size.height)];
        [self.bedragField setBackground:nameFieldImage];
        [self.bedragField setText:@""];
        [self.bedragField setPlaceholder:@"Bedrag"];
        [self.bedragField setDelegate:self];
        [self.bedragField setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.bedragField changeLeftMargin:30];
        [self.view addSubview:self.bedragField];
        
        //Schrijf over button
        UIImage *schrijfOverButtonImage = [ImageFactory createImageWithName:@"crediterenButton" andType:@"png" andDirectory:@"images"];
        self.schrijfOverButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.schrijfOverButton setImage:schrijfOverButtonImage forState:UIControlStateNormal];
        self.schrijfOverButton.frame = CGRectMake(72, 425, schrijfOverButtonImage.size.width, schrijfOverButtonImage.size.height);
        [self.schrijfOverButton addTarget:self action:@selector(crediteren:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.schrijfOverButton];
        
        
    }
    return self;
}


-(void)crediteren:(UIButton*)sender{
    [self backButton:nil];
}

-(void)getRekeningen{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getRekeningenForUser" arguments:[NSArray arrayWithObjects: user_id ,nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}



- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    self.rekeningenArr = [[NSArray alloc] initWithArray:arr];
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= self.rekeningenArr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in self.rekeningenArr) {
        NSLog(@"Item name: %@", item);
        CGFloat x = i * self.rekeningPicker.frame.size.width;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber *rekeningID = (NSNumber *)[item valueForKey:@"id"];
        NSString *rekeningNaam = (NSString *)[item valueForKey:@"rekeningNaam"];
        NSNumber *saldo = (NSNumber *)[item valueForKey:@"saldo"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        RekeningModel *rekeningModel = [[RekeningModel alloc] initWithRekeningNaam:rekeningNaam andID:rekeningID andSaldo:saldo andUserID:userID];
        
        //Rekening view aanmaken in de scrollview
        DoelsparenRekeningView *rekeningPickerRowView = [[DoelsparenRekeningView alloc] initWithFrame:CGRectMake(0, 0, self.rekeningPicker.bounds.size.width, self.rekeningPicker.bounds.size.height) andRekeningModel:rekeningModel];
        [rekeningPickerRowView setFrame:CGRectMake(x, 0,self.view.frame.size.width, self.view.frame.size.height)];
        [self.rekeningPicker addSubview:rekeningPickerRowView];
        
        i++;
    }
    
    self.rekeningPicker.contentSize = CGSizeMake(self.rekeningPicker.frame.size.width * viewcount, self.rekeningPicker.bounds.size.height);
    
    
	//Add the page control
	self.pageControl = [[DDPageControl alloc] init];
	[self.pageControl setCenter: CGPointMake(self.view.center.x,self.view.center.y + 80)];
	[self.pageControl setNumberOfPages: viewcount];
	[self.pageControl setCurrentPage: 0];
	[self.pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged];
	[self.pageControl setDefersCurrentPageDisplay: YES];
    [self.pageControl setType:DDPageControlTypeOnFullOffFull];
	[self.pageControl setOnColor: [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
	[self.pageControl setOffColor: [UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 10.0f];
	[self.view addSubview: self.pageControl];
    
}


-(void)pageControlClicked:(DDPageControl*)sender{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
	[self.rekeningPicker setContentOffset: CGPointMake(self.rekeningPicker.bounds.size.width * thePageControl.currentPage , self.rekeningPicker.contentOffset.y) animated: YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (self.pageControl.currentPage != nearestNumber)
	{
		self.pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[self.pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[self.pageControl updateCurrentPageDisplay] ;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


/****************************************************************************************************************/
// viewWillAppear: hide back button, show navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide navbar button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_geldVerwijderen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [self getRekeningen];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
