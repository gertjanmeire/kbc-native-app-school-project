//
//  TransactieModel.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "TransactieModel.h"

@implementation TransactieModel

@synthesize transactieID = _transactieID;
@synthesize rekeningNaam = _rekeningNaam;
@synthesize saldo = _saldo;
@synthesize userID = _userID;
@synthesize betaalmethode = _betaalmethode;
@synthesize datum = _datum;
@synthesize titel = _titel;
@synthesize amount = _amount;
@synthesize plaats = _plaats;
@synthesize gemeente = _gemeente;
@synthesize rekeningID = _rekeningID;

- (id)initWithTransactieID:(NSNumber*)transactieID andRekeningNaam:(NSString*)rekeningNaam andSaldo:(NSNumber*)saldo andUserID:(NSNumber*)userID andBetaalmethode:(NSString*)betaalmethode andDate:(NSString*)datum  andTitle:(NSString*)titel andAmount:(NSNumber*)amount andPlaats:(NSString*)plaats andGemeente:(NSString*)gemeente andRekeningID:(NSNumber*)rekeningID
{
    self = [super init];
    if (self) {
        self.transactieID = transactieID;
        self.rekeningNaam = rekeningNaam;
        self.saldo = saldo;
        self.userID = userID;
        self.betaalmethode = betaalmethode;
        self.datum = datum;
        self.titel = titel;
        self.amount = amount;
        self.plaats = plaats;
        self.gemeente = gemeente;
        self.rekeningID = rekeningID;
    }
    return self;
}

@end
