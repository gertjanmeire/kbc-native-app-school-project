//
//  OnlineBankingMenuViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface OnlineBankingMenuViewController : UIViewController

@property (strong, nonatomic) UIButton* onlineBankingMenuButton;
@property (nonatomic, assign) IIViewDeckNavigationControllerBehavior behavior;

@end
