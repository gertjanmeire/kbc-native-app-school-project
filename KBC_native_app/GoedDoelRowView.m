//
//  GoedDoelRowView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "GoedDoelRowView.h"

@implementation GoedDoelRowView

@synthesize titleLabel = _titleLabel;
@synthesize detailButton = _detailButton;
@synthesize currencyLabel = _currencyLabel;
@synthesize donatiePicker = _donatiePicker;
@synthesize donateSlider = _donateSlider;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title
{
    self = [super initWithFrame:frame];
    if (self) {
        //Background
        UIImage *backgroundImage = [ImageFactory createImageWithName:@"goedDoelRowViewBG" andType:@"png" andDirectory:@"images"];
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        [self addSubview:backgroundImageView];

        
        //Title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 260, 30)];
        [self.titleLabel setText:title];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:27]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self addSubview:self.titleLabel];
        
        //Slider images
        UIImage * sliderLeftTrackImage = [ImageFactory createImageWithName:@"sliderLeftTrack" andType:@"png" andDirectory:@"images"];
         UIImage * sliderRightTrackImage = [ImageFactory createImageWithName:@"sliderRightTrack" andType:@"png" andDirectory:@"images"];
         UIImage * thumbImage = [ImageFactory createImageWithName:@"thumb" andType:@"png" andDirectory:@"images"];
         
         //Donate slider
         self.donateSlider = [[UISlider alloc] initWithFrame:CGRectMake(10, 195, 260, 50)];
         [self.donateSlider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
         [self.donateSlider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
         [self.donateSlider setThumbImage:thumbImage forState:UIControlStateNormal];
         self.donateSlider.minimumValue = 1;
         self.donateSlider.maximumValue = 125;
         [self.donateSlider setValue:0 animated:YES];
         [self.donateSlider addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchDragInside];
         [self addSubview:self.donateSlider];
         
         //Round progress view
         UIColor *tintColor = [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0];
         self.roundProgressLarge = [[ADVRoundProgressView alloc] initWithFrame:CGRectMake(57, 40, 166, 166) andImage:[ImageFactory createImageWithName:@"progress-circle-large" andType:@"png" andDirectory:@"images"]];
         [self.roundProgressLarge setTintColor:tintColor];
         self.roundProgressLarge.progress = 1;
         [self addSubview:self.roundProgressLarge];
        
        
        //Detail button
        UIImage* detailButtonImage = [ImageFactory createImageWithName:@"detailButton" andType:@"png" andDirectory:@"images"];
        self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.detailButton setImage:detailButtonImage forState:UIControlStateNormal];
        self.detailButton.frame = CGRectMake(257, 15, detailButtonImage.size.width, detailButtonImage.size.height);
        [self.detailButton addTarget:self action:@selector(showDetailView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.detailButton];
        


    }
    return self;
}


-(void)changeValue:(UISlider*)sender{
    NSLog(@"%f", sender.value);
    self.roundProgressLarge.progress = sender.value;
}

/****************************************************************************************************************/
// showDetailView: show the detailview with the information about the selected goal
/****************************************************************************************************************/
-(void)showDetailView:(UIButton*)sender{
    NSLog(@"Show detail view");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOW_DETAIL_VIEW_OF_GOAL" object:nil];
}


@end
