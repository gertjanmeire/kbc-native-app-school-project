//
//  LoginHandtekeningViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginHandtekeningViewController : UIViewController

@property (strong, nonatomic) UIButton* bevestigButton;
@property (strong, nonatomic) UIButton* tutorialButton;

@end
