//
//  MenuViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "MenuViewController.h"
#import "ScashingMenuViewController.h"
#import "OnlineBankingMenuViewController.h"
#import "DoelSparenMenuViewController.h"
#import "HandigMenuViewController.h"
#import "ZoekKantoorInAppViewController.h"
#import "HulpInAppViewController.h"


@interface MenuViewController () <IIViewDeckControllerDelegate>{
    IIViewDeckCenterHiddenInteractivity _centerHidden;
    IIViewDeckNavigationControllerBehavior _navBehavior;
}

@end

@implementation MenuViewController

@synthesize mainMenuButton = _mainMenuButton;
@synthesize logoutButton = _logoutButton;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _centerHidden = IIViewDeckCenterHiddenUserInteractive;
        _navBehavior = IIViewDeckNavigationControllerContained;
        
        [self.view setBackgroundColor:[UIColor blueColor]];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the main menu items
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Background image
     UIImage *backgroundImage = [ImageFactory createImageWithName:@"mainMenuSlideBG" andType:@"png" andDirectory:@"images"];
     UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
     [self.view addSubview:backgroundImageView];
    
    //Add the buttons
    float xPos = 35;
    float yPos = 65;
    
    for(int i=1; i<=6; i++){
        NSString *buttonName = [NSString stringWithFormat:@"mainMenuButton%d", i];
        if(i%2 == 0 || i == 1){
            UIImage* mainMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
            self.mainMenuButton.frame = CGRectMake(xPos, yPos, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
            [self.mainMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.mainMenuButton addTarget:self action:@selector(gotoPage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.mainMenuButton];
            
            xPos += 145;
        }else if(i%2 == 1 && i != 1){
            xPos = 35;
            yPos += 140;
            
            UIImage* mainMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
            self.mainMenuButton.frame = CGRectMake(xPos, yPos, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
            [self.mainMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.mainMenuButton addTarget:self action:@selector(gotoPage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.mainMenuButton];
            
            xPos += 145;
        }
    }
    
    UIImage *logoutButtonImage = [ImageFactory createImageWithName:@"logoutButton" andType:@"png" andDirectory:@"images"];
    self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.logoutButton setImage:logoutButtonImage forState:UIControlStateNormal];
    self.logoutButton.frame = CGRectMake(30, 488, logoutButtonImage.size.width, logoutButtonImage.size.height);
    [self.logoutButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.logoutButton];

}


/****************************************************************************************************************/
// gotoPage: go to one of the menu items
/****************************************************************************************************************/
-(void)gotoPage:(UIButton*)sender{
    UIButton* senderButton = (UIButton*)sender;
    NSString* compareToString = senderButton.currentTitle;
    
    if([compareToString isEqualToString:@"mainMenuButton1"]){
        ScashingMenuViewController *scashingMenuVC = [[ScashingMenuViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:scashingMenuVC];
        
    }else if([compareToString isEqualToString:@"mainMenuButton2"]){
        OnlineBankingMenuViewController *onlineBankingMenuVC = [[OnlineBankingMenuViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:onlineBankingMenuVC];

    }else if([compareToString isEqualToString:@"mainMenuButton3"]){
        DoelSparenMenuViewController *doelSparenMenuVC = [[DoelSparenMenuViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:doelSparenMenuVC];

    }else if([compareToString isEqualToString:@"mainMenuButton4"]){
        HandigMenuViewController *handigMenuVC = [[HandigMenuViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:handigMenuVC];

    }else if([compareToString isEqualToString:@"mainMenuButton5"]){
        ZoekKantoorInAppViewController *zoekenVC = [[ZoekKantoorInAppViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:zoekenVC];

    }else if([compareToString isEqualToString:@"mainMenuButton6"]){
        HulpInAppViewController *hulpVC = [[HulpInAppViewController alloc] init];
        [self.viewDeckController rightViewPushViewControllerOverCenterController:hulpVC];

    }
    [self.viewDeckController closeOpenView];
}

-(void)viewDeckController:(IIViewDeckController *)viewDeckController didBounceViewSide:(IIViewDeckSide)viewDeckSide closingController:(UIViewController *)closingController{
    NSLog(@"DIT IS NOG EEN TEST");
}

- (void)viewDeckController:(IIViewDeckController *)viewDeckController didBounceWithClosingController:(UIViewController*)openController{
    NSLog(@"TEEEEST");
}


/****************************************************************************************************************/
// logout: logout the user and go back to the login screen
/****************************************************************************************************************/
-(void)logout:(UIButton*)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
}


/****************************************************************************************************************/
// viewDidAppear: Add the navigation bar and hide the back buttons etc...
/****************************************************************************************************************/
-(void)viewDidAppear:(BOOL)animated{
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_taal" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
