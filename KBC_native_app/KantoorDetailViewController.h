//
//  KantoorDetailViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 25/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface KantoorDetailViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIButton* belButton;
@property (strong, nonatomic) UIButton* emailButton;

@end
