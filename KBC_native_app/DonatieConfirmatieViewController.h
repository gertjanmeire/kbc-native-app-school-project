//
//  DonatieConfirmatieViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 27/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface DonatieConfirmatieViewController : UIViewController

@property (strong, nonatomic) UIButton* terugButton;

@end
