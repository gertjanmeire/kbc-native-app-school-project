//
//  LoginHandtekeningViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "LoginHandtekeningViewController.h"

@interface LoginHandtekeningViewController ()

@end

@implementation LoginHandtekeningViewController

@synthesize bevestigButton = _bevestigButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"loginHandtekeningBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    //Bevestig button
    UIImage *bevestigButtonImage = [ImageFactory createImageWithName:@"bevestigButton" andType:@"png" andDirectory:@"images"];
    self.bevestigButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bevestigButton setImage:bevestigButtonImage forState:UIControlStateNormal];
    self.bevestigButton.frame = CGRectMake(72, 370, bevestigButtonImage.size.width, bevestigButtonImage.size.height);
    [self.bevestigButton addTarget:self action:@selector(logTheUserIn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.bevestigButton];

    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tutorialButton];

}


-(void)logTheUserIn:(UIButton*)sender{
    //Log the user in
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGIN" object:nil];
}


/****************************************************************************************************************/
// viewWillAppear: Hide the back button and show the navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 25, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Navigation bar image
    self.navigationController.navigationBar.hidden = NO;
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbarLoginHandtekening" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// goBackToLogin: go back to login for your first time after registration
/****************************************************************************************************************/
-(void)goBackToLogin:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayLoginHandtekening" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
