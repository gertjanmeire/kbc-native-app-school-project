//
//  ScashingMainMenuViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ScashingMenuViewController.h"
#import "ScashingReceiveViewController.h"
#import "ScashingSendViewController.h"
#import "ScashPotViewController.h"

@interface ScashingMenuViewController ()

@end

@implementation ScashingMenuViewController

@synthesize receiveMoneyButton = _receiveMoneyButton;
@synthesize sendMoneyButton = _sendMoneyButton;
@synthesize scashPotButton = _scashPotButton;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor redColor]];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"scashingMenuBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];


    //Receive money button
    UIImage* receiveMoneyButtonImage = [ImageFactory createImageWithName:@"receiveMoneyButton" andType:@"png" andDirectory:@"images"];
    self.receiveMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.receiveMoneyButton setImage:receiveMoneyButtonImage forState:UIControlStateNormal];
    self.receiveMoneyButton.frame = CGRectMake(7, 25, receiveMoneyButtonImage.size.width, receiveMoneyButtonImage.size.height);
    [self.receiveMoneyButton addTarget:self action:@selector(gotoReceiveMoney:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.receiveMoneyButton];
    
    //Send money button
    UIImage* sendMoneyButtonImage = [ImageFactory createImageWithName:@"sendMoneyButton" andType:@"png" andDirectory:@"images"];
    self.sendMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendMoneyButton setImage:sendMoneyButtonImage forState:UIControlStateNormal];
    self.sendMoneyButton.frame = CGRectMake(160, 25, sendMoneyButtonImage.size.width, sendMoneyButtonImage.size.height);
    [self.sendMoneyButton addTarget:self action:@selector(gotoSendMoney:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendMoneyButton];
    
    //ScashPotButton 
    UIImage* scashPotButtonImage = [ImageFactory createImageWithName:@"scashPotButton" andType:@"png" andDirectory:@"images"];
    self.scashPotButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.scashPotButton setImage:scashPotButtonImage forState:UIControlStateNormal];
    self.scashPotButton.frame = CGRectMake(19, 306, scashPotButtonImage.size.width, scashPotButtonImage.size.height);
    [self.scashPotButton addTarget:self action:@selector(gotoScashPot:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.scashPotButton];
    
    
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tutorialButton];
    
}

-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayScashingMenu" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}




/****************************************************************************************************************/
// gotoReceiveMoney: go to the receive money view
/****************************************************************************************************************/
-(void)gotoReceiveMoney:(UIButton*)sender{
    NSLog(@"Go to receive money");
    ScashingReceiveViewController *scashingReceiveVC = [[ScashingReceiveViewController alloc] init];
    [self.navigationController pushViewController:scashingReceiveVC animated:YES];
}


/****************************************************************************************************************/
// gotoSendMoney: go to the send money view
/****************************************************************************************************************/
-(void)gotoSendMoney:(UIButton*)sender{
    NSLog(@"Go to send money");
    ScashingSendViewController *scashingSendVC = [[ScashingSendViewController alloc] init];
    [self.navigationController pushViewController:scashingSendVC animated:YES];
}


/****************************************************************************************************************/
// gotoScashPot: go to the scash pot view
/****************************************************************************************************************/
-(void)gotoScashPot:(UIButton*)sender{
    NSLog(@"Go to scash pot");
    ScashPotViewController *scashpotVC = [[ScashPotViewController alloc] init];
    [self.navigationController pushViewController:scashpotVC animated:YES];
}


/****************************************************************************************************************/
// viewWillAppear: hide the back button and show the navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_scashen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];

    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
