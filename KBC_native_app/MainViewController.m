//
//  MainViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "MainViewController.h"
#import "LoginViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize nlButton = _nlButton;
@synthesize frButton = _frButton;
@synthesize enButton = _enButton;
@synthesize deButton = _deButton;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor redColor]];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the language selection buttons
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];

    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"languageSelectionBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];

    
    //Language buttons
    UIImage *nlButtonImage = [ImageFactory createImageWithName:@"nlButton" andType:@"png" andDirectory:@"images"];
    self.nlButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.nlButton setImage:nlButtonImage forState:UIControlStateNormal];
    self.nlButton.frame = CGRectMake(37, 167, nlButtonImage.size.width, nlButtonImage.size.height);
    [self.nlButton addTarget:self action:@selector(gotoLoginView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nlButton];
    
    UIImage *frButtonImage = [ImageFactory createImageWithName:@"frButton" andType:@"png" andDirectory:@"images"];
    self.frButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.frButton setImage:frButtonImage forState:UIControlStateNormal];
    self.frButton.frame = CGRectMake(257, 162, frButtonImage.size.width, frButtonImage.size.height);
    [self.frButton addTarget:self action:@selector(gotoLoginView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.frButton];
    
    UIImage *enButtonImage = [ImageFactory createImageWithName:@"enButton" andType:@"png" andDirectory:@"images"];
    self.enButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.enButton setImage:enButtonImage forState:UIControlStateNormal];
    self.enButton.frame = CGRectMake(247, 241, enButtonImage.size.width, enButtonImage.size.height);
    [self.enButton addTarget:self action:@selector(gotoLoginView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.enButton];
    
    UIImage *deButtonImage = [ImageFactory createImageWithName:@"deButton" andType:@"png" andDirectory:@"images"];
    self.deButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deButton setImage:deButtonImage forState:UIControlStateNormal];
    self.deButton.frame = CGRectMake(27, 243, deButtonImage.size.width, deButtonImage.size.height);
    [self.deButton addTarget:self action:@selector(gotoLoginView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.deButton];    
}


/****************************************************************************************************************/
// gotoLoginView: Go to the LoginViewController
/****************************************************************************************************************/
-(void)gotoLoginView:(UIButton *)sender{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:NO];
}


/****************************************************************************************************************/
// viewWillAppear: hide back button and show navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];

    //Navigation bar image
    self.navigationController.navigationBar.hidden = YES;

    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
