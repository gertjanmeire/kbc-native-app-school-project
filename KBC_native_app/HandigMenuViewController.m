//
//  HandigMenuViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "HandigMenuViewController.h"

@interface HandigMenuViewController ()

@end

@implementation HandigMenuViewController

@synthesize step1View = _step1View;
@synthesize startButton = _startButton;

@synthesize step2View = _step2View;


@synthesize step3View = _step3View;
@synthesize mainMenuButton = _mainMenuButton;

@synthesize step4View = _step4View;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor yellowColor]];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad:  add the graphical components for the HandigMenuViewController
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Step 1
    self.step1View = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,510)];
    [self.view addSubview:self.step1View];
    
    //Background image step 1 view
    UIImage *backgroundImage1 = [ImageFactory createImageWithName:@"howtoStep1BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView1 = [[UIImageView alloc] initWithImage:backgroundImage1];
    [self.step1View addSubview:backgroundImageView1];
    
    //Aanmelden
    UIImage *startButtonImage = [ImageFactory createImageWithName:@"startButton" andType:@"png" andDirectory:@"images"];
    self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.startButton setImage:startButtonImage forState:UIControlStateNormal];
    self.startButton.frame = CGRectMake(72, 400, startButtonImage.size.width, startButtonImage.size.height);
    self.startButton.userInteractionEnabled = YES;
    [self.startButton addTarget:self action:@selector(gotoStep2:) forControlEvents:UIControlEventTouchUpInside];
    [self.step1View addSubview:self.startButton];


    
    //Step 2
    self.step2View = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,510)];
    self.step2View.hidden = YES;
    [self.view addSubview:self.step2View];
    
    //Background image step 2 view
    UIImage *backgroundImage2 = [ImageFactory createImageWithName:@"howtoStep2BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView2 = [[UIImageView alloc] initWithImage:backgroundImage2];
    [self.step2View addSubview:backgroundImageView2];
    
    UITapGestureRecognizer* step2Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoStep3:)];
    [self.step2View addGestureRecognizer:step2Tap];
    
    //Step 3
    self.step3View = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,510)];
    self.step3View.hidden = YES;
    [self.view addSubview:self.step3View];
    
    //Background image step 3 view
    UIImage *backgroundImage3 = [ImageFactory createImageWithName:@"howtoStep3BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView3 = [[UIImageView alloc] initWithImage:backgroundImage3];
    [self.step3View addSubview:backgroundImageView3];

    
    float xPos = 20;
    float yPos = 85;
    
    for(int i=1; i<=6; i++){
        NSString *buttonName = [NSString stringWithFormat:@"howtoMenuButton%d", i];
        if(i%2 == 0 || i == 1){
            UIImage* mainMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
            self.mainMenuButton.frame = CGRectMake(xPos, yPos, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
            [self.mainMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.mainMenuButton addTarget:self action:@selector(gotoStep4:) forControlEvents:UIControlEventTouchUpInside];
            [self.step3View addSubview:self.mainMenuButton];
            
            xPos += 145;
        }else if(i%2 == 1 && i != 1){
            xPos = 20;
            yPos += 140;
            
            UIImage* mainMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
            self.mainMenuButton.frame = CGRectMake(xPos, yPos, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
            [self.mainMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.mainMenuButton addTarget:self action:@selector(gotoStep4:) forControlEvents:UIControlEventTouchUpInside];
            [self.step3View addSubview:self.mainMenuButton];
            
            xPos += 145;
        }
    }

    
    //Step 4
    self.step4View = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,510)];
    self.step4View.hidden = YES;
    [self.view addSubview:self.step4View];
    
    //Background image step 4 view
    UIImage *backgroundImage4 = [ImageFactory createImageWithName:@"howtoStep4BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView4 = [[UIImageView alloc] initWithImage:backgroundImage4];
    [self.step4View addSubview:backgroundImageView4];
    
    
    

}


-(void)gotoStep2:(UIButton*)sender{
    self.step1View.hidden = YES;
    self.step2View.hidden = NO;
    self.step3View.hidden = YES;
    self.step4View.hidden = YES;
}

-(void)gotoStep3:(UIButton*)sender{
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = NO;
    self.step4View.hidden = YES;
}

-(void)gotoStep4:(UIButton*)sender{
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = YES;
    self.step4View.hidden = NO;
}

/****************************************************************************************************************/
// viewWillAppear: hide back button, show navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide navbar button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_howto" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Background image
    /*UIImage *backgroundImage = [ImageFactory createImageWithName:@"zoekKantoorBG" andType:@"png" andDirectory:@"images"];
     UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
     [self.view addSubview:backgroundImageView];*/
        
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
