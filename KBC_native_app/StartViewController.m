//
//  StartViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "StartViewController.h"
#import "MenuViewController.h"

#import "ScashingMenuViewController.h"
#import "OnlineBankingMenuViewController.h"
#import "DoelSparenMenuViewController.h"
#import "HandigMenuViewController.h"
#import "ZoekKantoorInAppViewController.h"
#import "HulpInAppViewController.h"



@interface StartViewController ()

@end

@implementation StartViewController

@synthesize logoutButton = _logoutButton;
@synthesize tutorialButton = _tutorialButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor brownColor]];
    }
    return self;
}

/****************************************************************************************************************/
// viewDidLoad: Add the main menu buttons
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image 
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"mainMenuBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];

    
    float xPos = 20;
    float yPos = 65;
    
    for(int i=1; i<=6; i++){
        NSString *buttonName = [NSString stringWithFormat:@"mainMenuButton%d", i];
        if(i%2 == 0 || i == 1){
            UIImage* mainMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
            self.mainMenuButton.frame = CGRectMake(xPos, yPos, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
            [self.mainMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.mainMenuButton addTarget:self action:@selector(gotoPage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.mainMenuButton];
            
            xPos += 145;
        }else if(i%2 == 1 && i != 1){
            xPos = 20;
            yPos += 140;
            
            UIImage* mainMenuButtonImage = [ImageFactory createImageWithName:buttonName andType:@"png" andDirectory:@"images"];
            self.mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
            self.mainMenuButton.frame = CGRectMake(xPos, yPos, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
            [self.mainMenuButton setTitle:buttonName forState:UIControlStateNormal];
            [self.mainMenuButton addTarget:self action:@selector(gotoPage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.mainMenuButton];
            
            xPos += 145;
        }
    }
    
    UIImage *logoutButtonImage = [ImageFactory createImageWithName:@"logoutButton" andType:@"png" andDirectory:@"images"];
    self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.logoutButton setImage:logoutButtonImage forState:UIControlStateNormal];
    self.logoutButton.frame = CGRectMake(20, 488, logoutButtonImage.size.width, logoutButtonImage.size.height);
    [self.logoutButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.logoutButton];
    
}



/****************************************************************************************************************/
// gotoPage: go to one of the menu items 
/****************************************************************************************************************/
-(void)gotoPage:(UIButton*)sender{
    UIButton* senderButton = (UIButton*)sender;
    NSString* compareToString = senderButton.currentTitle;

    //Set the sidemenu
    MenuViewController* menuVC = [[MenuViewController alloc] init];

    if([compareToString isEqualToString:@"mainMenuButton1"]){
        ScashingMenuViewController* scashingVC = [[ScashingMenuViewController alloc] init];
        
        self.deckController = [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:scashingVC] leftViewController:nil rightViewController:menuVC];
    }else if([compareToString isEqualToString:@"mainMenuButton2"]){
        OnlineBankingMenuViewController *onlineBankingMenuVC = [[OnlineBankingMenuViewController alloc] init];
        
        self.deckController = [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:onlineBankingMenuVC] leftViewController:nil rightViewController:menuVC];
    }else if([compareToString isEqualToString:@"mainMenuButton3"]){        
        DoelSparenMenuViewController *doelSparenMenuVC = [[DoelSparenMenuViewController alloc] init];
        
        self.deckController = [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:doelSparenMenuVC] leftViewController:nil rightViewController:menuVC];
    }else if([compareToString isEqualToString:@"mainMenuButton4"]){
        HandigMenuViewController *handigMenuVC = [[HandigMenuViewController alloc] init];
        
        self.deckController = [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:handigMenuVC] leftViewController:nil rightViewController:menuVC];
    }else if([compareToString isEqualToString:@"mainMenuButton5"]){
        NSLog(@"Goto menu item 5");
        ZoekKantoorInAppViewController *zoekenVC = [[ZoekKantoorInAppViewController alloc] init];
        
        self.deckController = [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:zoekenVC] leftViewController:nil rightViewController:menuVC];
    }else if([compareToString isEqualToString:@"mainMenuButton6"]){
        HulpInAppViewController *hulpVC = [[HulpInAppViewController alloc] init];
        
        self.deckController = [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:hulpVC] leftViewController:nil rightViewController:menuVC];
    }
    self.deckController.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    self.deckController.delegateMode = IIViewDeckDelegateAndSubControllers;
    self.deckController.rightSize = 30;
    
    [self presentViewController:self.deckController animated:YES completion:nil];
}


/****************************************************************************************************************/
// logout: logout the user and go back to the login screen
/****************************************************************************************************************/
-(void)logout:(UIButton*)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
}


-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
