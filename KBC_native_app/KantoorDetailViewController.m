//
//  KantoorDetailViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 25/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "KantoorDetailViewController.h"

@interface KantoorDetailViewController ()

@end

@implementation KantoorDetailViewController

@synthesize belButton = _belButton;
@synthesize emailButton = _emailButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor cyanColor]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"kantoorDetailBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    
    //Bel button
    UIImage *belButtonImage = [ImageFactory createImageWithName:@"belButton" andType:@"png" andDirectory:@"images"];
    self.belButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.belButton setImage:belButtonImage forState:UIControlStateNormal];
    self.belButton.frame = CGRectMake(37, 133, belButtonImage.size.width, belButtonImage.size.height);
    [self.belButton addTarget:self action:@selector(callNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.belButton];

    
    //Email button
    UIImage *emailButtonImage = [ImageFactory createImageWithName:@"emailButton" andType:@"png" andDirectory:@"images"];
    self.emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.emailButton setImage:emailButtonImage forState:UIControlStateNormal];
    self.emailButton.frame = CGRectMake(37, 198, emailButtonImage.size.width, emailButtonImage.size.height);
    [self.emailButton addTarget:self action:@selector(sendMail:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.emailButton];


}


-(void)callNumber:(UIButton*)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:0478410896"]];
}


-(void)sendMail:(UIButton*)sender{
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
    mailViewController.mailComposeDelegate = self;
    [mailViewController setSubject:[NSString stringWithFormat:@"Vraag door KBC klant"]];
    [mailViewController setMessageBody:@"" isHTML:NO];
    [self presentViewController:mailViewController animated:YES completion:nil];
}

/****************************************************************************************************************/
// viewDidAppear: hide the back button, show the navigation bar and show custom bar buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide the back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_kantoorDetail" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    [super viewWillAppear:animated];
}




/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult*)result error:(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
