//
//  HulpInAppViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 25/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "HulpInAppViewController.h"
#import "HulpOptionView.h"
#import "HulpDetailViewController.h"

#import "HulpOptionModel.h"


@interface HulpInAppViewController ()

@end

@implementation HulpInAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];
    }
    return self;
}

/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"hulpInNoodBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    //Set the UIScrollView with all the user's pet's
    self.hulpPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(25, 38, 270, 415)];
    [self.hulpPicker setBounces:YES];
    [self.hulpPicker setPagingEnabled:YES];
    [self.hulpPicker setShowsHorizontalScrollIndicator:YES];
    self.hulpPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.hulpPicker setCanCancelContentTouches:NO];
    
    self.hulpPicker.delegate = self;
    [self.view addSubview:self.hulpPicker];
}


-(void)getHulpOptions{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getHulpOptions" arguments:[NSArray arrayWithObjects: nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}



- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= arr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in arr) {
        NSLog(@"Item name: %@", item);
        CGFloat x = i * self.hulpPicker.frame.size.width;
        
        //Maak PER optie een model aan (HulpOptionModel) om de data door te sturen naar het detail view
        NSString *nameImage = (NSString *)[item valueForKey:@"nameImage"];
        NSString *detailImage = (NSString *)[item valueForKey:@"detailImage"];
        HulpOptionModel *hulpOptionModel = [[HulpOptionModel alloc] initWithName:nameImage andWithDetail:detailImage];
        
        
        //CUSTOM UIVIEWS AANMAKEN!! met properties in voor de option!!
        UIImage* hulpImage = [ImageFactory createImageWithName:nameImage andType:@"png" andDirectory:@"images"];
        HulpOptionView *hulpOptionView = [[HulpOptionView alloc] initWithFrame:CGRectMake(0, 0, 270, 415) andHulpOptionModel:hulpOptionModel];
        [hulpOptionView setFrame:CGRectMake(x, 0, 270, 415)];
        [self.hulpPicker addSubview:hulpOptionView];
        
        UITapGestureRecognizer *optionClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoDetailView:)];
        [hulpOptionView addGestureRecognizer:optionClick];
        
        
        i++;
    }
    
    self.hulpPicker.contentSize = CGSizeMake(self.hulpPicker.frame.size.width *viewcount, 207);
    
    //Add the page control
	self.pageControl = [[DDPageControl alloc] init];
	[self.pageControl setCenter: CGPointMake(self.view.center.x,self.view.center.y + 230)];
	[self.pageControl setNumberOfPages: viewcount];
	[self.pageControl setCurrentPage: 0];
	[self.pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged];
	[self.pageControl setDefersCurrentPageDisplay: YES];
    [self.pageControl setType:DDPageControlTypeOnFullOffFull];
	[self.pageControl setOnColor: [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
	[self.pageControl setOffColor: [UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 10.0f];
	[self.view addSubview: self.pageControl];
    
}


-(void)pageControlClicked:(DDPageControl*)sender{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
    [self.hulpPicker setContentOffset:CGPointMake(self.hulpPicker.bounds.size.width * thePageControl.currentPage, self.hulpPicker.contentOffset.y) animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (self.pageControl.currentPage != nearestNumber)
	{
		self.pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[self.pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[self.pageControl updateCurrentPageDisplay] ;
}
- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}


/****************************************************************************************************************/
// gotoDetailView: go to the detail view of the clicked option
/****************************************************************************************************************/
-(void)gotoDetailView:(UITapGestureRecognizer*)sender{
    HulpOptionView *clickedOptionView = (HulpOptionView*)sender.view;
    HulpDetailViewController *hulpDetailView = [[HulpDetailViewController alloc] initWithModel:clickedOptionView.hulpOptionModel];
    [self.navigationController pushViewController:hulpDetailView animated:YES];
}





/****************************************************************************************************************/
// viewWillAppear: hide the back button, show the custom buttons and navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    self.navigationController.navigationBar.hidden = NO;
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_hulpInNood" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
        
    [self getHulpOptions];
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
