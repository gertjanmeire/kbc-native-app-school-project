//
//  RekeningOverzichtViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "RekeningOverzichtViewController.h"
#import "IIWrapController.h"
#import "IIViewDeckController.h"
#import "TransactiesViewController.h"
#import "OverschrijvingenViewController.h"
#import "RekeningView.h"

#import "RekeningModel.h"

@interface RekeningOverzichtViewController ()

@end

@implementation RekeningOverzichtViewController

@synthesize rekeningPicker = _rekeningPicker;
@synthesize pageControl = _pageControl;
@synthesize transactiesButton = _transactiesButton;
@synthesize overschrijvingButton = _overschrijvingButton;
@synthesize rekeningArr = _rekeningArr;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];
    }
    return self;
}


-(void)getRekeningen{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getRekeningenForUser" arguments:[NSArray arrayWithObjects: user_id ,nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}



- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    self.rekeningArr = [[NSArray alloc] initWithArray:arr];
    
    //Vul de picker op met de resultaten in de arr
    NSInteger viewcount= self.rekeningArr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in self.rekeningArr) {
        NSLog(@"Item name: %@", item);
        CGFloat x = i * self.rekeningPicker.frame.size.width;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber *rekeningID = (NSNumber *)[item valueForKey:@"id"];
        NSString *rekeningNaam = (NSString *)[item valueForKey:@"rekeningNaam"];
        NSNumber *saldo = (NSNumber *)[item valueForKey:@"saldo"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        RekeningModel *rekeningModel = [[RekeningModel alloc] initWithRekeningNaam:rekeningNaam andID:rekeningID andSaldo:saldo andUserID:userID];
        
        //Rekening view aanmaken in de scrollview
        RekeningView *rekeningView = [[RekeningView alloc] initWithFrame:CGRectMake(0, 0, self.rekeningPicker.bounds.size.width, self.rekeningPicker.bounds.size.height) andRekeningModel:rekeningModel];
        [rekeningView setFrame:CGRectMake(x, 0,self.view.frame.size.width, self.view.frame.size.height)];
        [self.rekeningPicker addSubview:rekeningView];        
        
        i++;
    }
    
    self.rekeningPicker.contentSize = CGSizeMake(self.rekeningPicker.frame.size.width *viewcount, 129);
    
    //Add the page control
	self.pageControl = [[DDPageControl alloc] init];
	[self.pageControl setCenter: CGPointMake(self.view.center.x,self.view.center.y + 60)];
	[self.pageControl setNumberOfPages: viewcount];
	[self.pageControl setCurrentPage: 0];
	[self.pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged];
	[self.pageControl setDefersCurrentPageDisplay: YES];
    [self.pageControl setType:DDPageControlTypeOnFullOffFull];
	[self.pageControl setOnColor: [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
	[self.pageControl setOffColor: [UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 10.0f];
	[self.view addSubview: self.pageControl];
    
}


-(void)pageControlClicked:(DDPageControl*)sender{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
    [self.rekeningPicker setContentOffset:CGPointMake(self.rekeningPicker.bounds.size.width * thePageControl.currentPage, self.rekeningPicker.contentOffset.y) animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (self.pageControl.currentPage != nearestNumber)
	{
		self.pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[self.pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[self.pageControl updateCurrentPageDisplay] ;
}
- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}




/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    
    
    //Set the UIScrollView with all the user's pet's
    self.rekeningPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 30, 320, 275)];
    [self.rekeningPicker setBounces:YES];
    [self.rekeningPicker setPagingEnabled:YES];
    [self.rekeningPicker setShowsHorizontalScrollIndicator:YES];
    self.rekeningPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.rekeningPicker setCanCancelContentTouches:NO];
    
    self.rekeningPicker.delegate = self;
    [self.view addSubview:self.rekeningPicker];
        
    
    //Bekijk transacties btton
    UIImage *transactiesButtonImage = [ImageFactory createImageWithName:@"bekijkTransactiesButton" andType:@"png" andDirectory:@"images"];
    self.transactiesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.transactiesButton setImage:transactiesButtonImage forState:UIControlStateNormal];
    self.transactiesButton.frame = CGRectMake(72, 341, transactiesButtonImage.size.width, transactiesButtonImage.size.height);
    [self.transactiesButton addTarget:self action:@selector(gotoTransactionsView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.transactiesButton];
    
    //Overschrijving vanaf deze rekening
    UIImage *overschrijvingButtonImage = [ImageFactory createImageWithName:@"overschrijvingVanafRekeningButton" andType:@"png" andDirectory:@"images"];
    self.overschrijvingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.overschrijvingButton setImage:overschrijvingButtonImage forState:UIControlStateNormal];
    self.overschrijvingButton.frame = CGRectMake(72, 401, overschrijvingButtonImage.size.width, overschrijvingButtonImage.size.height);
    [self.overschrijvingButton addTarget:self action:@selector(gotoOverschrijvingView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.overschrijvingButton];
    
}


/****************************************************************************************************************/
// gotoTransactionsView: go to the view for transactions
/****************************************************************************************************************/
-(void)gotoTransactionsView:(UIButton*)sender{
    NSLog(@"Go to transactions view");
    
    //Get the current item in the scrollview
    int cardIndex = (int)(self.rekeningPicker.contentOffset.x / self.rekeningPicker.frame.size.width);
    NSLog(@"INDEX IS: %d", cardIndex);
    
    RekeningView *selectedView = (RekeningView*)[self.rekeningArr objectAtIndex:cardIndex];
    NSLog(@"Selected View: %@", [selectedView valueForKey:@"id"]);
    
    
    
    //NSLog(@"Selected view: %d", [selectedModel.rekeningID intValue]);
    
    
    //Open the transactions view
    TransactiesViewController *transactiesVC = [[TransactiesViewController alloc] initWithRekeningID:[selectedView valueForKey:@"id"]];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:transactiesVC];
}




/****************************************************************************************************************/
// gotoOverschrijvingenView: go to the view for Overschrijvingen 
/****************************************************************************************************************/
-(void)gotoOverschrijvingView:(UIButton*)sender{
    NSLog(@"Go to overschrijving view");
    OverschrijvingenViewController *overschrijvingenVC = [[OverschrijvingenViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:overschrijvingenVC];
}


/****************************************************************************************************************/
// viewWillAppear: hide the back button, show the navigationbar and show custom back button
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_rekeningOverzicht" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [self getRekeningen];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}



/****************************************************************************************************************/
// backButton: go back 1 view in the navigationController stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
