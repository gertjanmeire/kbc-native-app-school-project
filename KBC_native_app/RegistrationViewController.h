//
//  RegistrationViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"

@interface RegistrationViewController : UIViewController <AMFRemotingCallDelegate, UITextFieldDelegate>{
    AppModel* appModel;
}

//AppModel
@property (strong, nonatomic) AppModel* appModel;

//Step 1
@property (strong, nonatomic) UIView* step1View;
@property (strong, nonatomic) CustomTextField* cardnrFieldForResponse;
@property (strong, nonatomic) CustomTextField* responseField;
@property (strong, nonatomic) UIButton* responseButton;
@property (strong, nonatomic) UIButton* tutorialButton;

//Step 2
@property (strong, nonatomic) UIView* step2View;
@property (strong, nonatomic) CustomTextField* passwordField1;
@property (strong, nonatomic) CustomTextField* passwordField2;
@property (strong, nonatomic) UIButton *confirmButton;

//Step 3
@property (strong, nonatomic) UIView* step3View;
@property (strong, nonatomic) UIButton* bevestigButton;
@property (strong, nonatomic) UIButton* step3InfoButton;

//Step 4
@property (strong, nonatomic) UIView* step4View;
@property (strong, nonatomic) CustomTextField* cardnrField;
@property (strong, nonatomic) CustomTextField* passwordField;
@property (strong, nonatomic) UIImageView* rememberPasswordImageView;
@property (strong, nonatomic) UIButton* registerButton;

//Registration complete view
@property (strong, nonatomic) UIView* registrationCompleteView;
@property (strong, nonatomic) UIButton* gotoLoginButton;

@end
