//
//  NotificationView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 27/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "NotificationView.h"

@implementation NotificationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImage* notificationBG = [ImageFactory createImageWithName:@"notification" andType:@"png" andDirectory:@"images"];
        UIImageView* notificationImageView = [[UIImageView alloc] initWithImage:notificationBG];
        [self addSubview:notificationImageView];
        
    }
    return self;
}

@end
