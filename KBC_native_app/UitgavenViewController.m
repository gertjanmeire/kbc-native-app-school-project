//
//  UitgavenViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "UitgavenViewController.h"
#import "TransactionsForCategoryViewController.h"

@interface UitgavenViewController ()

@end

@implementation UitgavenViewController

@synthesize periodeSlider = _periodeSlider;
@synthesize themasScrollView = _themasScrollView;

@synthesize tutorialButton = _tutorialButton;
@synthesize pageControl = _pageControl;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setUserInteractionEnabled:YES];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"uitgavenBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];


    //Slider images
    UIImage * sliderLeftTrackImage = [ImageFactory createImageWithName:@"slider2EmptyTrack" andType:@"png" andDirectory:@"images"];
    UIImage * sliderRightTrackImage = [ImageFactory createImageWithName:@"slider2Track" andType:@"png" andDirectory:@"images"];
    UIImage * thumbImage = [ImageFactory createImageWithName:@"thumb2" andType:@"png" andDirectory:@"images"];
    
    //Periode slider
    self.periodeSlider = [[UISlider alloc] initWithFrame:CGRectMake(10, 77, 283, 50)];
    [self.periodeSlider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.periodeSlider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
    [self.periodeSlider setThumbImage:thumbImage forState:UIControlStateNormal];
    self.periodeSlider.minimumValue = 0;
    self.periodeSlider.maximumValue = 1200;
    [self.periodeSlider setValue:250 animated:YES];
    //[self.periodeSlider addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.periodeSlider];

    
    //Set the UIScrollView with all the user's pet's
    self.themasScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 165, 320, 270)];
    [self.themasScrollView setBounces:YES];
    [self.themasScrollView setPagingEnabled:YES];
    [self.themasScrollView setShowsHorizontalScrollIndicator:YES];
    self.themasScrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.themasScrollView setCanCancelContentTouches:NO];
    
    self.themasScrollView.delegate = self;
    [self.view addSubview:self.themasScrollView];
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoTransactionsForCategoryView:)];
    tap.cancelsTouchesInView = NO;
    [self.themasScrollView addGestureRecognizer:tap];
    
    //Fill the hulpPicker
    NSArray *uitgavenCategorien = [NSArray arrayWithObjects:@"uitgavenCat1", @"uitgavenCat2",  @"uitgavenCat3", @"uitgavenCat4", @"uitgavenCat5", nil];
     
     NSInteger viewcount = uitgavenCategorien.count;
     for (int i=0; i<viewcount; i++) {
     NSLog(@"Add pet type");
     CGFloat x = i * self.themasScrollView.frame.size.width + 15;
     //CUSTOM UIVIEWS AANMAKEN!! met properties in voor het pet!!
     UIImage* catImage = [ImageFactory createImageWithName:[uitgavenCategorien objectAtIndex:i] andType:@"png" andDirectory:@"images"];
     UIImageView* catImageView = [[UIImageView alloc] initWithImage:catImage];
     [catImageView setFrame:CGRectMake(x, 0, catImage.size.width, catImage.size.height)];
     [self.themasScrollView addSubview:catImageView];
     }
     
     self.themasScrollView.contentSize = CGSizeMake(self.themasScrollView.frame.size.width *viewcount, 129);
    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(280, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tutorialButton];
    
    //Add the page control
	self.pageControl = [[DDPageControl alloc] init];
	[self.pageControl setCenter: CGPointMake(self.view.center.x,self.view.center.y + 165)];
	[self.pageControl setNumberOfPages: viewcount];
	[self.pageControl setCurrentPage: 0];
	[self.pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged];
	[self.pageControl setDefersCurrentPageDisplay: YES];
    [self.pageControl setType:DDPageControlTypeOnFullOffFull];
	[self.pageControl setOnColor: [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
	[self.pageControl setOffColor: [UIColor colorWithRed:(0.0/255.0) green:(163.0/255.0) blue:(227.0/255.0) alpha:1.0]];
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 10.0f];
	[self.view addSubview: self.pageControl];
    
}


-(void)pageControlClicked:(DDPageControl*)sender{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
	[self.themasScrollView setContentOffset: CGPointMake(self.themasScrollView.bounds.size.width *thePageControl.currentPage, self.themasScrollView.contentOffset.y) animated: YES] ;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (self.pageControl.currentPage != nearestNumber)
	{
		self.pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[self.pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[self.pageControl updateCurrentPageDisplay] ;
}
    

-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayUitgaven" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}


-(void)gotoTransactionsForCategoryView:(UITapGestureRecognizer*)sender{
    NSLog(@"TAP");
    TransactionsForCategoryViewController *transactionsForCategoryView = [[TransactionsForCategoryViewController alloc] init];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:transactionsForCategoryView];
}

/****************************************************************************************************************/
// viewwillAppear: change to tab 1
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_uitgaven" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
