//
//  TransactieDetailViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "TransactieDetailViewController.h"
#import "TransactieModel.h"

@interface TransactieDetailViewController ()

@end

@implementation TransactieDetailViewController

//Model
@synthesize transactieModel = _transactieModel;

@synthesize betaalmethode = _betaalmethode;
@synthesize amount = _amount;
@synthesize datum = _datum;
@synthesize plaats = _plaats;
@synthesize gemeente = _gemeente;

- (id)initWithTransactionModel:(TransactieModel*)model
{
    self = [super init];
    if (self) {
        //Background image
        UIImage *backgroundImage = [ImageFactory createImageWithName:@"transactieDetailBG" andType:@"png" andDirectory:@"images"];
         UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
         [self.view addSubview:backgroundImageView];

        
        //Set the model
        self.transactieModel = model;
        NSLog(@"Titel: %@", self.transactieModel.betaalmethode);

        
        self.betaalmethode = [[UILabel alloc] initWithFrame:CGRectMake(106, 34, 190, 38)];
        [self.betaalmethode setFont:[UIFont fontWithName:@"LorimerLight" size:38]];
        [self.betaalmethode setBackgroundColor:[UIColor clearColor]];
        self.betaalmethode.textAlignment = NSTextAlignmentLeft;
        [self.betaalmethode setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.betaalmethode setText:self.transactieModel.betaalmethode];
        [self.view addSubview:self.betaalmethode];
        
        self.amount = [[UILabel alloc] initWithFrame:CGRectMake(106, 109, 190, 38)];
        [self.amount setFont:[UIFont fontWithName:@"LorimerLight" size:26]];
        [self.amount setBackgroundColor:[UIColor clearColor]];
        self.amount.textAlignment = NSTextAlignmentLeft;
        [self.amount setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.amount setText:[NSString stringWithFormat:@"€ %.2f", [self.transactieModel.amount doubleValue]]];
        [self.view addSubview:self.amount];
        
        self.datum = [[UILabel alloc] initWithFrame:CGRectMake(106, 180, 190, 26)];
        [self.datum setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
        [self.datum setBackgroundColor:[UIColor clearColor]];
        self.datum.textAlignment = NSTextAlignmentLeft;
        [self.datum setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.datum setText:self.transactieModel.datum];
        [self.view addSubview:self.datum];
        
        self.plaats = [[UILabel alloc] initWithFrame:CGRectMake(106, 245, 190, 26)];
        [self.plaats setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
        [self.plaats setBackgroundColor:[UIColor clearColor]];
        self.plaats.textAlignment = NSTextAlignmentLeft;
        [self.plaats setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.plaats setText:self.transactieModel.plaats];
        [self.view addSubview:self.plaats];
        
        self.gemeente = [[UILabel alloc] initWithFrame:CGRectMake(106, 315, 190, 26)];
        [self.gemeente setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
        [self.gemeente setBackgroundColor:[UIColor clearColor]];
        self.gemeente.textAlignment = NSTextAlignmentLeft;
        [self.gemeente setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.gemeente setText:self.transactieModel.gemeente];
        [self.view addSubview:self.gemeente];

        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSLog(@"Titel: %@", self.transactieModel.titel);
    

}


/****************************************************************************************************************/
// viewDidAppear: hide the back button, show the navigation bar and show custom bar buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide the back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_transactieDetail" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
