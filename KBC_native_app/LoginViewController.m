//
//  LoginViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "ZoekKantoorViewController.h"
#import "HulpViewController.h"
#import "MenuViewController.h"
#import "IntroWizardViewController.h"
#import "LoginHandtekeningViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize appModel = appModel;
@synthesize kaartNrTextField = _kaartNrTextField;
@synthesize wachtwoordTextField = _wachtwoordTextField;
@synthesize onthoudenImageView = _onthoudenImageView;
@synthesize loginButton = _loginButton;
@synthesize registrationButton = _registrationButton;
@synthesize kantoorButton = _kantoorButton;
@synthesize  hulpButton = _hulpButton;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        appModel = [[AppModel sharedAppModel] init];
    }
    return self;
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical elements of the login view
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"loginBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
        
    //Kaartnr
    UIImage *kaartnrFieldImage = [ImageFactory createImageWithName:@"textfieldLoginBG" andType:@"png" andDirectory:@"images"];
    self.kaartNrTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 100, kaartnrFieldImage.size.width, kaartnrFieldImage.size.height)];
    [self.kaartNrTextField setBackground:kaartnrFieldImage];
    [self.kaartNrTextField setText:@"0123-0123-0123-0123-0"];
    [self.kaartNrTextField setPlaceholder:@"Kaartnummer"];
    [self.kaartNrTextField setDelegate:self];
    [self.kaartNrTextField setKeyboardType:UIKeyboardTypePhonePad];
    [self.kaartNrTextField changeTopMargin:15];
    [self.kaartNrTextField changeLeftMargin:65];
    [self.view addSubview:self.kaartNrTextField];
    
    //Wachtwoord
    UIImage *wachtwoordFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.wachtwoordTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 175, wachtwoordFieldImage.size.width, wachtwoordFieldImage.size.height)];
    [self.wachtwoordTextField setBackground:wachtwoordFieldImage];
    [self.wachtwoordTextField setPlaceholder:@"Wachtwoord"];
    [self.wachtwoordTextField setSecureTextEntry:YES];
    [self.wachtwoordTextField setDelegate:self];
    [self.view addSubview:self.wachtwoordTextField];
    
    //Onthouden
    UIImage *onthoudenImage = [ImageFactory createImageWithName:@"kaartnummerOnthouden" andType:@"png" andDirectory:@"images"];
    self.onthoudenImageView = [[UIImageView alloc] initWithImage:onthoudenImage];
    [self.onthoudenImageView setFrame:CGRectMake(20, 235, onthoudenImage.size.width, onthoudenImage.size.height)];
    [self.view addSubview:self.onthoudenImageView];
    
    
    //Aanmelden
    UIImage *loginButtonImage = [ImageFactory createImageWithName:@"loginButton" andType:@"png" andDirectory:@"images"];
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setImage:loginButtonImage forState:UIControlStateNormal];
    self.loginButton.frame = CGRectMake(72, 280, loginButtonImage.size.width, loginButtonImage.size.height);
    [self.loginButton addTarget:self action:@selector(checkIfUserExists:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.loginButton];
    
    //Registreren
    UIImage *registrationButtonImage = [ImageFactory createImageWithName:@"registrationButton" andType:@"png" andDirectory:@"images"];
    self.registrationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registrationButton setImage:registrationButtonImage forState:UIControlStateNormal];
    self.registrationButton.frame = CGRectMake(127, 350, registrationButtonImage.size.width, registrationButtonImage.size.height);
    [self.registrationButton addTarget:self action:@selector(gotoRegistrationView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.registrationButton];
    
    //Kantoor
    UIImage *zoekKantoorButtonImage = [ImageFactory createImageWithName:@"zoekKantoorButton" andType:@"png" andDirectory:@"images"];
    self.kantoorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.kantoorButton setImage:zoekKantoorButtonImage forState:UIControlStateNormal];
    self.kantoorButton.frame = CGRectMake(15, 405, zoekKantoorButtonImage.size.width, zoekKantoorButtonImage.size.height);
    [self.kantoorButton addTarget:self action:@selector(gotoZoekKantoorView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.kantoorButton];
    
    //Hulp
    UIImage *hulpButtonImage = [ImageFactory createImageWithName:@"hulpButton" andType:@"png" andDirectory:@"images"];
    self.hulpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.hulpButton setImage:hulpButtonImage forState:UIControlStateNormal];
    self.hulpButton.frame = CGRectMake(165, 405, hulpButtonImage.size.width, hulpButtonImage.size.height);
    [self.hulpButton addTarget:self action:@selector(gotoHulpView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.hulpButton];
    
}


/****************************************************************************************************************/
// gotoRegistrationView: Go to the registrationViewController
/****************************************************************************************************************/
-(void)gotoRegistrationView:(UIButton *)sender{
    RegistrationViewController *registrationVC = [[RegistrationViewController alloc] init];
    [self.navigationController pushViewController:registrationVC animated:YES];
}


/****************************************************************************************************************/
// gotoZoekKantoorView: Go to the ZoekKantoorViewController
/****************************************************************************************************************/
-(void)gotoZoekKantoorView:(UIButton *)sender{
    ZoekKantoorViewController *zoekVC = [[ZoekKantoorViewController alloc] init];
    [self.navigationController pushViewController:zoekVC animated:YES];
}


/****************************************************************************************************************/
// gotoHulpView: GO to the HulpViewController
/****************************************************************************************************************/
-(void)gotoHulpView:(UIButton *)sender{
    HulpViewController *hulpVC = [[HulpViewController alloc] init];
    [self.navigationController pushViewController:hulpVC animated:YES];
}


/****************************************************************************************************************/
// checkIfUserExists: Check if a user his/her login arguments are correct
/****************************************************************************************************************/
-(void)checkIfUserExists:(UIButton*)sender{
    NSString* cardnr = self.kaartNrTextField.text;
    NSString* pass = self.wachtwoordTextField.text;
    
    if(sender == self.loginButton){
        if(![cardnr isEqualToString:@""] && ![pass isEqualToString:@""]){
            NSArray *loginArguments = [NSArray arrayWithObjects:cardnr, pass, nil];
            
            //Check webservice is user exists
            //If so change view else show error message
            NSString *URLString = [NSString stringWithFormat:@"%@", appModel.IP];
            AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"checkIfUserExists" arguments:[NSArray arrayWithObjects: loginArguments, nil]];
            m_remotingCall.delegate = self;
            [m_remotingCall start];
        }else{
            NSLog(@"Please fill in all fields");
        }
    }
    
    
}

/****************************************************************************************************************/
// remotingCallDidFinishLoading: 
/****************************************************************************************************************/
- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSObject *)object
{
	NSLog(@"[MainVC] remoting call was successful. received data: %@", [object valueForKey:@"validation"]);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    NSString *validationString = (NSString *)[object valueForKey:@"validation"];
    NSString *userExistsString = (NSString *)[object valueForKey:@"loginInfoCorrect"];
    NSNumber* firstTimeLogin = (NSNumber*)[object valueForKey:@"first_time_login"];
    
    
    
    //If the validationString is "login" check if the user exists and login or show an error message
    if([validationString isEqualToString:@"login"]){
        if([userExistsString isEqualToString:@"yes"]){
            self.appModel.userInfo = object;
            if([firstTimeLogin intValue] == 1){
                //Go the the start wizard
                IntroWizardViewController *introWizardVC = [[IntroWizardViewController alloc] init];
                [self.navigationController pushViewController:introWizardVC animated:YES];
            }else{
                //Toon handtekening scherm
                LoginHandtekeningViewController *loginHandtekeningVC = [[LoginHandtekeningViewController alloc] init];
                [self.navigationController pushViewController:loginHandtekeningVC animated:YES];
            }
        }else if([userExistsString isEqualToString:@"no"]){
            //Show error that the user does not exists
            NSLog(@"User does not exist");
            UIImage* errorTextfieldBG = [ImageFactory createImageWithName:@"textfieldErrorBG" andType:@"png" andDirectory:@"images"];
            [self.wachtwoordTextField setBackground:errorTextfieldBG];
            [self.wachtwoordTextField setText:@""];
            [self.wachtwoordTextField setPlaceholder:@"Uw wachtwoord is niet correct"];
            [self.wachtwoordTextField setTextColor:[UIColor whiteColor]];
            //Change placeholder text color
            self.wachtwoordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Uw wachtwoord is niet correct" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        }
    }
}


/****************************************************************************************************************/
// remotingCall: fail of the remote call
/****************************************************************************************************************/
- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"[MainVC] remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}


/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    

    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_taal" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
