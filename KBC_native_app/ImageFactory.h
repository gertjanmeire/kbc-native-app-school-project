//
//  ImageFactory.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageFactory : NSObject

+(UIImage *)createImageWithName:(NSString *)imageName andType:(NSString *)imageType andDirectory:(NSString *)imageDir;
@end
