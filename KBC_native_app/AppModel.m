//
//  AppModel.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 28/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "AppModel.h"

@implementation AppModel

@synthesize userInfo = _userInfo;
@synthesize IP = _IP;
@synthesize registeringCardNr = _registeringCardNr;
@synthesize registeringPassword = _registeringPassword;

+(AppModel *)sharedAppModel{
    static AppModel *sharedAppModel;
    
    @synchronized(self){
        if(!sharedAppModel){
            sharedAppModel = [[AppModel alloc] init];
            sharedAppModel.IP = @"http://192.168.1.6:8888//amfphp-2.1/Amfphp/";
            //sharedAppModel.IP = @"http://www.gertjanmeire.be/devine/20122013/majorV/amfphp-2.1/Amfphp/";

            //sharedAppModel.IP = @"192.168.1.5";
        }
      
        return sharedAppModel;
    }
}

@end
