//
//  OverschrijvingenViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "OverschrijvingenViewController.h"
#import "OverschrijvingStep1RekeningView.h"

#import "RekeningModel.h"

@interface OverschrijvingenViewController ()

@end

@implementation OverschrijvingenViewController

@synthesize overschrijvingHeaderImageView = _overschrijvingHeaderImageView;

@synthesize headerImage1 = _headerImage1;
@synthesize headerImage2 = _headerImage2;
@synthesize headerImage3 = _headerImage3;
@synthesize headerImage4 = _headerImage4;
@synthesize headerImage5 = _headerImage5;

@synthesize tabbarButton1 = _tabbarButton1;
@synthesize tabbarButton2 = _tabbarButton2;
@synthesize tabbarButton3 = _tabbarButton3;
@synthesize tabbarButton4 = _tabbarButton4;
@synthesize tabbarButton5 = _tabbarButton5;


@synthesize step1View = _step1View;
@synthesize rekeningenPicker = _rekeningenPicker;
@synthesize rekeningArr = _rekeningArr;

@synthesize step2View = _step2View;
@synthesize contactenRekeningenPicker = _contactenRekeningenPicker;
@synthesize contactenRekeningArr = _contactenRekeningArr;

@synthesize step3View = _step3View;
@synthesize amountField = _amountField;

@synthesize step4View = _step4View;


@synthesize step5View = _step5View;
@synthesize normaalBerichtButton = _normaalBerichtButton;
@synthesize gestructureerdBerichtButton = _gestructureerdBerichtButton;
@synthesize normaalBerichtField = _normaalBerichtField;
@synthesize gestructureerdBerichtField = _gestructureerdBerichtField;
@synthesize overschrijvingsButton = _overschrijvingsButton;

@synthesize tutorialButton = _tutorialButton;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(249.0/255.0) blue:(249.0/255.0) alpha:1.0]];
        
        self.headerImage1 = [ImageFactory createImageWithName:@"overschrijvingTopImage1" andType:@"png" andDirectory:@"images"];
        self.headerImage2 = [ImageFactory createImageWithName:@"overschrijvingTopImage2" andType:@"png" andDirectory:@"images"];
        self.headerImage3 = [ImageFactory createImageWithName:@"overschrijvingTopImage3" andType:@"png" andDirectory:@"images"];
        self.headerImage4 = [ImageFactory createImageWithName:@"overschrijvingTopImage4" andType:@"png" andDirectory:@"images"];
        self.headerImage5 = [ImageFactory createImageWithName:@"overschrijvingTopImage5" andType:@"png" andDirectory:@"images"];
        
        self.selectedTabbarButton1Image = [ImageFactory createImageWithName:@"overschrijvingTab1Selected" andType:@"png" andDirectory:@"images"];
        self.selectedTabbarButton2Image = [ImageFactory createImageWithName:@"overschrijvingTab2Selected" andType:@"png" andDirectory:@"images"];
        self.selectedTabbarButton3Image = [ImageFactory createImageWithName:@"overschrijvingTab3Selected" andType:@"png" andDirectory:@"images"];
        self.selectedTabbarButton4Image = [ImageFactory createImageWithName:@"overschrijvingTab4Selected" andType:@"png" andDirectory:@"images"];
        self.selectedTabbarButton5Image = [ImageFactory createImageWithName:@"overschrijvingTab5Selected" andType:@"png" andDirectory:@"images"];

        
        //Top image
        self.overschrijvingHeaderImageView = [[UIImageView alloc] initWithImage:self.headerImage1];
        [self.overschrijvingHeaderImageView setFrame:CGRectMake(0, 0, 320, 154)];
        [self.view addSubview:self.overschrijvingHeaderImageView];
        
    }
    return self;
}


-(void)getRekeningen{
    NSObject *userInfoObject = [AppModel sharedAppModel].userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
    
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"%@", [AppModel sharedAppModel].IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"getRekeningenForUser" arguments:[NSArray arrayWithObjects: user_id ,nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}



- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    //Eigen rekeningen picker opvullen
    self.rekeningArr = [[NSArray alloc] initWithArray:arr];
    
    NSInteger viewcount= self.rekeningArr.count;
    CGFloat i = 0;
    
    
    for (NSObject *item in self.rekeningArr) {
        NSLog(@"Item name: %@", item);
        CGFloat x = i * self.rekeningenPicker.frame.size.width;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber *rekeningID = (NSNumber *)[item valueForKey:@"id"];
        NSString *rekeningNaam = (NSString *)[item valueForKey:@"rekeningNaam"];
        NSNumber *saldo = (NSNumber *)[item valueForKey:@"saldo"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        RekeningModel *rekeningModel = [[RekeningModel alloc] initWithRekeningNaam:rekeningNaam andID:rekeningID andSaldo:saldo andUserID:userID];
        
        //Rekening view aanmaken in de scrollview
        OverschrijvingStep1RekeningView *rekeningView = [[OverschrijvingStep1RekeningView alloc] initWithFrame:CGRectMake(0, 0, self.rekeningenPicker.bounds.size.width, self.rekeningenPicker.bounds.size.height) andRekeningModel:rekeningModel];
        [rekeningView setFrame:CGRectMake(x, 0,self.rekeningenPicker.bounds.size.width, self.rekeningenPicker.bounds.size.height)];
        [self.rekeningenPicker addSubview:rekeningView];
        
        i++;
    }
    self.rekeningenPicker.contentSize = CGSizeMake(self.rekeningenPicker.frame.size.width * viewcount, self.rekeningenPicker.frame.size.height);
    

    
    
    
    //Contacten rekening picker opvullen
    self.contactenRekeningArr = [[NSArray alloc] initWithArray:arr];
    
    NSInteger viewcount2= self.contactenRekeningArr.count;
    CGFloat i2 = 0;
    
    
    for (NSObject *item in self.contactenRekeningArr) {
        NSLog(@"Item name: %@", item);
        CGFloat x2 = i2 * self.contactenRekeningenPicker.frame.size.width;
        
        //Maak PER optie een model aan (RekeningModel) om de data door te sturen naar het detail view
        NSNumber *rekeningID = (NSNumber *)[item valueForKey:@"id"];
        NSString *rekeningNaam = (NSString *)[item valueForKey:@"rekeningNaam"];
        NSNumber *saldo = (NSNumber *)[item valueForKey:@"saldo"];
        NSNumber* userID = (NSNumber*)[item valueForKey:@"userID"];
        RekeningModel *rekeningModel = [[RekeningModel alloc] initWithRekeningNaam:rekeningNaam andID:rekeningID andSaldo:saldo andUserID:userID];
        
        //Rekening view aanmaken in de scrollview
        OverschrijvingStep1RekeningView *rekeningView = [[OverschrijvingStep1RekeningView alloc] initWithFrame:CGRectMake(0, 0, self.contactenRekeningenPicker.bounds.size.width, self.contactenRekeningenPicker.bounds.size.height) andRekeningModel:rekeningModel];
        [rekeningView setFrame:CGRectMake(x2, 0,self.contactenRekeningenPicker.bounds.size.width, self.contactenRekeningenPicker.bounds.size.height)];
        [self.contactenRekeningenPicker addSubview:rekeningView];
        
        i2++;
    }
    
    self.contactenRekeningenPicker.contentSize = CGSizeMake(self.contactenRekeningenPicker.frame.size.width *viewcount2, self.contactenRekeningenPicker.frame.size.height);

    


    
}

- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}


/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Tabbar buttons
     self.tabbarButton1Image= [ImageFactory createImageWithName:@"overschrijvingTab1" andType:@"png" andDirectory:@"images"];
     self.tabbarButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
     [self.tabbarButton1 setImage:self.tabbarButton1Image forState:UIControlStateNormal];
     self.tabbarButton1.frame = CGRectMake(0, 154, self.tabbarButton1Image.size.width, self.tabbarButton1Image.size.height);
     [self.tabbarButton1 addTarget:self action:@selector(changeToTab1:) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:self.tabbarButton1];
     
     self.tabbarButton2Image = [ImageFactory createImageWithName:@"overschrijvingTab2" andType:@"png" andDirectory:@"images"];
     self.tabbarButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
     [self.tabbarButton2 setImage:self.tabbarButton2Image forState:UIControlStateNormal];
     self.tabbarButton2.frame = CGRectMake(64, 154, self.tabbarButton2Image.size.width, self.tabbarButton2Image.size.height);
     [self.tabbarButton2 addTarget:self action:@selector(changeToTab2:) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:self.tabbarButton2];
     
     self.tabbarButton3Image = [ImageFactory createImageWithName:@"overschrijvingTab3" andType:@"png" andDirectory:@"images"];
     self.tabbarButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
     [self.tabbarButton3 setImage:self.tabbarButton3Image forState:UIControlStateNormal];
     self.tabbarButton3.frame = CGRectMake(128, 154, self.tabbarButton3Image.size.width, self.tabbarButton3Image.size.height);
     [self.tabbarButton3 addTarget:self action:@selector(changeToTab3:) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:self.tabbarButton3];
    
    self.tabbarButton4Image = [ImageFactory createImageWithName:@"overschrijvingTab4" andType:@"png" andDirectory:@"images"];
    self.tabbarButton4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tabbarButton4 setImage:self.tabbarButton4Image forState:UIControlStateNormal];
    self.tabbarButton4.frame = CGRectMake(192, 154, self.tabbarButton4Image.size.width, self.tabbarButton4Image.size.height);
    [self.tabbarButton4 addTarget:self action:@selector(changeToTab4:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tabbarButton4];

    self.tabbarButton5Image = [ImageFactory createImageWithName:@"overschrijvingTab5" andType:@"png" andDirectory:@"images"];
    self.tabbarButton5 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tabbarButton5 setImage:self.tabbarButton5Image forState:UIControlStateNormal];
    self.tabbarButton5.frame = CGRectMake(256, 154, self.tabbarButton5Image.size.width, self.tabbarButton5Image.size.height);
    [self.tabbarButton5 addTarget:self action:@selector(changeToTab5:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tabbarButton5];

    
    
    //STEP 1 VIEW
    self.step1View = [[UIView alloc] initWithFrame:CGRectMake(0, 214, 320, 480)];
     [self.view addSubview:self.step1View];
     
    //Set the UIScrollView with all the user's pet's
    self.rekeningenPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, 320, 179)];
    [self.rekeningenPicker setBounces:YES];
    [self.rekeningenPicker setPagingEnabled:YES];
    [self.rekeningenPicker setShowsHorizontalScrollIndicator:YES];
    self.rekeningenPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.rekeningenPicker setCanCancelContentTouches:NO];
    
    self.rekeningenPicker.delegate = self;
    [self.step1View addSubview:self.rekeningenPicker];
    
    
    
    //STEP 2 VIEW
    self.step2View = [[UIView alloc] initWithFrame:CGRectMake(0, 214, 320, 480)];
    self.step2View.hidden = YES;
    [self.view addSubview:self.step2View];
    
    //Set the UIScrollView met alle contact rekeningen
    self.contactenRekeningenPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, 320, 179)];
    [self.contactenRekeningenPicker setBounces:YES];
    [self.contactenRekeningenPicker setPagingEnabled:YES];
    [self.contactenRekeningenPicker setShowsHorizontalScrollIndicator:YES];
    self.contactenRekeningenPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.contactenRekeningenPicker setCanCancelContentTouches:NO];
    
    self.contactenRekeningenPicker.delegate = self;
    [self.step2View addSubview:self.contactenRekeningenPicker];

    
    
     //STEP 3 VIEW
     self.step3View = [[UIView alloc] initWithFrame:CGRectMake(0, 214, 320, 480)];
     self.step3View.hidden = YES;
     [self.view addSubview:self.step3View];
    
    
    //Amount field
    UIImage *amountFieldImage = [ImageFactory createImageWithName:@"overschrijvingBedragTextfieldBG" andType:@"png" andDirectory:@"images"];
    self.amountField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 20, amountFieldImage.size.width, amountFieldImage.size.height)];
    [self.amountField setBackground:amountFieldImage];
    [self.amountField setText:@""];
    [self.amountField setDelegate:self];
    [self.amountField changeLeftMargin:55];
    [self.step3View addSubview:self.amountField];
    
    
    //STEP 4 VIEW
    self.step4View = [[UIView alloc] initWithFrame:CGRectMake(0, 214, 320, 480)];
    self.step4View.hidden = YES;
    [self.view addSubview:self.step4View];
    
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker setFrame:CGRectMake(10, 20, 300, 200)];
    [self.step4View addSubview:self.datePicker];

    
    
    //STEP 5 VIEW
    self.step5View = [[UIView alloc] initWithFrame:CGRectMake(0, 214, 320, 480)];
    self.step5View.hidden = YES;
    [self.view addSubview:self.step5View];
    
    //Normaal bericht button
    UIImage *normaalBerichtButtonImage = [ImageFactory createImageWithName:@"vrijBerichtButton" andType:@"png" andDirectory:@"images"];
    self.normaalBerichtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.normaalBerichtButton setImage:normaalBerichtButtonImage forState:UIControlStateNormal];
    self.normaalBerichtButton.frame = CGRectMake(20, 20, normaalBerichtButtonImage.size.width, normaalBerichtButtonImage.size.height);
    [self.normaalBerichtButton addTarget:self action:@selector(switchBerichtField:) forControlEvents:UIControlEventTouchUpInside];
    [self.step5View addSubview:self.normaalBerichtButton];
    
    //Normaal bericht field
    UIImage *normaalBerichtFieldImage = [ImageFactory createImageWithName:@"vrijBerichtTextfieldBG" andType:@"png" andDirectory:@"images"];
    self.normaalBerichtField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 84, normaalBerichtFieldImage.size.width, normaalBerichtFieldImage.size.height)];
    [self.normaalBerichtField setBackground:normaalBerichtFieldImage];
    [self.normaalBerichtField setText:@""];
    [self.normaalBerichtField setDelegate:self];
    [self.step5View addSubview:self.normaalBerichtField];

    
    //Gestructureerd bericht button
    UIImage *gestructureerdBerichtButtonImage = [ImageFactory createImageWithName:@"gestructureerdButton" andType:@"png" andDirectory:@"images"];
    self.gestructureerdBerichtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.gestructureerdBerichtButton setImage:gestructureerdBerichtButtonImage forState:UIControlStateNormal];
    self.gestructureerdBerichtButton.frame = CGRectMake(161, 20, gestructureerdBerichtButtonImage.size.width, gestructureerdBerichtButtonImage.size.height);
    [self.gestructureerdBerichtButton addTarget:self action:@selector(switchBerichtField:) forControlEvents:UIControlEventTouchUpInside];
    [self.step5View addSubview:self.gestructureerdBerichtButton];
    
    //Gestructureerd bericht field
    UIImage *gestructureerdBerichtFieldImage = [ImageFactory createImageWithName:@"gestructureerdBerichtTextfieldBG" andType:@"png" andDirectory:@"images"];
    self.gestructureerdBerichtField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 84, gestructureerdBerichtFieldImage.size.width, gestructureerdBerichtFieldImage.size.height)];
    [self.gestructureerdBerichtField setBackground:gestructureerdBerichtFieldImage];
    [self.gestructureerdBerichtField setText:@""];
    [self.gestructureerdBerichtField setDelegate:self];
    self.gestructureerdBerichtField.hidden = YES;
    [self.gestructureerdBerichtField changeLeftMargin:55];
    [self.step5View addSubview:self.gestructureerdBerichtField];
    
    //Overschrijving button
    UIImage *overschrijvingsButtonImage = [ImageFactory createImageWithName:@"overschrijvingButton" andType:@"png" andDirectory:@"images"];
    self.overschrijvingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.overschrijvingsButton setImage:overschrijvingsButtonImage forState:UIControlStateNormal];
    self.overschrijvingsButton.frame = CGRectMake(72, 225, overschrijvingsButtonImage.size.width, overschrijvingsButtonImage.size.height);
    [self.overschrijvingsButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.step5View addSubview:self.overschrijvingsButton];


    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tutorialButton];
    
}

-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayOverschrijving" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}

-(void)switchBerichtField:(UIButton*)sender{
    if(sender == self.normaalBerichtButton){
        self.normaalBerichtField.hidden = NO;
        self.gestructureerdBerichtField.hidden = YES;
    }else{
        self.normaalBerichtField.hidden = YES;
        self.gestructureerdBerichtField.hidden = NO;
    }
}

/****************************************************************************************************************/
// changeToTab1: change to tab 1
/****************************************************************************************************************/
-(void)changeToTab1:(UIButton*)sender{
    NSLog(@"Change to tab 1");
    
    [self setSelectedTab:1];
    
    self.step1View.hidden = NO;
    self.step2View.hidden = YES;
    self.step3View.hidden = YES;
    self.step4View.hidden = YES;
    self.step5View.hidden = YES;
    
    
    
    [self.overschrijvingHeaderImageView setImage:self.headerImage1];
}


/****************************************************************************************************************/
// changeToTab2: change to tab 2
/****************************************************************************************************************/
-(void)changeToTab2:(UIButton*)sender{
    NSLog(@"Change to tab 2");
    
    [self setSelectedTab:2];
    
    self.step1View.hidden = YES;
    self.step2View.hidden = NO;
    self.step3View.hidden = YES;
    self.step4View.hidden = YES;
    self.step5View.hidden = YES;
    
    [self.overschrijvingHeaderImageView setImage:self.headerImage2];
}


/****************************************************************************************************************/
// changeToTab3: change to tab 3
/****************************************************************************************************************/
-(void)changeToTab3:(UIButton*)sender{
    NSLog(@"Change to tab 3");
    
    [self setSelectedTab:3];
    
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = NO;
    self.step4View.hidden = YES;
    self.step5View.hidden = YES;
    
    [self.overschrijvingHeaderImageView setImage:self.headerImage3];
}


/****************************************************************************************************************/
// changeToTab4: change to tab 4
/****************************************************************************************************************/
-(void)changeToTab4:(UIButton*)sender{
    NSLog(@"Change to tab 3");
    
    [self setSelectedTab:4];
    
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = YES;
    self.step4View.hidden = NO;
    self.step5View.hidden = YES;
    
    [self.overschrijvingHeaderImageView setImage:self.headerImage4];
}


/****************************************************************************************************************/
// changeToTab5: change to tab 5
/****************************************************************************************************************/
-(void)changeToTab5:(UIButton*)sender{
    NSLog(@"Change to tab 3");
    
    [self setSelectedTab:5];
    
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = YES;
    self.step4View.hidden = YES;
    self.step5View.hidden = NO;
    
    [self.overschrijvingHeaderImageView setImage:self.headerImage5];
}


-(void)setSelectedTab:(int)tabNumber{
    //self.selectedTab = tabNumber;
    
    [self.tabbarButton1 setImage:self.tabbarButton1Image forState:UIControlStateNormal];
    [self.tabbarButton2 setImage:self.tabbarButton2Image forState:UIControlStateNormal];
    [self.tabbarButton3 setImage:self.tabbarButton3Image forState:UIControlStateNormal];
    [self.tabbarButton4 setImage:self.tabbarButton4Image forState:UIControlStateNormal];
    [self.tabbarButton5 setImage:self.tabbarButton5Image forState:UIControlStateNormal];

    
    switch (tabNumber) {
        case 1:
            [self.tabbarButton1 setImage:self.selectedTabbarButton1Image forState:UIControlStateNormal];
        break;
        case 2:
            [self.tabbarButton2 setImage:self.selectedTabbarButton2Image forState:UIControlStateNormal];
        break;
        case 3:
            [self.tabbarButton3 setImage:self.selectedTabbarButton3Image forState:UIControlStateNormal];
        break;
        case 4:
            [self.tabbarButton4 setImage:self.selectedTabbarButton4Image forState:UIControlStateNormal];
        break;
        case 5:
            [self.tabbarButton5 setImage:self.selectedTabbarButton5Image forState:UIControlStateNormal];
        break;
    }
}

/****************************************************************************************************************/
// viewDidAppear: hide the back button, show the navigation bar and show custom bar buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide the back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_overschrijving" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [self getRekeningen];
    [self changeToTab1:nil];
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
