//
//  HulpDetailViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HulpOptionModel.h"

@interface HulpDetailViewController : UIViewController

@property (strong, nonatomic) UIImageView* hulpOptionImageView;

@property (strong, nonatomic) HulpOptionModel* hulpOptionModel;

- (id)initWithModel:(HulpOptionModel*)model;

@end
