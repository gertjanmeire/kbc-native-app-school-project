//
//  HandigMenuViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface HandigMenuViewController : UIViewController

//Step 1
@property (strong, nonatomic) UIView* step1View;
@property (strong, nonatomic) UIButton* startButton;

//Step 2
@property (strong, nonatomic) UIView* step2View;

//Step 3
@property (strong, nonatomic) UIView* step3View;
@property (strong, nonatomic) UIButton* mainMenuButton;

//Step 4
@property (strong, nonatomic) UIView* step4View;

@end
