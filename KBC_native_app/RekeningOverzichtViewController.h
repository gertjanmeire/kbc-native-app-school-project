//
//  RekeningOverzichtViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

#import "DDPageControl.h"

@interface RekeningOverzichtViewController : UIViewController <AMFRemotingCallDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView* rekeningPicker;
@property (strong, nonatomic) DDPageControl* pageControl;
@property (strong, nonatomic) UIButton* transactiesButton;
@property (strong, nonatomic) UIButton* overschrijvingButton;

@property (strong, nonatomic) NSArray* rekeningArr;

@end
