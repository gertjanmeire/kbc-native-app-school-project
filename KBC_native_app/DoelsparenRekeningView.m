//
//  DoelsparenRekeningView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 27/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "DoelsparenRekeningView.h"

@implementation DoelsparenRekeningView

@synthesize rekeningModel = _rekeningModel;

@synthesize  rekeningImageView = _rekeningImageView;
@synthesize titleLabel = _titleLabel;
@synthesize amountLabel = _amountLabel;

- (id)initWithFrame:(CGRect)frame andRekeningModel:(RekeningModel*)model
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //Rekeningmodel instellen
        self.rekeningModel = model;
        
        //Rekening bg imageview
        UIImage *rekeningBGImage = [ImageFactory createImageWithName:@"rekeningView3BG" andType:@"png" andDirectory:@"images"];
        self.rekeningImageView = [[UIImageView alloc] initWithImage:rekeningBGImage];
        [self.rekeningImageView setFrame:CGRectMake(0, 0, rekeningBGImage.size.width, rekeningBGImage.size.height)];
        [self addSubview:self.rekeningImageView];
        
        //Title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 264, 35)];
        [self.titleLabel setText:self.rekeningModel.rekeningNaam];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:32]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
                
        //Amount label
        self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, 264, 50)];
        [self.amountLabel setText:[NSString stringWithFormat:@"saldo %.2f EUR", [self.rekeningModel.saldo doubleValue]]];
        [self.amountLabel setFont:[UIFont fontWithName:@"LorimerLight" size:37]];
        [self.amountLabel setBackgroundColor:[UIColor clearColor]];
        self.amountLabel.textAlignment = NSTextAlignmentCenter;
        [self.amountLabel setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self addSubview:self.amountLabel];
        
    }
    return self;
}



@end
