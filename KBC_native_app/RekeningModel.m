//
//  RekeningModel.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "RekeningModel.h"

@implementation RekeningModel

@synthesize rekeningID = _rekeningID;
@synthesize rekeningNaam = _rekeningNaam;
@synthesize saldo = _saldo;
@synthesize userID = _userID;

- (id)initWithRekeningNaam:(NSString*)rekeningNaam andID:(NSNumber*)rekeningID andSaldo:(NSNumber*)saldo andUserID:(NSNumber*)userID
{
    self = [super init];
    if (self) {
        self.rekeningID = rekeningID;
        self.rekeningNaam = rekeningNaam;
        self.saldo = saldo;
        self.userID = userID;
    }
    return self;
}


@end
