//
//  HulpOptionView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HulpOptionModel.h"

@interface HulpOptionView : UIView

@property (strong, nonatomic) UIImage* hulpImage;
@property (strong, nonatomic) UIImageView* hulpOptionImageView;

@property (strong, nonatomic) HulpOptionModel* hulpOptionModel;

- (id)initWithFrame:(CGRect)frame andHulpOptionModel:(HulpOptionModel*)hulpOptionModel;
@end
