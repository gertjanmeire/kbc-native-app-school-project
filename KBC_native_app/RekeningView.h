//
//  RekeningView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RekeningModel.h"

@interface RekeningView : UIView

@property (strong, nonatomic) UIView* bundleView;

@property (strong, nonatomic) RekeningModel* rekeningModel;

@property (strong, nonatomic) UIImageView* rekeningImageView;
@property (strong, nonatomic) UILabel* titleLabel;
@property (strong, nonatomic) UILabel* amountLabel;


- (id)initWithFrame:(CGRect)frame andRekeningModel:(RekeningModel*)model;

@end
