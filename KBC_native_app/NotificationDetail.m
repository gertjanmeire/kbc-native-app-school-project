//
//  NotificationDetail.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 27/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "NotificationDetail.h"

@implementation NotificationDetail

@synthesize postButton = _postButton;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //Background image
        UIImage *backgroundImage = [ImageFactory createImageWithName:@"notificationDetailBG" andType:@"png" andDirectory:@"images"];
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        [backgroundImageView setFrame:CGRectMake(0, 20, 320, 568)];
        [self addSubview:backgroundImageView];
        
        //Aanmelden
        UIImage *postButtonImage = [ImageFactory createImageWithName:@"postButton" andType:@"png" andDirectory:@"images"];
        self.postButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.postButton setImage:postButtonImage forState:UIControlStateNormal];
        self.postButton.frame = CGRectMake(72, 379, postButtonImage.size.width, postButtonImage.size.height);
        [self.postButton addTarget:self action:@selector(hideNotificationDetail:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.postButton];


    }
    return self;
}

-(void)hideNotificationDetail:(UIButton*)sender{
    self.hidden = YES;
}


@end
