//
//  RekeningModel.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RekeningModel : NSObject

@property (strong, nonatomic) NSNumber* rekeningID;
@property (strong, nonatomic) NSString* rekeningNaam;
@property (strong, nonatomic) NSNumber* saldo;
@property (strong, nonatomic) NSNumber* userID;


- (id)initWithRekeningNaam:(NSString*)rekeningNaam andID:(NSNumber*)rekeningID andSaldo:(NSNumber*)saldo andUserID:(NSNumber*)userID;

@end
