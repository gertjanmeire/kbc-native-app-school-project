//
//  UitgavenViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "DDPageControl.h"


@interface UitgavenViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) UISlider* periodeSlider;
@property (strong, nonatomic) UIScrollView* themasScrollView;

@property (strong, nonatomic) UIButton* tutorialButton;
@property (strong, nonatomic) DDPageControl* pageControl;

@end
