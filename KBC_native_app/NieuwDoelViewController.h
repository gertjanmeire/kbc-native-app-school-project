//
//  NieuwDoelViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 22/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "DDPageControl.h"

#import "RekeningModel.h"

@interface NieuwDoelViewController : UIViewController <UITextFieldDelegate, AMFRemotingCallDelegate, UIScrollViewDelegate>

@property (strong,nonatomic) NSArray* rekeningenArr;

@property (strong, nonatomic) UIScrollView* nieuwDoelScrollview;
@property (strong, nonatomic) UIImageView* nieuwDoelImageView;

@property (strong, nonatomic) CustomTextField* naamField;
@property (strong, nonatomic) CustomTextField* deadlineField;
@property (strong, nonatomic) CustomTextField* doelbedragField;
@property (strong, nonatomic) UIScrollView* rekeningPicker;
@property (strong, nonatomic) DDPageControl* pageControl;
@property (strong, nonatomic) UIButton* confirmButton;
@property (strong, nonatomic) UIButton* tutorialButton;

@end
