//
//  TransactieRowView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "TransactieRowView.h"

@implementation TransactieRowView

@synthesize transactieModel = _transactieModel;


@synthesize dateLabel = _dateLabel;
@synthesize titleLabel = _titleLabel;
@synthesize amountLabel = _amountLabel;

@synthesize transactieBG = _transactieBG;

- (id)initWithFrame:(CGRect)frame andTransactieModel:(TransactieModel*)model
{
    self = [super initWithFrame:frame];
    if (self) {
        //Rekening bg imageview
        UIImage *transactieBGImage = [ImageFactory createImageWithName:@"transactieRowBG" andType:@"png" andDirectory:@"images"];
        self.transactieBG = [[UIImageView alloc] initWithImage:transactieBGImage];
        [self.transactieBG setFrame:CGRectMake(0, 0, transactieBGImage.size.width, transactieBGImage.size.height)];
        [self addSubview:self.transactieBG];
        
        //Set the model
        self.transactieModel = model;
        
        //TEST
        //self.title = @"Elisa Cuyt";
        
        //Date label
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 150, 25)];
        [self.dateLabel setText:self.transactieModel.datum ];
        [self.dateLabel setFont:[UIFont fontWithName:@"LorimerLight" size:16]];
        [self.dateLabel setBackgroundColor:[UIColor clearColor]];
        [self.dateLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self addSubview:self.dateLabel];
        
        //Title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 0, 150, 30)];
        [self.titleLabel setText:self.transactieModel.titel];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:24]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self addSubview:self.titleLabel];
        
        //Amount label
        self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(245, 55, 100, 25)];
        [self.amountLabel setText:[NSString stringWithFormat:@"€ %.2f", [self.transactieModel.amount doubleValue]]];
        [self.amountLabel setFont:[UIFont fontWithName:@"LorimerLight" size:22]];
        [self.amountLabel setBackgroundColor:[UIColor clearColor]];
        if([self.transactieModel.betaalmethode isEqualToString:@"Debiteren"]){
            [self.amountLabel setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(167.0/255.0) blue:(90.0/255.0) alpha:0.8]];
        }else{
            [self.amountLabel setTextColor:[UIColor colorWithRed:(255.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:0.6]];
        }
        [self addSubview:self.amountLabel];
    }
    return self;
}



@end
