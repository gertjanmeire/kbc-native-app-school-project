//
//  ScashingReceiveViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface ScashingReceiveViewController : UIViewController

//Step 1 - wachten op betaling
@property (strong, nonatomic) UIView* step1View;


//Step 2 - binnenkomende betalingen
@property (strong, nonatomic) UIView* step2View;
@property (strong, nonatomic) UIImageView* totaalLabelImageView;
@property (strong, nonatomic) UILabel* totalReceivedMoneyLabel;
@property (strong, nonatomic) UIButton* bevestigButton;

@property (strong, nonatomic) UIImageView* betaling1ImageView;
@property (strong, nonatomic) UIImageView* betaling2ImageView;


//Tutorial overlay
@property (strong, nonatomic) UIButton* tutorialButton;



@end
