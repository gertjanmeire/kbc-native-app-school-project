//
//  ImageFactory.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "ImageFactory.h"
#import "UIImage+Iphone5Background.h"

@implementation ImageFactory

+(UIImage *)createImageWithName:(NSString *)imageName andType:(NSString *)imageType andDirectory:(NSString *)imageDir{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:imageType inDirectory:imageDir];
    UIImage *image = [UIImage imageNamedForDevice:imagePath];
    
    return image;
}

@end
