//
//  ScashingSendViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ScashingSendViewController.h"
#import "ScashingSendConfirmationViewController.h"
#import "KBCCentenView.h"

@interface ScashingSendViewController ()

@end

@implementation ScashingSendViewController

@synthesize step1View = _step1View;

@synthesize step2View = _step2View;
@synthesize connection1ImageView = _connection1ImageView;
@synthesize connection2ImageView = _connection2ImageView;
@synthesize connection3ImageView = _connection3ImageView;

@synthesize firstUserConnectionActive = _firstUserConnectionActive;
@synthesize secondUserConnectionActive = _secondUserConnectionActive;
@synthesize thirdUserConnectionActive = _thirdUserConnectionActive;

@synthesize bedragField = _bedragField;
@synthesize kbcCentenView = _kbcCentenView;
@synthesize tutorialButton = _tutorialButton;

/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //User interaction aanzetten
        self.view.userInteractionEnabled = YES;
        
        self.firstUserConnectionActive = NO;
        self.secondUserConnectionActive = NO;
        self.thirdUserConnectionActive = NO;
        
        //Timer voor step 2 te simuleren
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showIncommingConnections:) userInfo:nil repeats:NO];
        [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(showSecondConnection:) userInfo:nil repeats:NO];
        [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(showThirdConnection:) userInfo:nil repeats:NO];
    }
    return self;
}




-(void)showIncommingConnections:(NSTimer*)sender{
    self.step1View.hidden = YES;
    self.step2View.hidden = NO;
    
    
    //Tijdelijk
    [self setupSwipeDetection];

    //Show the first incomming connection
    [self showFirstConnection];
}

-(void)showFirstConnection{
    //Connection 1
    UIImage* connection1Image = [ImageFactory createImageWithName:@"sendMoneyConnection1" andType:@"png" andDirectory:@"images"];
    self.connection1ImageView = [[UIImageView alloc] initWithImage:connection1Image];
    [self.connection1ImageView setFrame:CGRectMake(20, 12, connection1Image.size.width, connection1Image.size.height)];
    self.connection1ImageView.userInteractionEnabled = YES;
    [self.step2View addSubview:self.connection1ImageView];
    
    UITapGestureRecognizer* connectionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(connection1Tapped:)];
    [self.connection1ImageView addGestureRecognizer:connectionTap];
}

-(void)showSecondConnection:(NSTimer*)sender{
    //Connection 2
    UIImage* connection2Image = [ImageFactory createImageWithName:@"sendMoneyConnection2" andType:@"png" andDirectory:@"images"];
    self.connection2ImageView = [[UIImageView alloc] initWithImage:connection2Image];
    [self.connection2ImageView setFrame:CGRectMake(20, 68, connection2Image.size.width, connection2Image.size.height)];
    self.connection2ImageView.userInteractionEnabled = YES;
    [self.step2View addSubview:self.connection2ImageView];
    
    UITapGestureRecognizer* connectionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(connection2Tapped:)];
    [self.connection2ImageView addGestureRecognizer:connectionTap];

}

-(void)showThirdConnection:(NSTimer*)sender{
    //Connection 3
    UIImage* connection3Image = [ImageFactory createImageWithName:@"sendMoneyConnection3" andType:@"png" andDirectory:@"images"];
    self.connection3ImageView = [[UIImageView alloc] initWithImage:connection3Image];
    [self.connection3ImageView setFrame:CGRectMake(20, 125, connection3Image.size.width, connection3Image.size.height)];
    self.connection3ImageView.userInteractionEnabled = YES;
    [self.step2View addSubview:self.connection3ImageView];
    
    UITapGestureRecognizer* connectionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(connection3Tapped:)];
    [self.connection3ImageView addGestureRecognizer:connectionTap];

}

-(void)connection1Tapped:(UITapGestureRecognizer*)sender{
    if(self.firstUserConnectionActive == NO){
        self.firstUserConnectionActive = YES;
        
        UIImage* connectionSelectedImage = [ImageFactory createImageWithName:@"sendMoneyConnectionSelected1" andType:@"png" andDirectory:@"images"];
        [self.connection1ImageView setImage:connectionSelectedImage];
        [self setupSwipeDetection];
    }else{
        self.firstUserConnectionActive = NO;
        
        UIImage* connectionSelectedImage = [ImageFactory createImageWithName:@"sendMoneyConnection1" andType:@"png" andDirectory:@"images"];
        [self.connection1ImageView setImage:connectionSelectedImage];
        [self setupSwipeDetection];
    }

}

-(void)connection2Tapped:(UITapGestureRecognizer*)sender{
    if(self.secondUserConnectionActive == NO){
        self.secondUserConnectionActive = YES;
        
        UIImage* connectionSelectedImage = [ImageFactory createImageWithName:@"sendMoneyConnectionSelected2" andType:@"png" andDirectory:@"images"];
        [self.connection2ImageView setImage:connectionSelectedImage];
        [self setupSwipeDetection];
    }else{
        self.secondUserConnectionActive = NO;
        
        UIImage* connectionSelectedImage = [ImageFactory createImageWithName:@"sendMoneyConnection2" andType:@"png" andDirectory:@"images"];
        [self.connection2ImageView setImage:connectionSelectedImage];
        [self setupSwipeDetection];
    }
}

-(void)connection3Tapped:(UITapGestureRecognizer*)sender{
    if(self.thirdUserConnectionActive == NO){
        self.thirdUserConnectionActive = YES;
        
        UIImage* connectionSelectedImage = [ImageFactory createImageWithName:@"sendMoneyConnectionSelected3" andType:@"png" andDirectory:@"images"];
        [self.connection3ImageView setImage:connectionSelectedImage];
        [self setupSwipeDetection];
    }else{
        self.thirdUserConnectionActive = NO;
        
        UIImage* connectionSelectedImage = [ImageFactory createImageWithName:@"sendMoneyConnection3" andType:@"png" andDirectory:@"images"];
        [self.connection3ImageView setImage:connectionSelectedImage];
        [self setupSwipeDetection];
    }
}


-(void)setupSwipeDetection{
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPayConfirmation:)];
    [swipeGesture setDirection:(UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown )];
    swipeGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:swipeGesture];
}

-(void)gotoPayConfirmation:(UISwipeGestureRecognizer*)sender{
    NSLog(@"PAY");
    ScashingSendConfirmationViewController *scashingConfirmationVC = [[ScashingSendConfirmationViewController alloc] initWithAmount:self.bedragField.text andReceiver:@"Gert-Jan Meire"];
    [self.viewDeckController rightViewPushViewControllerOverCenterController:scashingConfirmationVC];
}

/****************************************************************************************************************/
// viewDidLoad: add the graphical components
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Step 1
    self.step1View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.step1View setBackgroundColor:[UIColor yellowColor]];
    [self.view addSubview:self.step1View];
    
    //Background image step 1 view
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"sendMoneyStep1BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.step1View addSubview:backgroundImageView];
    
    

    
    //Step 2
    self.step2View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.step2View.hidden = YES;
    [self.view addSubview:self.step2View];
    
    //Background image step 2 view
    UIImage *backgroundImage2 = [ImageFactory createImageWithName:@"sendMoneyStep2BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView2 = [[UIImageView alloc] initWithImage:backgroundImage2];
    [self.step2View addSubview:backgroundImageView2];
    
    //Bedrag field
    UIImage *bedragFieldImage = [ImageFactory createImageWithName:@"scashingBedragTextfieldBG" andType:@"png" andDirectory:@"images"];
    self.bedragField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 215, bedragFieldImage.size.width, bedragFieldImage.size.height)];
    [self.bedragField changeLeftMargin:35];
    [self.bedragField setBackground:bedragFieldImage];
    [self.bedragField setDelegate:self];
    [self.bedragField addTarget:self action:@selector(bedragChanged:) forControlEvents:UIControlEventEditingChanged];
    //[self.bedragField setKeyboardType:UIKeyboardTypePhonePad];
    [self.view addSubview:self.bedragField];


    
    self.kbcCentenView = [[KBCCentenView alloc] initWithFrame:CGRectMake(234, 234, 100, 50)];
    [self.view addSubview:self.kbcCentenView];

    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(290, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.step2View addSubview:self.tutorialButton];
        
    
    
}

-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayScashingSendMoney" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}

-(void)bedragChanged:(UITextField*)sender{
    NSNumberFormatter * bedragFormatter = [[NSNumberFormatter alloc] init];
    [bedragFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * bedrag = [bedragFormatter numberFromString:self.bedragField.text];
    NSNumber* kbcCenten = [NSNumber numberWithDouble:([bedrag doubleValue] / 2)];
    
    [self.kbcCentenView changeLabel:[NSString stringWithFormat:@"%d", [kbcCenten intValue]]];
}


/****************************************************************************************************************/
// viewDidAppear: hide the back buttons, show the navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_scashenBetalen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;

    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
