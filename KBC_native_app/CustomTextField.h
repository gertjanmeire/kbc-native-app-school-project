//
//  CustomTextField.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 01/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@property ( nonatomic) int leftMargin;
@property ( nonatomic) int topMargin;

-(void)changeTopMargin:(int)topMargin;
-(void)changeLeftMargin:(int)leftMargin;


@end
