//
//  RegistrationViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "RegistrationViewController.h"
#import "AppModel.h"
#import "IntroWizardViewController.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController

@synthesize appModel = appModel;

@synthesize step1View = _step1View;
@synthesize cardnrFieldForResponse = _cardnrFieldForResponse;
@synthesize responseField = _responseField;
@synthesize responseButton = _responseButton;
@synthesize tutorialButton = _tutorialButton;

@synthesize step2View = _step2View;
@synthesize passwordField1 = _passwordField1;
@synthesize passwordField2 = _passwordField2;
@synthesize confirmButton = _confirmButton;

@synthesize step3View = _step3View;
@synthesize step3InfoButton = _step3InfoButton;
@synthesize bevestigButton = _bevestigButton;

@synthesize step4View = _step4View;
@synthesize cardnrField = _cardnrField;
@synthesize passwordField = _passwordField;
@synthesize rememberPasswordImageView = _rememberPasswordImageView;
@synthesize registerButton = _registerButton;

@synthesize registrationCompleteView = _registrationCompleteView;
@synthesize gotoLoginButton = _gotoLoginButton;


/****************************************************************************************************************/
// init
/****************************************************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Setup appmodel instance
        appModel = [[AppModel sharedAppModel] init];
    }
    return self;
}

/****************************************************************************************************************/
// viewDidLoad: add the graphical components to the RegistrationViewController
/****************************************************************************************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    
    //STEP 1 VIEW
    self.step1View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.registrationCompleteView setBackgroundColor:[UIColor yellowColor]];
    [self.view addSubview:self.step1View];
    
    //Background image step 1 view
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"registrationStep1BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.step1View addSubview:backgroundImageView];

    
    //Textfield
    UIImage *cardnrFieldForResponseImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.cardnrFieldForResponse = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 87, cardnrFieldForResponseImage.size.width, cardnrFieldForResponseImage.size.height)];
    [self.cardnrFieldForResponse setBackground:cardnrFieldForResponseImage];
    [self.cardnrFieldForResponse setPlaceholder:@"Kaartnummer"];
    [self.cardnrFieldForResponse setDelegate:self];
    [self.step1View addSubview:self.cardnrFieldForResponse];
    
    //Textfield
    UIImage *responseFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.responseField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 227, responseFieldImage.size.width, responseFieldImage.size.height)];
    [self.responseField setBackground:responseFieldImage];
    [self.responseField setPlaceholder:@"Response"];
    [self.responseField setDelegate:self];
    [self.step1View addSubview:self.responseField];
    
    //Button
    UIImage *responseButtonImage = [ImageFactory createImageWithName:@"bevestigRegButton" andType:@"png" andDirectory:@"images"];
    self.responseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.responseButton setImage:responseButtonImage forState:UIControlStateNormal];
    self.responseButton.frame = CGRectMake(72, 311, responseButtonImage.size.width, responseButtonImage.size.height);
    [self.responseButton addTarget:self action:@selector(gotoRegistrationStep2:) forControlEvents:UIControlEventTouchUpInside];
    [self.step1View addSubview:self.responseButton];
    
    //Tutorial button
    UIImage *tutorialButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.tutorialButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tutorialButton setImage:tutorialButtonImage forState:UIControlStateNormal];
    self.tutorialButton.frame = CGRectMake(280, 455, tutorialButtonImage.size.width, tutorialButtonImage.size.height);
    [self.tutorialButton addTarget:self action:@selector(showTutorialForStep1:) forControlEvents:UIControlEventTouchUpInside];
    [self.step1View addSubview:self.tutorialButton];

    
    
    //STEP 2 VIEW
    self.step2View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.registrationCompleteView setBackgroundColor:[UIColor blackColor]];
    self.step2View.hidden = YES;
    [self.view addSubview:self.step2View];
    
    //Background image step 2 view
    UIImage *backgroundImage2 = [ImageFactory createImageWithName:@"registrationStep2BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView2 = [[UIImageView alloc] initWithImage:backgroundImage2];
    [self.step2View addSubview:backgroundImageView2];

    
    //Textfield
    UIImage *passwordField1Image = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.passwordField1 = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 98, passwordField1Image.size.width, passwordField1Image.size.height)];
    [self.passwordField1 setBackground:passwordField1Image];
    [self.passwordField1 setPlaceholder:@"Wachtwoord"];
    [self.passwordField1 setSecureTextEntry:YES];
    [self.passwordField1 setDelegate:self];
    [self.step2View addSubview:self.passwordField1];
    
    //Textfield
    UIImage *passwordField2Image = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.passwordField2 = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 203, passwordField2Image.size.width, passwordField2Image.size.height)];
    [self.passwordField2 setBackground:passwordField2Image];
    [self.passwordField2 setPlaceholder:@"Wachtwoord"];
    [self.passwordField2 setSecureTextEntry:YES];
    [self.passwordField2 setDelegate:self];
    [self.step2View addSubview:self.passwordField2];
    
    //Button
    UIImage *confirmButtonImage = [ImageFactory createImageWithName:@"bevestigRegButton" andType:@"png" andDirectory:@"images"];
    self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.confirmButton setImage:confirmButtonImage forState:UIControlStateNormal];
    self.confirmButton.frame = CGRectMake(72, 284, confirmButtonImage.size.width, confirmButtonImage.size.height);
    [self.confirmButton addTarget:self action:@selector(checkIfPasswordFieldsAreEqual:) forControlEvents:UIControlEventTouchUpInside];
    [self.step2View addSubview:self.confirmButton];
    
    
    //STEP 3 VIEW
    self.step3View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.step3View.hidden = YES;
    [self.view addSubview:self.step3View];
    
    //Background image step 3 view
    UIImage *backgroundImage3 = [ImageFactory createImageWithName:@"registrationStep3BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView3 = [[UIImageView alloc] initWithImage:backgroundImage3];
    [self.step3View addSubview:backgroundImageView3];
    
    
    //Bevestig button
    UIImage *bevestigButtonImage = [ImageFactory createImageWithName:@"bevestigButton" andType:@"png" andDirectory:@"images"];
    self.bevestigButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bevestigButton setImage:bevestigButtonImage forState:UIControlStateNormal];
    self.bevestigButton.frame = CGRectMake(72, 380, bevestigButtonImage.size.width, bevestigButtonImage.size.height);
    [self.bevestigButton addTarget:self action:@selector(gotoRegistrationStep4:) forControlEvents:UIControlEventTouchUpInside];
    [self.step3View addSubview:self.bevestigButton];

    
    //Info button
    UIImage *step3InfoButtonImage = [ImageFactory createImageWithName:@"tutorialButton" andType:@"png" andDirectory:@"images"];
    self.step3InfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.step3InfoButton setImage:step3InfoButtonImage forState:UIControlStateNormal];
    self.step3InfoButton.frame = CGRectMake(290, 455, step3InfoButtonImage.size.width, step3InfoButtonImage.size.height);
    [self.step3InfoButton addTarget:self action:@selector(showTutorialForView:) forControlEvents:UIControlEventTouchUpInside];
    [self.step3View addSubview:self.step3InfoButton];


    
    
    //STEP 4 VIEW
    self.step4View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.registrationCompleteView setBackgroundColor:[UIColor blueColor]];
    self.step4View.hidden = YES;
    [self.view addSubview:self.step4View];
    
    //Background image step 4 view
    UIImage *backgroundImage4 = [ImageFactory createImageWithName:@"registrationStep4BG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView4 = [[UIImageView alloc] initWithImage:backgroundImage4];
    [self.step4View addSubview:backgroundImageView4];

    
    //Textfield
    UIImage *cardnrFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.cardnrField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 74, cardnrFieldImage.size.width, cardnrFieldImage.size.height)];
    [self.cardnrField setBackground:cardnrFieldImage];
    [self.cardnrField setPlaceholder:@"Kaartnummer"];
    [self.cardnrField setDelegate:self];
    [self.step4View addSubview:self.cardnrField];
    
    //Textfield
    UIImage *passwordFieldImage = [ImageFactory createImageWithName:@"textfieldBG" andType:@"png" andDirectory:@"images"];
    self.passwordField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 131, passwordFieldImage.size.width, passwordFieldImage.size.height)];
    [self.passwordField setBackground:passwordFieldImage];
    [self.passwordField setPlaceholder:@"Wachtwoord"];
    [self.passwordField setSecureTextEntry:YES];
    [self.passwordField setDelegate:self];
    [self.step4View addSubview:self.passwordField];
    
    //ImageView
    UIImage *rememberPasswordImage = [ImageFactory createImageWithName:@"kaartnummerOnthouden" andType:@"png" andDirectory:@"images"];
    self.rememberPasswordImageView = [[UIImageView alloc] initWithImage:rememberPasswordImage];
    [self.rememberPasswordImageView setFrame:CGRectMake(20, 190, rememberPasswordImage.size.width, rememberPasswordImage.size.height)];
    [self.step4View addSubview:self.rememberPasswordImageView];

    
    //Button
    UIImage *registerButtonImage = [ImageFactory createImageWithName:@"aanmeldenButton" andType:@"png" andDirectory:@"images"];
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerButton setImage:registerButtonImage forState:UIControlStateNormal];
    self.registerButton.frame = CGRectMake(72, 240, registerButtonImage.size.width, registerButtonImage.size.height);
    [self.registerButton addTarget:self action:@selector(saveUserInDB:) forControlEvents:UIControlEventTouchUpInside];
    [self.step4View addSubview:self.registerButton];

    
    
    //REGISTRATION COMPLETE VIEW
    /*self.registrationCompleteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.registrationCompleteView.hidden = YES;
    [self.registrationCompleteView setBackgroundColor:[UIColor purpleColor]];
    [self.view addSubview:self.registrationCompleteView];
    
    UIImage *gotoLoginButtonImage = [ImageFactory createImageWithName:@"gotoLoginButton" andType:@"png" andDirectory:@"images"];
    self.gotoLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.gotoLoginButton setImage:gotoLoginButtonImage forState:UIControlStateNormal];
    self.gotoLoginButton.frame = CGRectMake(25, 300, gotoLoginButtonImage.size.width, gotoLoginButtonImage.size.height);
    [self.gotoLoginButton addTarget:self action:@selector(goBackToLogin:) forControlEvents:UIControlEventTouchUpInside];
    self.gotoLoginButton.hidden = YES;
    [self.view addSubview:self.gotoLoginButton];*/

}


-(void)showTutorialForStep1:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayRegStep1" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}



/****************************************************************************************************************/
// gotoRegistrationStep2: go to the next step in the registration process
/****************************************************************************************************************/
-(void)gotoRegistrationStep2:(UIButton*)sender{
    appModel.registeringCardNr = self.cardnrFieldForResponse.text;
    self.step1View.hidden = YES;
    self.step2View.hidden = NO;
    self.step3View.hidden = YES;
    self.step4View.hidden = YES;
}


/****************************************************************************************************************/
// checkIfPasswordFieldsAreEqual: check if password fields are equal
/****************************************************************************************************************/
-(void)checkIfPasswordFieldsAreEqual:(UIButton*)sender{
    NSString* pass1 = self.passwordField1.text;
    NSString* pass2 = self.passwordField2.text;
    
    if([pass1 isEqualToString:pass2] && ![pass1 isEqualToString:@""] && ![pass2 isEqualToString:@""]){
        NSLog(@"Passwords are equal: go to next step");
        //Go to next step
        [self gotoRegistrationStep3:nil];
        
        //Save the password in the appModel
        appModel.registeringPassword = pass1;
    }else{
        NSLog(@"Passwords are not equal");
        
        //Show error message
        UIImage* errorTextfieldBG = [ImageFactory createImageWithName:@"textfieldErrorBG" andType:@"png" andDirectory:@"images"];
        [self.passwordField1 setBackground:errorTextfieldBG];
        [self.passwordField1 setTextColor:[UIColor whiteColor]];
        //Change placeholder text color
        self.passwordField1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Uw wachtwoorden zijn niet gelijk" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        
        [self.passwordField2 setBackground:errorTextfieldBG];
        [self.passwordField2 setTextColor:[UIColor whiteColor]];
        //Change placeholder text color
        self.passwordField2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Uw wachtwoorden zijn niet gelijk" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];


    }
}


/****************************************************************************************************************/
// saveUserInDB: check if password is correct (in appModel.registeringPassword and save to DB
/****************************************************************************************************************/
-(void)saveUserInDB:(UIButton*)sender{
    NSString* cardnr = self.cardnrField.text;
    NSString* pass = self.passwordField.text;
    
    if([pass isEqualToString:appModel.registeringPassword] && [cardnr isEqualToString:appModel.registeringCardNr] && ![pass isEqualToString:@""]){
        //Save the user cardnr and password to the DB as a new user
        [self saveUserToDBWithAMFCallWithCardNr:cardnr andPassword:pass];
    }else{
        NSLog(@"Error! The password or cardnr fields are not correct!");
        UIImage* errorTextfieldBG = [ImageFactory createImageWithName:@"textfieldErrorBG" andType:@"png" andDirectory:@"images"];
        [self.cardnrField setBackground:errorTextfieldBG];
        [self.cardnrField setTextColor:[UIColor whiteColor]];
        //Change placeholder text color
        self.cardnrField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Uw kaartnr is niet correct" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        
        [self.passwordField setBackground:errorTextfieldBG];
        [self.passwordField setText:@""];
        [self.passwordField setTextColor:[UIColor whiteColor]];
        //Change placeholder text color
        self.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Uw wachtwoord is niet correct" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];


    }
}


/****************************************************************************************************************/
// saveUserToDBWithAMFCallWithCardNr: send an AMFPHP request to the PHP webservice to save the user in the DB
/****************************************************************************************************************/
-(void)saveUserToDBWithAMFCallWithCardNr:(NSString*)cardnr andPassword:(NSString*)password{
        NSArray *saveArguements = [NSArray arrayWithObjects:cardnr, password, nil];
        
        //Check webservice is user exists
        //If so change view else show error message
        NSString *URLString = [NSString stringWithFormat:@"%@", appModel.IP];
        AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"MajorVService" method:@"saveUserToDB" arguments:[NSArray arrayWithObjects: saveArguements, nil]];
        m_remotingCall.delegate = self;
        [m_remotingCall start];
}

/****************************************************************************************************************/
// remotingCallDidFinishLoading:
/****************************************************************************************************************/
- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSObject *)object
{
	NSLog(@"[MainVC] remoting call was successful. received data: %@", [object valueForKey:@"validation"]);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    NSString *successString = (NSString *)[object valueForKey:@"success"];
    
    //If the validationString is "login" check if the user exists and login or show an error message
    if([successString isEqualToString:@"true"]){
        //If the user is saved to the DB go to the confirmation step of the registration process
        IntroWizardViewController *introWizardVC = [[IntroWizardViewController alloc] init];
        [self.navigationController pushViewController:introWizardVC animated:YES];
        
    //If the validationString is "registration" go to the RegistrationViewController
    }else if([successString isEqualToString:@"false"]){
        NSLog(@"Registration event came back from AMFPHP");
        
    }
}


/****************************************************************************************************************/
// remotingCall: fail of the remote call
/****************************************************************************************************************/
- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"[MainVC] remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}



/****************************************************************************************************************/
// gotoRegistrationStep3: go to the next step in the registration process
/****************************************************************************************************************/
-(void)gotoRegistrationStep3:(UIButton*)sender{
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = NO;
    self.step4View.hidden = YES;
}


/****************************************************************************************************************/
// gotoRegistrationStep3: go to the next step in the registration process
/****************************************************************************************************************/
-(void)gotoRegistrationStep4:(UIButton*)sender{
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = YES;
    self.step4View.hidden = NO;
}


/****************************************************************************************************************/
// gotoRegistrationCompleteView: go to the last step in the registration process
/****************************************************************************************************************/
-(void)gotoRegistrationCompleteView{
    self.step1View.hidden = YES;
    self.step2View.hidden = YES;
    self.step3View.hidden = YES;
    self.registrationCompleteView.hidden = NO;
    self.gotoLoginButton.hidden = NO;
}


/****************************************************************************************************************/
// viewWillAppear: Hide the back button and show the navigation bar
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;

    //Navigation bar image
    self.navigationController.navigationBar.hidden = NO;
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_registratie" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];

    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// textFieldShouldReturn: delegate method of UITextFieldDelegate protocol that makes sure the keyboard hides when returning
/****************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/****************************************************************************************************************/
// goBackToLogin: go back to login for your first time after registration
/****************************************************************************************************************/
-(void)goBackToLogin:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)showTutorialForView:(UIButton*)sender{
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"overlayRegStep3" andType:@"png" andDirectory:@"images"];
    TutorialOverlayView *tutorialView = [[TutorialOverlayView alloc] initWithFrame:CGRectMake(0, 20, 320, 568) andBackgroundImage:backgroundImage];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
