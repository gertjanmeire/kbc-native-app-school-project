//
//  ZoekKantoorInAppViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "IIViewDeckController.h"

@interface ZoekKantoorInAppViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) CustomTextField* zoekOpGemeenteField;
@property (strong, nonatomic) UIButton* zoekOpGemeenteButton;
@property (strong, nonatomic) UIButton* zoekMetGPSButton;

@property (strong, nonatomic) MKMapView* mapView;
@property (strong, nonatomic) CLLocationManager* locationManager;

@end
