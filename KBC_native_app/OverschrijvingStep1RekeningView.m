//
//  OverschrijvingStep1RekeningView.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "OverschrijvingStep1RekeningView.h"

@implementation OverschrijvingStep1RekeningView

@synthesize rekeningModel = _rekeningModel;

@synthesize bundleView = _bundleView;

@synthesize rekeningImageView = _rekeningImageView;
@synthesize titleLabel = _titleLabel;
@synthesize saldoLabel = _saldoLabel;
@synthesize amountLabel = _amountLabel;

- (id)initWithFrame:(CGRect)frame andRekeningModel:(RekeningModel*)model
{
    self = [super initWithFrame:frame];
    if (self) {        
        //Bundeling view
        self.bundleView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, self.bounds.size.width, self.bounds.size.height)];
        [self addSubview:self.bundleView];


        //Rekeningmodel instellen
        self.rekeningModel = model;
                
        //Rekening bg imageview
        UIImage *rekeningBGImage = [ImageFactory createImageWithName:@"rekeningView2BG" andType:@"png" andDirectory:@"images"];
        self.rekeningImageView = [[UIImageView alloc] initWithImage:rekeningBGImage];
        [self.rekeningImageView setFrame:CGRectMake(0, 0, rekeningBGImage.size.width, rekeningBGImage.size.height)];
        [self.bundleView addSubview:self.rekeningImageView];
        
        //Title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 264, 50)];
        [self.titleLabel setText:self.rekeningModel.rekeningNaam];
        [self.titleLabel setFont:[UIFont fontWithName:@"LorimerLight" size:27]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.bundleView addSubview:self.titleLabel];
        
        //saldo label
        self.saldoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 88, 264, 50)];
        [self.saldoLabel setText:@"saldo"];
        [self.saldoLabel setFont:[UIFont fontWithName:@"LorimerLight" size:27]];
        [self.saldoLabel setBackgroundColor:[UIColor clearColor]];
        [self.saldoLabel setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        self.saldoLabel.textAlignment = NSTextAlignmentCenter;
        [self.bundleView addSubview:self.saldoLabel];

        
        //Amount label
        self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 123, 264, 50)];
        [self.amountLabel setText:[NSString stringWithFormat:@"%.2f EUR", [self.rekeningModel.saldo doubleValue]]];
        [self.amountLabel setFont:[UIFont fontWithName:@"LorimerLight" size:36]];
        [self.amountLabel setBackgroundColor:[UIColor clearColor]];
        self.amountLabel.textAlignment = NSTextAlignmentCenter;
        [self.amountLabel setTextColor:[UIColor colorWithRed:(210.0/255.0) green:(186.0/255.0) blue:(23.0/255.0) alpha:1.0]];
        [self.bundleView addSubview:self.amountLabel];

    }
    return self;
}


@end
