//
//  ADVRoundProgressViewType2.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 28/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ADVRoundProgressViewType2.h"
#import "CERoundProgressView/CERoundProgressView.h"

@interface ADVRoundProgressViewType2 ()

@property (nonatomic, strong) CERoundProgressView   *pieView;
@property (nonatomic, strong) UIImageView           *imgView;
@property (nonatomic, strong) UILabel               *lblValue;
@property (nonatomic, strong) UIImage               *backgroundImage;

- (void)_initIVars;

@end


@implementation ADVRoundProgressViewType2

@synthesize pieView;
@synthesize piePadding;
@synthesize imgView;
@synthesize lblValue;
@synthesize progress;
@synthesize image;
@synthesize fontSize;
@synthesize tintColor;
@synthesize backgroundImage = _backgroundImage;


#pragma mark - View lifecycle

- (id)initWithFrame:(CGRect)frame andImage:(UIImage*)sliderBackgroundImage{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundImage = sliderBackgroundImage;
        [self _initIVars];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self _initIVars];
    }
    return self;
}

- (void)_initIVars {
    self.backgroundColor = [UIColor clearColor];
    
    self.pieView = [[CERoundProgressView alloc] initWithFrame:self.bounds];
    self.pieView.tintColor = self.tintColor;
    self.pieView.startAngle = M_PI/2;
    [self addSubview:self.pieView];
    
    self.imgView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imgView.image = self.backgroundImage;
    [self addSubview:self.imgView];
    
    self.lblValue = [[UILabel alloc] initWithFrame:self.bounds];
    lblValue.backgroundColor = [UIColor clearColor];
    lblValue.textAlignment = NSTextAlignmentCenter;
    [lblValue setFont:[UIFont fontWithName:@"LorimerLight" size:90]];
    lblValue.textColor = [UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0];
    lblValue.shadowOffset = CGSizeMake(0, 1);
    [self addSubview:self.lblValue];
    
    
    self.piePadding = 1.5;
}


#pragma mark - Accessors

- (void)setProgress:(float)newProgress {
    if (newProgress < 0) {
        newProgress = 0.0;
    } else if(newProgress > 1.0) {
        newProgress = 1.0;
    }
    
    if (progress == newProgress) {
        return;
    }
    
    progress = newProgress;
    self.pieView.progress = progress;
    self.lblValue.text = [NSString stringWithFormat:@"%2.0f%%", progress*100];
}

- (void)setFontSize:(float)newFontSize {
    if (fontSize == newFontSize) {
        return;
    }
    
    fontSize = newFontSize;
    self.lblValue.font = [UIFont fontWithName:self.lblValue.font.fontName size:fontSize];
}

- (void)setImage:(UIImage *)newImage {
    if ([image isEqual:newImage]) {
        return;
    }
    
    image = newImage;
    self.imgView.image = image;
}

- (void)setTintColor:(UIColor *)newTintColor {
    if ([tintColor isEqual:newTintColor]) {
        return;
    }
    
    tintColor = newTintColor;
    self.pieView.tintColor = tintColor;
}

- (void)setPiePadding:(float)newPiePadding {
    if (piePadding == newPiePadding) {
        return;
    }
    
    piePadding = newPiePadding;
    CGRect pieFrame = self.bounds;
    pieFrame.origin.x = piePadding;
    pieFrame.origin.y = piePadding;
    pieFrame.size.width -= 2*pieFrame.origin.x;
    pieFrame.size.height -= 2*pieFrame.origin.y;
    
    self.pieView.frame = pieFrame;
}

@end
