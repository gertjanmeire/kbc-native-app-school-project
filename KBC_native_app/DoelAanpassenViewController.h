//
//  DoelAanpassenViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "DoelModel.h"
#import "DDPageControl.h"

@interface DoelAanpassenViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) DoelModel* doelModel;

@property (strong, nonatomic) NSArray* rekeningenArr;

@property (strong, nonatomic) CustomTextField* bedragField;
@property (strong, nonatomic) UISlider* intervalSlider;
@property (strong, nonatomic) UIScrollView* rekeningPicker;
@property (strong, nonatomic) DDPageControl* pageControl;
@property (strong, nonatomic) UIButton* confirmButton;

- (id)initWithDoelModel:(DoelModel*)model;

@end
