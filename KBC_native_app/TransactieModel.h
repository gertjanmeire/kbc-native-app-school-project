//
//  TransactieModel.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactieModel : NSObject

@property (strong, nonatomic) NSNumber* transactieID;
@property (strong, nonatomic) NSString* rekeningNaam;
@property (strong, nonatomic) NSNumber* saldo;
@property (strong, nonatomic) NSNumber* userID;
@property (strong, nonatomic) NSString* betaalmethode;
@property (strong, nonatomic) NSString* datum;
@property (strong, nonatomic) NSString* titel;
@property (strong, nonatomic) NSNumber* amount;
@property (strong, nonatomic) NSString* plaats;
@property (strong, nonatomic) NSString* gemeente;
@property (strong, nonatomic) NSNumber* rekeningID;


- (id)initWithTransactieID:(NSNumber*)transactieID andRekeningNaam:(NSString*)rekeningNaam andSaldo:(NSNumber*)saldo andUserID:(NSNumber*)userID andBetaalmethode:(NSString*)betaalmethode andDate:(NSString*)datum  andTitle:(NSString*)titel andAmount:(NSNumber*)amount andPlaats:(NSString*)plaats andGemeente:(NSString*)gemeente andRekeningID:(NSNumber*)rekeningID;

@end
