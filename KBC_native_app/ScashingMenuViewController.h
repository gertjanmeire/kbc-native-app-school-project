//
//  ScashingMainMenuViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface ScashingMenuViewController : UIViewController

@property (nonatomic, assign) IIViewDeckNavigationControllerBehavior behavior;

//Tutorial overlay
@property (strong, nonatomic) UIButton* tutorialButton;

@property (strong, nonatomic) UIButton* receiveMoneyButton;
@property (strong, nonatomic) UIButton* sendMoneyButton;
@property (strong, nonatomic) UIButton* scashPotButton;

@end
