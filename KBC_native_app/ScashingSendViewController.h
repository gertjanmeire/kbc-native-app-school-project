//
//  ScashingSendViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "KBCCentenView.h"

@interface ScashingSendViewController : UIViewController <UITextFieldDelegate>

//Step 1
@property (strong, nonatomic) UIView* step1View;

//Step 2
@property (strong, nonatomic) UIView* step2View;
@property (strong, nonatomic) UIImageView* connection1ImageView;
@property (strong, nonatomic) UIImageView* connection2ImageView;
@property (strong, nonatomic) UIImageView* connection3ImageView;

@property (nonatomic) BOOL firstUserConnectionActive;
@property (nonatomic) BOOL secondUserConnectionActive;
@property (nonatomic) BOOL thirdUserConnectionActive;


//Algemeen
@property (strong, nonatomic) CustomTextField* bedragField;
@property (strong, nonatomic) KBCCentenView* kbcCentenView;
@property (strong, nonatomic) UIButton* tutorialButton;

@end
