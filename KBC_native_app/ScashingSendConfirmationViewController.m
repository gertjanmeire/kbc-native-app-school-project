//
//  ScashingSendConfirmationViewController.m
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 26/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import "ScashingSendConfirmationViewController.h"

@interface ScashingSendConfirmationViewController ()

@end

@implementation ScashingSendConfirmationViewController

@synthesize amountLabel = _amountLabel;
@synthesize receiverLabel = _receiverLabel;
@synthesize confirmButton = _confirmButton;
@synthesize tutorialButton = _tutorialButton;

- (id)initWithAmount:(NSString*)amount andReceiver:(NSString*)receiver
{
    self = [super init];
    if (self) {
        //Amount label
        self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 300, 30)];
        [self.amountLabel setFont:[UIFont fontWithName:@"LorimerLight" size:30]];
        [self.amountLabel setText:[NSString stringWithFormat:@"€ %@", amount]];
        self.amountLabel.textAlignment = NSTextAlignmentCenter;
        [self.amountLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.amountLabel setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:self.amountLabel];
        
        
        //Receiver label
        self.receiverLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 110, 300, 30)];
        [self.receiverLabel setFont:[UIFont fontWithName:@"LorimerLight" size:30]];
        [self.receiverLabel setText:receiver];
        self.receiverLabel.textAlignment = NSTextAlignmentCenter;
        [self.receiverLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(78.0/255.0) blue:(130.0/255.0) alpha:1.0]];
        [self.receiverLabel setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:self.receiverLabel];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set background image
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"scashingBetalingConfirmatieBG" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    //Confirm button
    UIImage *confirmButtonImage = [ImageFactory createImageWithName:@"bevestigButton" andType:@"png" andDirectory:@"images"];
    self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.confirmButton setImage:confirmButtonImage forState:UIControlStateNormal];
    self.confirmButton.frame = CGRectMake(72, 435, confirmButtonImage.size.width, confirmButtonImage.size.height);
    [self.confirmButton addTarget:self action:@selector(goBackToScashingMenu:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.confirmButton];

}


-(void)goBackToScashingMenu:(UIButton*)sender{    
    //Get the view controller that is 2 step behind
    UIViewController *scashingMenuVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 3];
    
    [self.navigationController popToViewController:scashingMenuVC animated:YES];
}

/****************************************************************************************************************/
// viewWillAppear: hide the back buttons, show the navigation bar and custom buttons
/****************************************************************************************************************/
-(void)viewWillAppear:(BOOL)animated{
    //Hide back button
    [self.navigationItem setHidesBackButton:YES];
    
    //Navigation bar image
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_scashenBetalen" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    //Back button
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = navbarBackButton;
    
    
    //Main menu button
    UIImage *mainMenuButtonImage = [ImageFactory createImageWithName:@"mainMenuButton" andType:@"png" andDirectory:@"images"];
    UIButton *mainMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mainMenuButton setImage:mainMenuButtonImage forState:UIControlStateNormal];
    mainMenuButton.frame = CGRectMake(0, 5, mainMenuButtonImage.size.width, mainMenuButtonImage.size.height);
    [mainMenuButton addTarget:self action:@selector(slideOpenMainMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarMainMenuButton = [[UIBarButtonItem alloc] initWithCustomView:mainMenuButton];
    self.navigationItem.rightBarButtonItem = navbarMainMenuButton;
    
    [super viewWillAppear:animated];
}


/****************************************************************************************************************/
// slideOpenMainMenu: slide open the main menu when clicking the button in the navigation bar
/****************************************************************************************************************/
-(void)slideOpenMainMenu:(UIButton *)sender{
    [self.viewDeckController openRightViewAnimated:YES];
}


/****************************************************************************************************************/
// backButton: go back 1 view on the navigation controller stack
/****************************************************************************************************************/
-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
