//
//  TutorialOverlayView.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 24/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialOverlayView : UIView

@property (strong, nonatomic) UIImageView *backgroundImageView;

-(id)initWithFrame:(CGRect)frame andBackgroundImage:(UIImage*)backgroundImage;
-(void)showOverlay;

@end
