//
//  HulpViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 21/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDPageControl.h"

@interface HulpViewController : UIViewController <UIScrollViewDelegate, AMFRemotingCallDelegate>

@property (strong, nonatomic) UIScrollView* hulpPicker;
@property (strong, nonatomic) DDPageControl* pageControl;

@end
