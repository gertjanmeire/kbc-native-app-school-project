//
//  IntroWizardViewController.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 23/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroWizardViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView* wizardScrollview;
@property (strong, nonatomic) UIImageView* wizardImageView;
@property (strong, nonatomic) UIButton* bevestigButton;

@end
