//
//  ADVRoundProgressViewType2.h
//  KBC_native_app
//
//  Created by Gert-Jan Meire on 28/01/13.
//  Copyright (c) 2013 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADVRoundProgressViewType2 : UIView

@property (nonatomic)           float    progress;
@property (nonatomic)           float    piePadding;
@property (nonatomic)           float    fontSize;
@property (nonatomic, strong)   UIImage *image;
@property (nonatomic, retain) UIColor *tintColor UI_APPEARANCE_SELECTOR;


- (id)initWithFrame:(CGRect)frame andImage:(UIImage*)sliderBackgroundImage;

@end
